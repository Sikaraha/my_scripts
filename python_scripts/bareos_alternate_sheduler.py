#!/home/nevdokimov/my_scripts/PyCharm/venv/bin/python
# -*- coding: utf-8 -*-

import socket, logging, psycopg2, bareos.bsock, ConfigParser
from os import path as os_path
from sys import exit as sys_exit
from re import findall as re_findall, match as re_match
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta

def get_lock(process_name):
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        get_lock._lock_socket.bind('\0' + process_name)
    except socket.error:
        logging.error('{0} already running!'.format(process_name))
        sys_exit(1)

def getConfig(path):
	"""Загрузка и паринг конфига"""

    if not os_path.exists(path):
        logging.critical('config file not found!')
        sys_exit(1)

    global bareos_addr, bareos_port, bareos_pass, jobs_max, db_addr, db_user, db_pass, db_host, db_port

    try:
        config = ConfigParser.ConfigParser()
        config.read(path)

        bareos_addr = config.get("bareos-dir", "Address")
        bareos_port = config.get("bareos-dir", "Port")
        bareos_pass = bareos.bsock.Password(config.get("bareos-dir", "Password"))
        jobs_max = config.get("bareos-dir", "MaxRunningJob")
        db_addr = config.get("catalog", "Database")
        db_user = config.get("catalog", "User")
        db_pass = config.get("catalog", "Password")
        db_host = config.get("catalog", "Host")
        db_port = config.get("catalog", "Port")
    except ConfigParser.MissingSectionHeaderError:
        logging.critical('config file error!')
        sys_exit(1)
    except ConfigParser.NoOptionError:
        logging.critical('config options error!')
        sys_exit(1)

def db_connect(query):
	"""Подсключение в DB Postgres"""

    try:
        conn = psycopg2.connect(database=db_addr, user=db_user, password=db_pass, host=db_host, port=db_port)
        cursor = conn.cursor()
        cursor.execute(query)
    except psycopg2.OperationalError:
        logging.error('Database not available. Check name or password')
        sys_exit(1)
    except (psycopg2.errors.UndefinedColumn, psycopg2.errors.SyntaxError):
        logging.critical('SQL query error')
        sys_exit(1)
    else:
        return cursor

def bareos_connect(command):
	"""Подключение к bareos director"""

    try:
        directorconsole = bareos.bsock.DirectorConsole(address=bareos_addr, port=bareos_port, password=bareos_pass)
        exter = directorconsole.call(command)
        directorconsole.close()
        return exter
    except bareos.exceptions.AuthenticationError:
        logging.info('bareos-dir not available. Check name or password')
        sys_exit(1)

def make_job_dict(job_dict):
	"""Выгрузить из bareos director
	список имен Job включенных и имен Shaduler"""

    for job_conf in str(job_dict).split('Job {\n'):
        if "Schedule" in job_conf and "Enabled = no" not in job_conf:
            for l_conf in job_conf.split('\n'):
                if "Name" in l_conf:
                    job_name = l_conf.replace('  Name = ', '').replace('"', '')
                elif "Schedule" in l_conf:
                    shad_name = l_conf.replace('  Schedule = ', '').replace('"', '')
            yield job_name, shad_name

def query_last_time(list_job, lvl):
	"""Проверяем время последнего запуска заданий
	определенного уровня по списку"""

    last_time = {}
    for n, t in db_connect("SELECT name, max(starttime) AS time\
                            FROM job\
                            WHERE type='B'\
                            AND level='{0}'\
                            AND jobstatus IN ('T','C')\
                            AND name IN {1}\
                            GROUP BY name".format(lvl, list_job)):
        last_time[n] = t
    return last_time

def find_sched_schema(dict):
	"""Разбор схемы запуска"""

    for job, backup_schema in dict.items():
        split_schema = [x for x in backup_schema.split('_')]
        split_schema.reverse()
        yield job, split_schema

def parse_launch_scheme(job, list_sh):
	"""Разбор и вызов функции согласно
	схемы запуска"""

    global kwargs
    for eter in list_sh:
        level = dict_level.get(re_findall(r'^F|^D|^I', eter)[0])
        number_days = re_findall(r'\d\d?\d?', eter)[0]
        kwargs.clear()
        kwargs[dict_period.get(re_findall(r'h|d|w|m|y', eter)[0])] = int(number_days)

        if globals()[level](job, level) == 0:
            return

def Full(job, lvl):
	"""Проверка необзодимости запуска и вызов стартующей функции"""

    if last_time_Full.get(job) == None:
        runner(job, lvl)
        return 0
    if last_time_Inc.get(job) <= one_day \
            and last_time_Diff.get(job) <= one_day \
            and realtime >= last_time_Full.get(job) + relativedelta(**kwargs):
        runner(job, lvl)
        return 0

def Differential(job, lvl):
	"""Проверка необзодимости запуска и вызов стартующей функции"""

    if last_time_Diff.get(job) == None:
        runner(job, lvl)
        return 0
    if last_time_Full.get(job) <= one_day \
            and last_time_Inc.get(job) <= one_day \
            and realtime >= last_time_Diff.get(job) + relativedelta(**kwargs):
        runner(job, lvl)
        return 0

def Incremental(job, lvl):
	"""Проверка необзодимости запуска и вызов стартующей функции"""

    if last_time_Inc.get(job) == None:
        runner(job, lvl)
        return 0
    if last_time_Full.get(job) <= one_day \
            and last_time_Diff.get(job) <= one_day \
            and realtime >= last_time_Inc.get(job) + relativedelta(**kwargs):
        runner(job, lvl)
        return 0

def runner(job, lvl):
	"""Проверка количества запущенных заданий
	и старт задания"""

    global run_jobs
    if run_jobs < jobs_max:
        run_jobs += 1
        logging.info('Starting job {0} with level {1}'.format(job, lvl))
        bareos_connect('run job={0} level={1} yes'.format(job, lvl))
    else:
        logging.warning('Job {0} with level {1} in the launch queue'.format(job, lvl))
    return

if __name__ == "__main__":
    try:
        try:
            logging.basicConfig(level=logging.INFO,\
                                filename='/var/log/bareos/alter_schedule.log',\
                                format='%(asctime)s %(levelname)s:%(message)s')
        except IOError:
             print 'bareos_schedule log file error'
             sys_exit(1)
    
    
        get_lock('bareos_scheduler')
        path = "/etc/bareos/schedule.conf"
        realtime = datetime.today()
        one_day = realtime - timedelta(days=1)
        getConfig(path)

        job_dict = {}
        regex = '^(F|D|I)(\d\d?\d?)(h|d|w|m|y)_?(F|D|I)?(\d\d?\d?)(h|d|w|m|y)?_?(F|D|I)?(\d\d?\d?)(h|d|w|m|y)?'
        for job_name, shad_name in make_job_dict(bareos_connect('show jobs')):
            if re_match(regex, shad_name) is not None:
                job_dict[job_name] = shad_name
            else:
                logging.error('Job {0} error. Invalid Schedule name!'.format(job_name))
    
        run_jobs = 0
        try:
            for i, in db_connect("SELECT name FROM job \
                                WHERE jobstatus IN ('R','B','D','F','S','m','M','s','j','c')"):
                run_jobs += 1
                del job_dict[i]
        except KeyError:
            logging.error('{0} job has been launched that is not in the list!'.format(i))
    
        if len(job_dict.keys()) > 0:
            last_time_Full = query_last_time(str(job_dict.keys()).replace('[', '(').replace(']', ')'), 'F')
            last_time_Diff = query_last_time(str(job_dict.keys()).replace('[', '(').replace(']', ')'), 'D')
            last_time_Inc = query_last_time(str(job_dict.keys()).replace('[', '(').replace(']', ')'), 'I')
            dict_period = {'h':'hours', 'd': 'days', 'w': 'weeks', 'm': 'months', 'y': 'years'}
            dict_level = {'F': 'Full', 'D': 'Differential', 'I': 'Incremental'}
            kwargs = {}
        else:
            logging.warning('The launch list is empty. There is nothing to start')
            sys_exit(0)
    
        for j, s in find_sched_schema(job_dict):
            parse_launch_scheme(j, s)
    
        logging.info('Schedule complete\n')
        sys_exit(0)

    except Exception:
        logging.exception("Exception occurred")


# Закрыть соединение с db