#!/usr/bin/python2
# -*- coding: UTF-8 -*-

import os
import sys
from time import sleep

try:
    cache = sys.argv[1]
    datedel = sys.argv[2]
except:
    print "Usage: {0} <sputnik|ria> <date in Y-m-d format>".format(sys.argv[0])
    sys.exit(1)

rhgs_path = "/www/rhgs_media/rescaler_cache"
log_path = "/www/logs/ria22-rescaler"

if cache == "sputnik":
    src_dir = "{0}/sputnik_rescaler/".format(rhgs_path)
    src_file = "{0}/sputnik-rescaler/php/cache-{1}.log".format(log_path, datedel)
elif cache == "ria":
    src_dir = "{0}/ria_rescaler/".format(rhgs_path)
    src_file = "{0}/ria2018-rescaler/php/cache-{1}.log".format(log_path, datedel)
else:
    print "sputnik or ria"
    sys.exit(1)

if not os.path.isfile(src_file):
    print "File {0} not found!".format(src_file)
    sys.exit(1)

if not os.path.isdir(src_dir):
    print "Directory {0} not found!".format(src_file)
    sys.exit(1)

pidfile = "/var/run/rescaler-cleaner-{0}.pid".format(cache)
if os.path.exists(pidfile):
    print "{0} already exists!".format(pidfile)
    sys.exit(1)
else:
    pid = open(pidfile, 'w')
    pid.close()

with open(src_file,'r') as files_list:
    list = files_list.read().splitlines()
    for f in list:
        print (src_dir + f.split()[1])
        try:
            os.unlink(src_dir + f.split()[1])
            sleep(0.02)
        except OSError:
            pass

log_arch = "{0}/cleaner/{1}_{2}.log".format(log_path, cache, datedel)
os.rename(src_file,log_arch)

os.unlink(pidfile)
