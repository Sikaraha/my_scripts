import re, json
from subprocess import Popen, PIPE

class LogParser(object):
    def init(self, options):
        pattern_pgsql_general = options["regex_pgsql_general"]
        pattern_pgsql_other = options["regex_pgsql_other"]
        self.regex_pgsql_general = re.compile(pattern_pgsql_general)
        self.regex_pgsql_other = re.compile(pattern_pgsql_other)
        self.counter = 0
        return True

    def deinit(self):
        return True

    def parse(self, log_message):
        match = self.regex_pgsql_general.match(log_message['MESSAGE'])
        if match:
            match = match.groupdict()
            if match.get('message_type') == 'statement' and match.get('command_type') in ['SELECT', 'UPDATE', 'DELETE', 'INSERT']:
                message_body = match.get('message_body').replace('#011', ' ').replace('#012', ' ')
                normalize_qurey, errs = Popen("/usr/bin/normalize6 {0}".format(json.dumps(message_body)), shell=True, stdout=PIPE).communicate()
                normalize_qurey = normalize_qurey.decode("utf-8")
                query_text = re.findall('^.*\;', normalize_qurey)
                query_parameter = re.findall('\{.*\}$', normalize_qurey)
                try:
                    match['qury_text'] = query_text[0]
                    match['query_parameter'] = query_parameter[0]
                    match['message_body'] = ''
                except IndexError:
                    match['qury_text'] = '{ERR}'
                    match['query_parameter'] = '{ERR}'
            for key, value in match.items():
                if value is not None:
                    if "pgsql_timestamp" in key:
                        value = str(value.replace(" ", "T", 1)) + "+0300"
                    log_message[key] = str(value)
            self.counter += 1
            # return True
            return print(log_message)
        else:
            match = self.regex_pgsql_other.match(log_message['MESSAGE'])
            if match:
                for key, value in match.groupdict().items():
                    if value is not None:
                        log_message[key] = str(value)
                self.counter += 1
                return True
        return False

options = {
    "regex_pgsql_general": "\\[\\d+\\]\\s(?P<pgsql_timestamp>\\d+-\\d+-\\d+\\s\\d+:\\d+:\\d+.\\d+)\\sMSK\\s\\[\\d+\\]:\\s\\[\\d+\\]\\suser=(?P<pgsql_user>[^,]+),db=(?P<pgsql_db>\\S+)\\s\\[(?P<transaction_backend_id>\\d+)/(?P<transaction_localxid>\\d+)\\]\\s\\[(?P<command_type>[A-z\\s]*)\\]\\s?(?:\\[[a-z0-9\\.]+\\])?\\s(?:LOG:)?\\s*(?:duration:)?\\s?(?P<duration>[0-9.]+)?\\s?(?:ms)?\\s*(?P<message_type>\\w+):?\\s?(?:\\S+:)?\\s*(?P<command_comment>\\/\\*[\\sa-z\\-]*(?P<haproxy_request_id>[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+)?.?\\*\\/)?\\s*(?P<message_body>.+)$",
    "regex_pgsql_other": "(?P<other_message>.*)"
}

log = LogParser()
log.init(options)

arg = {
    'MESSAGE': '[5989] 2021-04-20 17:32:24.762 MSK [36388]: [5981] user=ria22,db=ria22 [444/0] [SELECT] [607ee5c7.8e24] LOG:  duration: 0.322 ms  statement: SELECT "media_variant"."media_variant_id" AS "media_variant_id", "media_variant"."media_id" AS "media_id", "media_variant"."media_variant_mimetype" AS "media_variant_mimetype", "media_variant"."media_variant_height" AS "media_variant_height", "media_variant"."media_variant_width" AS "media_variant_width", "media_variant"."media_variant_type" AS "media_variant_type", "media_variant"."media_variant_filename" AS "media_variant_filename", "media_variant"."media_variant_meta_data" AS "media_variant_meta_data", "media_variant"."media_variant_views" AS "media_variant_views", "media_variant"."media_variant_external_url" AS "media_variant_external_url", "media_variant"."media_variant_filesize" AS "media_variant_filesize", "media_variant"."media_variant_is_active" AS "media_variant_is_active", "media_variant"."media_variant_external_id" AS "media_variant_external_id", "media_variant"."media_variant_date" AS "media_variant_date", "media_variant"."media_variant_import_id" AS "media_variant_import_id", "media_variant"."media_variant_prototype_uri" AS "media_variant_prototype_uri" FROM "media_variant" WHERE "media_id" IN (1515832504)'
}

log.parse(arg)
