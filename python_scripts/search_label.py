#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import sys, re

if __name__ == "__main__":

  list_param=sys.argv[1:]
  if list_param:
    for i in list_param:
      p = re.findall(r"[a-zA-Z]\w\d\d\d\dL6$", i)
      print(p[0])
  else:
    for i in iter(sys.stdin.readline, ''):
      p = re.findall(r"[a-zA-Z]\w\d\d\d\dL6$", i)
      try:
        print(p[0])
      except IndexError:
        pass
