#!/usr/bin/python2
# -*- coding: utf-8 -*-

import datetime
#import re
#import sslpsk
import psycopg2
import bareos.bsock

conn = psycopg2.connect(
    database="bareos",
    user="postgres",
    password="",
    host="10.24.13.13",
    port="5432"
)
print "Database opened!"

password = bareos.bsock.Password('bareos')
directorconsole = bareos.bsock.DirectorConsole(address='10.24.13.13', port=9101, password=password)
print "Bareos-dir connected!"

# Формируем список запущенных заданий
running = []
cursor = conn.cursor()
cursor.execute("SELECT name FROM job WHERE jobstatus IN ('R','B','D','F','S','m','M','s','j','c')")
for i in cursor.fetchall():
    running.append(", ".join(i))

# Формируем список заданий bareos-dir
show_jobs = str(directorconsole.call('show jobs'))
job_list = []
for job_name in show_jobs.split('\n'):
    if "Name" in job_name:
        job_name = job_name.replace('  Name = ', '').replace('"', '')
        job_list.append(job_name)
# Удаляем из списка заланий уже запущенные
if len(running):
    for running_job in running:
        if running_job in job_list:
            job_list.remove(running_job)

#Сопоставляем имя джоба с именем шедуллера?
print 'Включенные в шедуллер задания со схемой запуска:'
job_schedul = {}
for job in job_list:
    show_shad = str(directorconsole.call('show job={0}'.format(job)))
    for schedule in show_shad.split('\n'):
        if "Schedule" in schedule:
            schedule = schedule.replace('  Schedule = ', '').replace('"', '')
            job_schedul[job]=schedule
print job_schedul

print datetime.datetime.now()