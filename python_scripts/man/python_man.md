python_man
=============================

Вывести информацию о функции или методе

    help()

Вывести содержание модуля

    dir()

Список list
list неизменяемый обьект (методы делают его копию с изминениями)

    i = [1,2,3,4,5]
    i = list(rangre(1,6))
    i[:] - все значения
    i[5:] - срез индексов с пятого и до конца

Кортеж tuple
tuple = константа (не изменяемый тип)

    i = (1,2,3,4,5)
    a, b, c = 1, 2, 3 - присвоение через неявный кортэж

Коллекця dict
dict индексы приниают любое значение
выгружать значеня по ключу лучше методом get ('это не вызывет ошбки')
так же можно проверять оператором in ('a' in dict)
    
    i = {'a':1, 'b':2, 'c':3}
    for k, v in i
        print(k, v)

Множества set
set число может воторятся только 1 раз
    
    i = {1,2,3,4,5}

Однострочный цикл
    
    l = [i**2 for i in [2, 3, 4]]

Аргументы функции
---------------------

Переменная

    def fn(a=1, b=1, c=1):
        print(a b c)
    fn(a=2, c=2)

    >> 2 1 2

Кортеж
принимает не ограниченное количество аргументов

    def fn(*parans):
        for p in params:
            print(p)

Словарь
аргументы fn(key=value, key=value)

    def fn(**parans):
        for k, v in params.items():
            print(p, m)

Коментарии в функции
----------------------
Многосточный комментарий сразу после определения функции

    def fn(**parans):
        '''Help on function:

           description of the function
        '''
        for k, v in params.items():
            print(p, m)

Импорт модулей
---------------------
функции вызываются с указанем модуля

    import modile_name

функции вызываются без указания модуля

    from math import soc as math_cos, sin as math_sin

Пути к встроенным модулям
----------------------

    import sys
    sys.path

Функции
----------------------

    open('file', 'mode', encoding='utf-8')

r   - чтение
r+  - чтение и запись
w   - запись
w+  - запись и чтение
a   - дозапись
a+  - дозапись и чтение

после использования фаил необходимо закрыть

    close()

либо

    with open('file', 'mode', encoding='utf-8') as file:
        pass

Чтение посимвольно

    read(number)

Чтение строки

    readline(line number)

Чтение всех строк
    
    readlines()

Модули
---------------------------

os

    os.getcwd()                 - текущий каталог
    os.path.exists('name.file') - проверка пути
    os.path.isdir('name')       - проверка каталога
    os.path.isfile('name.file') - проверка файла
    os.listdir()                - листинг директории

sys

Обработка ошибок
--------------------------

Перехват ошибки

    try:
        1/0
    expect ValueError as e:
        print('ERROR 1')
    expect (EOFError, KeyboardInterrupt) as n:
        if type(n) == EOFError:
            print('ERROR 2')
        else:
            print('ERROR 3')
    expect:
        print('Непонятная ошибка')
    else:
        print('Результат')
    finally:
        print('The END!')
