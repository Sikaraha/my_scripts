#!/usr/bin/python3

def area_of_disk(radius):
	'''area_of_disk(number)
	Return area on disk
	'''
	return 3.14 * radius ** 2

def area_of_ring(outer, inner):
	return area_of_disk(outer) - area_of_disk(inner)

