#!/usr/bin/python3

import random

def monty_hall(num_int):
    win = 0
    for i in range(1, num_int):
        if random.randint(1,3) == random.randint(1,3):
            win +=1
    return win

def has_duplicates(lst):
    unq = set(lst)
    if len(unq) != len(lst):
        return True
    return False
 
def birthday(num_int):
    result = 0
    for i in range(0, num_int):
        bdays = [random.randint(1, 365) for i in range(23)]
        if has_duplicates(bdays):
            result += 1
    return result

