#!/usr/bin/python3



#####################

#import paradox
#print(paradox.monty_hall(10000))
#print(paradox.birthday(1000))

#####################

#import random
#
#def monty_hall(num_int):
#	win = 0
#	for i in range(1, num_int):
#		if random.randint(1,3) == random.randint(1,3):
#			win +=1
#	return win
#
#def has_duplicates(lst):
#    unq = set(lst)
#    if len(unq) != len(lst):
#        return True
#    return False
# 
#def birthday(num_int):
#	result = 0
#	for i in range(0, num_int):
#		bdays = [random.randint(1, 365) for i in range(23)]
#		if has_duplicates(bdays):
#			result += 1
#	return result
#
#print('Шанс выигрыша:', monty_hall(100), '%')
#print(birthday(100), '%')

#####################
#from random import randint
#from datetime import date
#
#users_list = []
#users_dict = {}
#
#for i in range(1, 24):
#	users_list.append(f'Man{i}')
#	i+=1
#
#for a in users_list:
#	yers = int(randint(2001, 2019))
#	month = int(randint(1, 12))
#	day = int(randint(1, 29))
#	berthday = date(yers, month, day)
#	users_dict.update(a)
#
#print(users_list)
#print(users_dict)

#####################
# Путь к модулям
#from sys import path
#
#print(path)

#####################
# Запуск кода из своего модуля(в том же каталоге что и main)

#import funcs
#
#o = float(input('Введите внешний радиус: '))
#i = float(input('Введите внутренний радиус: '))
#
#print(funcs.area_of_ring(o, i))
 
#####################

#def area_of_disk(radius):
#	'''area_of_disk(number)
#	Return area on disk
#	'''
#	return 3.14 * radius ** 2
#
#def area_of_ring(outer, inner):
#	return area_of_disk(outer) - area_of_disk(inner)
#
#o = float(input('Введите внешний радиус: '))
#i = float(input('Введите внутренний радиус: '))
#
#print(area_of_ring(o, i))
#
#help(area_of_disk)

#####################

#def seconds_per_day(days=1):
#	s =24 * days * 60 * 60  
#	return s
#
#nd = int(input('Введите кол-во дней: '))
#
#print(seconds_per_day(nd))

#####################
# Использовать имя файла в скрипте

#print(__file__)
#
#import sys
#print(sys.argv[0])

# Имя модуля можено использовать через:
# print(__name__)

#####################
# Перебор вывода
# Аналог case в bash
 
#hello = {
#	'ru':'Привет!',
#	'en':'Hi!',
#	'default':'Unknown language',
#}
#
#s = input('Введите код: ')
#
#greet = hello.get(s, hello['default'])
#
#print(greet)

#####################

#s = list(range(2, 5))

#l = [i**2 for i in range(2, 5) if i >= 2]

#print(l)

#####################

#Парадокс Монти Холла упрощенный вариант

#import random
#
#num = 100
#win = 0
#
#for i in range(1, num):
#    if random.randint(1,3) == random.randint(1,3):
#        win +=1
#
#print(f'Шанс проигрыша: {win}%')
#print(f'Шанс выигрыша: {num-win}%')
  
#####################
#n1 = 2
#n2 = 3
#
#if n1 > n2:
#	print('Больше')
#elif n1 < n2:
#	print('Меньше')
#else:
#	print('Равно')
#
#if True:
#	pass

###################
#from random import randrange
#
#n1 = randrange(1, 11)
#n2 = randrange(1, 11)
#
#ansver = input(f'Введите сумму {n1} + {n2}: ')
#test = ansver.replace('.', '', 1)
#
#if not test.isdigit():
#	 print('Нужно ввести число!')
#else:
#	if ansver == test:
#		ansver = int(ansver)
#	else:
#		ansver = float(ansver)
#
#	if int(ansver) == n1+n2:
#		print('Ok')
#	else:
#		print('Err')

#####################
#import random
# 
#n = random.randint(0, 10)
#print('Угадайте число от 1 до 10')
#while  True:
#	ansver = input('Введите число: ')
#	ansver = int(ansver)
#	if ansver == n:
#		print('Угадал!')
#		break
#	else:
#		print('Не угадал!')
#		if ansver > n:
#			print('Меньше')
#			continue
#		elif ansver < n:
#			print('Больше')
#			continue
