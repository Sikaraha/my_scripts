# -*- coding: utf-8 -*-

from tkinter import *
import tkinter.messagebox as box

window = Tk()
window.title('Моя программка')
frame = Frame(window)
entry = Entry(frame)

def tog():
    if window.cget('bg') == 'red':
        window.configure(bg='grey')
    else:
        window.configure(bg='red')

def dialog():
    var = box.askyesno('Message Box', 'Proceed?')
    if var == 1:
        box.showinfo('Yes Box', 'Proceeding...')
    else:
        box.showinfo('No Box', 'Canselling...')

def dialog_n():
    box.showinfo('Привет!', 'Привет {0}!'.format(entry.get()))

label = Label(window, text = 'Мой зайчик')
btn_tog = Button(window, text='Щелк!', command=tog)
#btn = Button(window, text='Нажми меня', command=dialog)
btn = Button(frame, text='Ввести имя', command=dialog_n)
btn_end = Button(window, text='Выход', command=exit)

label.pack(padx=200, pady=50)
#btn.pack(padx=150, pady=50)
#btn_tog.pack(padx=150, pady=20)
btn.pack(side=RIGHT, padx=5)
entry.pack(side=LEFT)
frame.pack(padx=20, pady=20)
btn_end.pack(padx=50, pady=50)
window.mainloop()
