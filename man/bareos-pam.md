manual bareos pam auth
==============

Создаем конфиг авторизации

cat > /etc/pam.d/bareos

	auth       required     pam_unix.so

Правим права на /etc/shadow (если мы будем пользоваться именно этим модулем)

	chmod 740 /etc/shadow
	chown :bareos /etc/shadow

Проверяем работу модуля (пользователь username должен присутствовать в системе)

	su - bareos -s /bin/bash
	pamtester bareos username authenticate

Создаем именнованную консоль для авторизации через pam

cat > bareos-dir.d/console/pam-console.conf

	Console {
	  Name = "PamConsole"
	  Password = "Secretpassword"
	  UsePamAuthentication = yes
	}

В идеале фаил пользователя должен создаваться скриптом

cat > bareos-dir.d/user/username.conf

	User {
	   Name = "username"
	   Password = "" # unsed because authenticated by PAM
	   Profile = "webui-admin"
	}


Приводим конфиг /etc/bareos-webui/directors.ini к виду

	[localhost-dir-pam]
	enabled              = "yes"
	diraddress           = "localhost"
	dirport              = 9101
	tls_verify_peer      = false
	server_can_do_tls    = false
	server_requires_tls  = false
	client_can_do_tls    = false
	client_requires_tls  = false
	pam_console_name     = "pam-webui"
	pam_console_password = "secret"

