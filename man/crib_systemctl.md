SystemD crib
====================================
Вывод дерева загруженных процессов
> pstree

Вывести время загрузки
> systemd-analyze

Вывод листинга всех активных сервисов
> systemctl list-units -t service

Вывод списка юнитов запуск которых закончился неудачей
> systemctl --failed

Проверка присутствия сервиса в автозагрузке
> systemctl is-enabled xxx.service

Запуск сервиса
> systemctl start xxx.service

Остановить сервис
> systemctl stop xxx.service

Завершить все процессы сервиса
> systemctl kill <service>

Проверка статуса 
> systemctl status xxx.service

Перевести сервер в Single mod
> runlevel1.target, rescue.target

systemd journal
------------------------------------

Вывести весь лог системы (размер можно изменить в /etc/systemd/journald.conf SystemMaxUse=)
> journalctl

> journalctl -fu sshd

Лог с момента перезагрузки
> journalctl -b

Вывести последние записи
> journalctl -f

Вывод определенной утилиты
> journalctl /path/file

Вывод конкретоного юнита
> journalctl -u netcfg


Service
====================================
Вывести все сервисы
> service --status-all