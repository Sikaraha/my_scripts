Fedora minimal install
=========================

Загружаем образ Fedora-Everything-netinst

	http://mirror.linux-ia64.org/fedora/linux/releases/32/Everything/x86_64/iso/Fedora-Everything-netinst-x86_64-32-1.6.iso

Во время установки в anaconda выбираем комплект минмальной установки

Установка X серсера

	dnf install @base-x

Установка минмального набора софта

	dnf install gdm gnome-shell nautilus gnome-terminal xdg-user-dirs-gtk fedora-workstation-backgrounds eog gnome-screenshot vim bzip2 tar wget mtr gparted

Включаем автозапуск gdm

	systemctl enable gdm

Включаем графику

	systemctl set-default graphical.target

Докачать недостающие языковые пакеты

	dnf install langpacks-ru

Проверка пакетов

	dnf check

