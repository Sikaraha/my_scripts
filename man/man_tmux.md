MAN TMUX
--------------------

Создать сессию 1

	tmux new -s session1
Подключиться к сессии

	tmux attach -t session1
Завершение сессии 1

	tmux kill-session -t session1
Завершить все сессии

	tmux kill-server
Список сессий

	tmux list
Выбрать сессию

	Ctrl + D + C
Новое окно

	Ctrl + B + C
Список окон

	Ctrl + B + W
Переключение

	Ctrl + B + O
Разделить окно погоризонтали

	Ctrl + B + "
Разделить окно по вертикали

	Ctrl + B + %
Закрыть окно

	Ctrl + B + X
Отключиться от сессии

	Ctrl + B + D


