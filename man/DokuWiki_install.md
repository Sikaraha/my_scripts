DockuWiki (php nginx) 

Скачиваем и распаковываем

	cd /usr/src/
	wget https://www.php.net/distributions/php-7.4.7.tar.xz
	tar xJf php-7.4.7.tar.xz
	cd php-7.4.7

Конфигурируем

	./configure --help
	./configure --prefix=/opt/php-7.4.7 --enable-fpm --with-openssl --enable-gd
	
Ставим

	make
	make install

Перемещаем файлы настройки в нужные директории

	cp php.ini-development /usr/local/php/php.ini
	cp /usr/local/etc/php-fpm.d/www.conf.default /usr/local/etc/php-fpm.d/www.conf
	cp sapi/fpm/php-fpm /usr/local/bin


vim /usr/local/php/php.ini

	cgi.fix_pathinfo=0
	short_open_tag = Off
	safe_mode = Off
	output_buffering = Off
	output_handler = (Включите в DokuWiki gzip_output функции вместо этого.)
	zlib.output_compression = Off
	implicit_flush = Off
	max_execution_time = 30
	max_input_time = 60
	max_input_vars = 10000
	memory_limit = 32M
	error_reporting = E_ALL & ~ E_NOTICE
	display_errors = Off
	display_startup_errors = Off
	log_errors = On
	variable_order = «EGPCS»
	register_argc_argv = Off
	magic_quotes_gpc = Off
	magic_quotes_runtime = Off
	magic_quotes_sybase = Off
	file_uploads = On
	upload_max_filesize = (должен соответствовать ожидаемому максимальному размеру загружаемых медиафайлов)
	session.use_cookies = 1
	session.cache_limiter = nocache
		[Extensions]
		extension=php_openssl.dll
	register_globals = Off

vim /usr/local/etc/php-fpm.d/www.conf

	user = www-data
	group = www-data

Старт php-fpm

	/usr/local/bin/php-fpm

NGINX
==============================

vim /usr/local/nginx/conf/nginx.conf

	location / {
    	root   html;
    	index  index.php index.html index.htm;
	}

Следующий шаг - убедиться, что .php файлы отправляются в бэкенд PHP-FPM. Введите следующее:

	location ~* \.php$ {
    	fastcgi_index   index.php;
    	fastcgi_pass    127.0.0.1:9000;
    	include         fastcgi_params;
    	fastcgi_param   SCRIPT_FILENAME    $document_root$fastcgi_script_name;
    	fastcgi_param   SCRIPT_NAME        $fastcgi_script_name;
	}

	/usr/local/nginx/sbin/nginx -s stop
	/usr/local/nginx/sbin/nginx

	rm /usr/local/nginx/html/index.html
	echo "<?php phpinfo(); ?>" >> /usr/local/nginx/html/index.php

Теперь откройте в браузере http://localhost. Должна отобразиться информация phpinfo().

Настройка DockuWiki

	ssl_certificate /etc/ssl/certs/example_com_pack.crt;
	ssl_certificate_key /etc/ssl/private/example_com.key;
	ssl_session_timeout 5m;
	ssl_ciphers "HIGH:!aNULL:!MD5 or HIGH:!aNULL:!MD5:!3DES";
	ssl_dhparam /etc/ssl/private/dhparam2048.pem;

	server {
	    listen               80;
	    listen                   [::]:80;
	    server_name          wiki.domain.example;
	    return 301 https://$server_name$request_uri;
	}
	 
	server {
	    listen [::]:443 ssl;
	    listen 443 ssl;
	 
	    server_name wiki.domain.example;
	 
	    # Maximum file upload size is 4MB - change accordingly if needed
	    client_max_body_size 4M;
	    client_body_buffer_size 128k;
	 
	    root /dokuwiki;
	    index doku.php;
	 
	    #Remember to comment the below out when you're installing, and uncomment it when done.
	    location ~ /(conf/|bin/|inc/|install.php) { deny all; }
	 
	    #Support for X-Accel-Redirect
	    location ~ ^/data/ { internal ; }
	 
	    location ~ ^/lib.*\.(js|css|gif|png|ico|jpg|jpeg)$ {
	        expires 365d;
	    }
	 
	    location / { try_files $uri $uri/ @dokuwiki; }
	 
	    location @dokuwiki {
	        # rewrites "doku.php/" out of the URLs if you set the userwrite setting to .htaccess in dokuwiki config page
	        rewrite ^/_media/(.*) /lib/exe/fetch.php?media=$1 last;
	        rewrite ^/_detail/(.*) /lib/exe/detail.php?media=$1 last;
	        rewrite ^/_export/([^/]+)/(.*) /doku.php?do=export_$1&id=$2 last;
	        rewrite ^/(.*) /doku.php?id=$1&$args last;
	    }
	 
	    location ~ \.php$ {
	        try_files $uri $uri/ /doku.php;
	        include fastcgi_params;
	        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	        fastcgi_param REDIRECT_STATUS 200;
	        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
	        # fastcgi_pass unix:/var/run/php5-fpm.sock; #old php version
	    }
	}

Перезагрузить nginx и php-fpm

	service php7.0-fpm reload && service nginx reload
	# service php5-fpm reload && service nginx reload # for the older php5 version

