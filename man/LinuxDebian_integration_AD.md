Интерграция Linux(Debian) c доменом AD Windows
=================================================

Настраиваем DC Windows
-------------------------------------------------

1. Ставим Windows Server 2016
2. Разворачиваем роль DC
3. Натсраиваем первый домен леса
	- Задаем имя - name.zone
	- Выставляем совместимость(придобавлении в другой лес), задаем пароль локального пользьвователя DC Администратор
    - пропускам делегировние домена
    - задаем NetBIOS name для домена (NAME)
    - задаем расположение рабочих каталог AD (база LDAP, логи, политики)
    - Стартуем сгенерированный скрипт.

4. Натсройка DNS Service
	- Для того что бы разрешить автоматическое доавление A-записи нового устройства в домене необходимо разрешить Не безопастное обновление зон (Non-Sequre and Sequre)
	(иначе нужно будет добавлять новые линукс машины в AD руками)
	Машины не введенные в домен всеравно нужн будет добавлять руками
	- DNS manager
	- Зоны прямого просмотра (Forvard Lookup Zones)
	  name.zone -> Свойства -> Главная -> Динамическое обновлеие: Nonsequre and sequre
	- Зоны обратного просмотра (Reverse Lookup Zones)
	  Новая зона
	  ->
	  ->
	  ->
	  ->
	  Добавляем адрес сети 111.111.111
	  Разрешаем безопастный и небезопастный метод обновления (Allow both nonsequre and sequre dynamic updates)

Настраиваем авторизацию сервиса в AD
------------------------------------------------

1. Создаем пользоваеля для авторизации сервиса (squid) и привязываем к нему принципала и выружаем все в фаил.
	(?когда создали принципала?)
	- Создаем пльзователя gatehttp
	- в PS выполняем		

		C:\Windows\system32>ktpass -princ HTTP/gate.name.zone@NAME.ZONE -mapuser gatehttp -pass 'Pa$$word' -out c:\gatehttp.keytab
	
	- Выружаем с пмощью ssh созданный keaytab-фаил на шлюз (gate) в /root/gatehttp.keytab	
	- Подключаемся по ssh на сервер gate
	- выполняем:

		ktutil
		rkt /root/gatehttp.keyta
		list
		wkt /etc/krb.keytab
		quit
		klist -ek /etc/krb5.keytab
		chmod +r /etc/krb5.keytab
		sysemctl restart squid.service

	- Поле этого нужо указать вбраузере proxi и авторизация будет проходить через kerberos ticket

2. Через winbind

		apt instal winbind libnss-winbind
		ls -lh /etc/samba/smb.conf (создан при установке winbind)
		cat > /etc/samba/smb.conf
		
		[global]
		workgroup = NAME
		security = ADS
		realm = NAME.ZONE
		kerberos method = system keytab
		winbind use default domain = yes

Эта часть конфига позволяет аутентифицироваться пользователю


  - Добавляем машину в домен AD

		net ads join -U Администратор
		service winbind restart

  - Проверяем связь с AD

		wbinfo -t

  - Cмотрим пользователей AD

		wbinfo -u

  - Смотрим группы в AD

		wbinfo -g

  - Cоздаем систмный keytab

		net ads keytab create -U Администратор

  - Создаем принципала для сервиса sqid (gatehttp)

		net ads keytab add HTTP -U Администратор

  - Проверяем содержимое системного keytab

		klist -ek /etc/krb5.keytab

  - На серверах с развернутыми слжбами необходимо дать права на четение системного keytab (в идеале только нужной группе)

		chmod +r /etc/krb5.keytab 

  - И после этого перезапустить целевые службы

		sysemctl restart squid.service

Натсраиваем Linux client
-------------------------------------------------
Необходимо созать в AD пользователя (есл мы будем авторизоваться в почтовом клиенте необходимо указать его почту в учетно записи)

		apt instal winbind libnss-winbind
		ls -lh /etc/samba/smb.conf (создан при установке winbind)
		cat > /etc/samba/smb.conf
		
		[global]
		workgroup = NAME
		security = ADS
		realm = NAME.ZONE
		kerberos method = system keytab
		winbind use default domain = yes
		winbind enum users = yes
        winbind enum groups = yes
        winbind cache time = 36
        idmap config * : range = 20000-40000
        template homedir = /home/%U
        template shell = /bin/bash

Тут стоит решить дилему с правами на доступ к каталогам пользователей AD

Добавляем машину в домен AD

		net ads join -U Администратор
		service winbind restart

Проверяем связь с AD

		wbinfo -t

Так же необходимо указать что пароль пользователя необходимо брать из winbind, а не из /etc/shadow

		vim /etc/nsswitch.conf
		...
		passwd:         compat winbind
		group:          compat winbind
		shadow:         compat winbind
		...

Тепер на машиу можно осуществлять локальный вход под пользоватлем домена


