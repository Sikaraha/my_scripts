crib multipath raid
=======================================
Посмотреть подключенные устройства (в том числе разными маршрутами)
> lsscsi-s

Просмотреть подключенные через multipath девайсы
> ls -lh /dev/mapper/

Просмотреть WWNID подключенных через multipath
> multipath -l

Конфиг multipath /etc/multipath.conf (обязательно редактировать обнаружение и блеклист)

Собираем массив
> mdadm --create --verbose /dev/md0 --level=1  --raid-devices=2 /dev/mapper/mpatha /dev/mapper/mpathb

Форматируем раздел на массиве
> mkfs -t xfs /dev/md0

Монтируем раздел
> mount /dev/md0 /mnt/md0

Монтируем при загрузке
> echo "/dev/md0 /mnt/md0 xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0" >> /etc/fstab

Сохраняем конфиг mdadm (без конфига устройства блокируются ?dmraid_driver или multipath?)
> mdadm --detail --scan > /etc/mdadm.conf 

И записать изменения в загрузочный образ (не исполнял ?update - debian?)
> update-initramfs -u

В дистриб RH для редиктирования initramfs используют
> darkut

ПРИ ЛЮБЫХ МАНИПУЛЯЦИХ С initramfs НЕ ЗАБУДЬ СДЕЛАТЬ ЕГО РЕЗЕРВНУЮ КОПИЮ 
> cp /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r).img.$(date +%m-%d-%H%M%S).bak

Вывести список загруженных в данный момент модулей ядра
> /sbin/lsmod