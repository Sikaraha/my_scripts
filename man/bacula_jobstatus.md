JobStatus
----------------------
| Статус  | Значение                            |
|---------|-------------------------------------|
| Т       | Завершается нормально
| C       | Создан, но еще не запущен
| R       | Выполняется
| B       | Заблокирован
| E       | Завершено с ошибкой
| e       | Нефатальная ошибка
| f       | Фатальная ошибка
| D       | Проверка различий
| A       | Отменено пользователем
| F       | Ожидание File демона
| S       | Ожидание Storage демона
| m       | Ожидание монтирования новго Volume
| М       | ожидание монтирования
| s       | Ожидание хранилища
| j       | Ожидание ресурса Job
| c       | Ожидание ресурса Client
| d       | Ожидание Maximum jobs
| t       | Ожидание старта
| p       | Ожидание завершения работы с более высоким приоритетом
| W       | Завершено с предупреждениями

JobType
----------------------
| Тип       | Значение                            |
|-----------|-------------------------------------|
| B         | Backup
| M         | Previous job that has been migrated
| V         | Verify
| R         | Restore
| c         | Console
| C         | Copy
| I         | Internal system job
| D         | Admin job
| A         | Archive
| g         | Migration
| S         | Scan


Backup Level
----------------------
| Тип       | Значение                            |
|-----------|-------------------------------------|
| F         | Full backup
| I         | Incremental backup
| D         | Differential backup
| C         | Verify from catalog
| V         | Verify init db
| O         | Verify volume to catalog
| d         | Verify disk to catalog
| A         | Verify data on volume
| B         | Base job
|           | Restore or admin job 
 
VoLStatus
----------------------
| Статус    | Значение                            |
|-----------|-------------------------------------|
| Used      | Закончен
| Append    | Готов к записи
| Full      | Полный
| Recycle   | Подготовлен к перезаписи
| Purged    | Очищен
| Archive   | Архив
| Error     | Ошибка
| Disabled  | Отключен

        SELECT Job.JobId AS jobid,
               Job.Name  AS jobname,
               Job.Level     AS level,
               Job.StartTime AS starttime,
               Job.JobFiles  AS jobfiles,
               Job.JobBytes  AS jobbytes,
               Job.JobStatus AS jobstatus,
               (CURRENT_TIMESTAMP - starttime) AS duration,
               Client.Name AS clientname
          FROM Job INNER JOIN Client USING (ClientId)
          WHERE                                      
            JobStatus IN ('R','B','D','F','S','m','M','s','j','c');


          SELECT volumename,volbytes,volfiles
          FROM media
          WHERE volstatus IN ('Used','Full','Recycle','Purged','Error')
            AND volbytes <= 1024
            AND volfiles = 0;
