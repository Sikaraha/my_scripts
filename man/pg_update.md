Обновление PG_UPGRADE
=========================

Перед обновлением необходимо остановить standby.

1. Ставим новую версию postgres
(пакеты стоит брать с postgres.org)

		yum install postgresql10 postgresql10-libs postgresql10-server postgresql10-devel

2. Ставим аналогичные плагины (pg_repack). Либо удаляем их до обновления.
		
		DROP EXTENSION pg_repack CASCADE;

3. Запускаем check
(стоит использовать pg_update из новой версии postgres)

		/usr/pgsql-10/bin/initdb --locale=C --lc-collate=C --lc-ctype=en_US.UTF-8 -E UTF8 -D /data/pgsql/10/		
		/usr/pgsql-10/bin/pg_upgrade --check --link -b /usr/pgsql-9.2/bin/ -B /usr/pgsql-10/bin/ -d /data/pgsql/9.2/ -D /data/pgsql/10/

4. Останавливаем старую версию кластера
				
		systemctl stop postgresql-92.service
		/usr/pgsql-10/bin/pg_upgrade --link -b /usr/pgsql-9.2/bin/ -B /usr/pgsql-10/bin/ -d /data/pgsql/9.2/ -D /data/pgsql/10/

5. Переносим конфиги в новую версию
6. Стартуем новую версию

		systemctl enable postgresql-10.service
		systemctl start postgresql-10.service

7. Ставим новую версию postgres на реплике (но не инициализируем кластер!)

		yum install postgresql10 postgresql10-libs postgresql10-server postgresql10-devel
		systemctl enable postgresql-10.service


8. На реплике запускаем
		
		pg_basebackup -h backup1-1.rian.off -U postgres -D /data/pgsql/10  -Xf -P




------------------------------------------------
. Запускаем rsynk старой версии и новой с мастера на слейв (для того что бы избежать копирования файла данных, а скопировать только линки)

		rsync -alHv --delete --size-only --no-i-r /data/pgsql/ backup1-2.rian.off:/data/pgsql/

. Стартуем

		systemctl start postgresql-10.service

. Заменяем в задании bacula-db (в связи с изменениями в 10ой версии)
		Command = "/bin/sh -c '/usr/bin/psql -U postgres -h localhost -c \"SELECT pg_xlog_replay_pause();\"'"
		Command = "/bin/sh -c '/usr/bin/psql -U postgres -h localhost -c \"SELECT pg_wal_replay_pause();\"'"

		Command = "/bin/sh -c '/usr/bin/psql -U postgres -h localhost -c \"SELECT pg_xlog_replay_resume();\"'"
		Command = "/bin/sh -c '/usr/bin/psql -U postgres -h localhost -c \"SELECT pg_wal_replay_resume();\"'"


