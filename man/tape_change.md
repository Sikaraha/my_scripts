Смена лент
=================================================================

Посмотреть слоты в ченжере

	mtx -f /dev/changer1 status

Переместить старые кассеты в mailslots и обратно чистые

    mtx -f /dev/changer1 transfer 89 34

Присвоить метки имена volumes кассетам в bareos

    label storage=Storage-oracle-LTO-4 pool=pool-lto6-cpp slot=8 barcodes yes

Обновить имена volumes в базе bareos

    update slots storage=Storage-oracle-LTO-4 drive=0

Переместить кассету в другой pool bareos

    update volume=ML6282L6 pool=pool-lto6-cpp

вывести касссеты и их статусы в pool bareos

    list media pool=pool-lto6-cpp-1

Извлеч кассету из drive bareos

    release storage=Storage-oracle-LTO-6 

Очистка испорченной касеты
----------------------------------
Заружаем кассету из 40 слота в drive 0

    mtx -f /dev/changer1 load 40 0

Перематываем кассету в начало

    mt -f /dev/tape4 rewind

Ставим метку записи в начало кассеты

    mt -f /dev/tape4 weof

Выгружаем кассету из drive 0 обратно в 40 слот

    mtx -f /dev/changer1 unload 40 0

Полная очистка ленты
----------------------------------
    mt -f /dev/st0 eraze

