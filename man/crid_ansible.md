CRIB ANSIBLE
=====================

ansible ad-hoc
---------------------
-i путь к inventory file
-o вывод в одну строку
-m модуль
-a аргументы модуля

Доступнсть:

    ansible host/group -m ping

Версия ОС:

    ansible host/group -m setup -a 'filter=ansible_distribution*'

    "ansible_facts": {
        "ansible_distribution": "RedHat", 
        "ansible_distribution_file_parsed": true, 
        "ansible_distribution_file_path": "/etc/redhat-release", 
        "ansible_distribution_file_search_string": "Red Hat", 
        "ansible_distribution_file_variety": "RedHat", 
        "ansible_distribution_major_version": "6", 
        "ansible_distribution_release": "Santiago", 
        "ansible_distribution_version": "6.10"


playbook
--------------------

Всегда начинается с 

    ---

Повышаем привилегии:

    become: yes/no

Модули

Вывести сообщение

    debug:
      msg:

Модуль сравнения: (выполяем задачу только на опреденные дистрибутивах определнной мажорной версии)
    
    when: ansible_distribution == "RedHat" and ansible_distribution_major_version == "6"

Запуск в случае внесения изминений текущей задачей

    notify:
      - Handler name

    handlers:
      name: Handler name


Заполнение шаблона данными:

    - template:
        src: template.file
        dest: /path/dir/
        owner: root
        group: root
        mode: 0644


Замена строки в файле:

    lineinfile:
      dest: /path/dir/tmp.file
      regexp: '^This'
      insertafter: '^#This'
      line: 'This a comment'
      state: present

