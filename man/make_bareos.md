Ставим необходимые пакеты

	yum install mtx wget gtest gcc-c++ zlib-devel fastlz-devel libacl-devel openssl-devel readline-devel jansson jansson-devel librados2-devel php pam pam-devel

Создаем директории

	mkdir /usr/src/build /opt/bareos-Release-18.2.7

Скачиваем исходники
	cd /usr/src/
	wget https://github.com/bareos/bareos/archive/Release/18.2.7.tar.gz

Распаковываем

	tar -xzf 18.2.7.tar.gz
	cd /usr/src/build

Конфигурируем

	cmake3
			-D CMAKE_INSTALL_PREFIX:PATH=/opt/bareos-Release-18.2.7/
			-D postgresql=yes
			-D sysconfdir="/opt/bareos-Release-18.2.7/scripts/system/"
			-D sbindir="/opt/bareos-Release-18.2.7/bin/"
			-D confdir="/opt/bareos-Release-18.2.7/etc/"
			-D logdir="/opt/bareos-Release-18.2.7/log/"
			-D libdir="/opt/bareos-Release-18.2.7/lib64/bareos/"
			-D backenddir="/opt/bareos-Release-18.2.7/backends/"
			-D plugindir="/opt/bareos-Release-18.2.7/plugins/"
			-D scriptdir="/opt/bareos-Release-18.2.7/scripts/"
			-D workingdir="/opt/bareos-Release-18.2.7/working/"
			-D piddir="/opt/bareos-Release-18.2.7/pid/"
			-D archivedir="/opt/bareos-Release-18.2.7/drive/"
			/usr/src/bareos-Release-18.2.7/

Собираем

	cmake3 --build . --target install

