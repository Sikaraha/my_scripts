Уменьшаем размер тома LVM с xfs на виртуальной машине vmware
=============================================================

Добавляем еще однин диск меньшего развера в интерфейсе vcenter
Проверяем появился ли о в системе
  
    ls -lha /dev/sd*
    pvdisplay 

Добавляем physical volume (pv)

    pvcreate /dev/sdc

Создаем новую volume group (vg)

    vgdisplay 
    vgcreate -s 32M v0 /dev/sdc

Создаем новый logical volume (lv)

    lvcreate -n www -l 100%VG v0
    lvdisplay 

Форматируем

    mkfs.xfs /dev/v0/www 

Проверяем процессы работающего с томом сервиса и останавливаем его

    ps ax|grep kafk[a]
    /www/conf/init.d/kafka_analytic stop
    ps ax|grep kafk[a]

Создаем точку монтирования и подключаем туда новый lv

    mkdir /mnt/www
    mount /dev/v0/www /mnt/www
    mount

Переносим данные со старого большого тома на новый маленький
  
    xfsdump -J - /dev/v1/www | xfsrestore -J - /mnt/www

Отключаем оба тома

    umount /www/
    umount /mnt/www/

Коментируем старый и добавляем новый том  автозагрузку
  
    vim /etc/fstab
      /dev/mapper/v0-www      /www    xfs     defaults        0 0

Монтируем новый том

    mount /www
    mount |grep www

Стартуем сервис живущий на томе

    /www/conf/init.d/kafka_analytic start
    ps ax|grep kafk[a]

Удаляем старый(большой) logical volume

    lvremove /dev/v1/www 
    lvdisplay 

Удаляем его volume group (vg)

    vgremove v1 
    vgdisplay 

Удаляем physical volume (pv)

    pvremove /dev/sdb
    pvdisplay

После этого можно удалить старый (большой) диск в интерфейсе vcenter
