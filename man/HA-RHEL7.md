RHEL7 HA
====================
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/high_availability_add-on_administration/s1-resourcegroupcreate-haaa

Установка на всех узлах кластера необходимых пакетов (включая corosync и pacemaker)

	yum install pcs pacemaker fence-agents-all -y

Демон pcsd используется с pcs-tool для настройки кластера и обмена данными между узлами. pcs-tool может управлять всеми аспектами Pacemaker и демона Corosync:
- Создание и настройка кластера Pacemaker/Corosync
- Изменить конфигурацию кластера во время его работы
- Удаленно настраивайте Pacemaker и Corosync, а также запускайте, останавливайте и отображайте информацию о состоянии кластера.

Для использования ПК вы должны установить пароль на каждом узле для идентификатора пользователя hacluster, который является учетной записью администратора ПК. Установите пароль для пользователя hacluster на всех узлах на «redhat». Команда будет жаловаться на «неверный пароль», но все равно установит его.

	# passwd hacluster

Перед настройкой кластера необходимо запустить демон pcsd и включить его при запуске на каждом узле.

	# systemctl start pcsd.service
	# systemctl enable pcsd.service

Следующая команда аутентифицирует пользователя hacluster на ha-node1.example.com для всех узлов в нашем примере трехузлового кластера. На ha-node1.example.com запустите (скопируйте команду без "#", но с обратной косой чертой):

	# pcs cluster auth \
	ha-node1.example.com \
	ha-node2.example.com \
	ha-node3.example.com

Теперь три узла настроены, и кластер может быть создан

Выполните следующую команду с ha-node1.example.com, чтобы создать кластер из трех узлов my_cluster , состоящий из узлов ha-node1.example.com , ha-node2.example.com и ha-node3.example.com . Это распространит файлы конфигурации кластера на все узлы кластера и запустит кластер.

	# pcs cluster setup --start --name my_cluster \
	ha-node1.example.com \
	ha-node2.example.com \
	ha-node3.example.com

Включить автостарт кластера при загрузке

	# pcs cluster enable --all
	ha-node1.example.com: Cluster Enabled
	ha-node2.example.com: Cluster Enabled
	ha-node3.example.com: Cluster Enabled

Так же автозагрузку можно оставить выключенной (что бы еисправная нода не пыалась присоединится к кластеру) и присоединять ноды командой:

	# pcs cluster start

Проверить состояние кластера

	# pcs cluster status

Ограниченный вывод стояния:

	pcs status resources - состояние ресурсов
			 	groups 	 - группы
			 	cluster  - состояние кластера
			 	corosync - 
			 	nodes [corosync|both|config] - состояние узлов

Fencing используется для отделения ноды от основных машин кластера (напримр для устраненя неисправностей - аналог физического отключения ноды на железных машинах)

Вы создаете устройство ограждения stonith

	pcs stonith create myapc fence_apc_snmp \
	ipaddr="zapc.example.com" pcmk_host_map="z1.example.com:1;z2.example.com:2" \
	pcmk_host_check="static-list" pcmk_host_list="z1.example.com,z2.example.com" \
	login="apc" passwd="apc"

Отобразить параметры устройства ограждения

	pcs stonith show myapc

Эксклюзивная активация группы томов в кластере
====================

Выполните следующую команду, чтобы убедиться, что в файле locking_type установлено значение 1 и use_lvmetad 0 /etc/lvm/lvm.conf. Эта команда также отключает и останавливает все процессы lvmetad.

	lvmconf --enable-halvm --services --startstopservices

Определите, vg в настоящее время настроены в локальном хранилище, с помощью команды. Она выведет список настроенных vg.

	 vgs --noheadings -o vg_name

Добавляем vg (записе volume_list в /etc/lvm/lvm.conf)

	volume_list = ["rhel_root", "rhel_home"]

Пересобраем initramfs что бы загрузочный образ не пытатлся активировать группу томов, управляемую кластером.

	dracut -H -f /boot/initramfs-$(uname -r).img $(uname -r)

Перезагружаем узел!

После сарта проверяем работу служб кластера

	pcs cluster status

Можно подождав, пока не перезагрузится каждый узел, запустить службы кластера на каждом из узлов с помощью команды

	pcs cluster start --all

Создание рескрсов кластера и групп ресрсов с помощю pcs
=====================

Этот вариант использования требует, чтобы вы создали четыре кластерных ресурса.

1. LVM ресурс my_lvm
2. Filesystem ресурс my_fs (/dev/my_vg/my_lv)
3. IPaddr2 ресурс - плавающий IP для apachegroup
4. apache ресурс с именем Website


Создаем группу ресурсов apachegroup и ее ресурсы

	pcs resource create my_lvm LVM volgrpname=my_vg exclusive=true --group apachegroup

После создания ресурс стартует автоматически

Проверяем состояние ресурса

	pcs resource show

Ресурс можно вручную запустить/остановить

	pcs resource enabe/disable

Добавляем оставшиеся ресурсы в группу

	pcs resource create my_fs Filesystem device="/dev/my_vg/my_lv" directory="/var/www" fstype="ext4" --group apachegroup

	pcs resource create VirtualIP IPaddr2 ip=198.51.100.3 cidr_netmask=24 --group apachegroup

	pcs resource create Website apache configfile="/etc/httpd/conf/httpd.conf" statusurl="http://127.0.0.1/server-status" --group apachegroup

Проверяем состояние кластера

	 pcs status

Отладка ресурсов

	pcs resource debug-start <resource>
	pcs resource debug-start

Перемещение ресрсов по узлам кластера
=============================
Вручную перевести ресурс в standby можно командой

	pcs node standby z1.example.com

Вернуть ресурс в прежнее состояние

	pcs node unstandby z1.example.com

Ресурсы имеют resource-stickiness значение, которое вы можете установить в качестве мета-атрибута при создании ресурса
По умолчанию это 0 (Это может привести к самопоизвольному перемещению ресурсов между узлами кластера для равномерного распределения нагрузки)
Рекомендуется присоздании класера установить resource-stickiness 1

Если требуется балансировка ресурсов (например добавлен новый узел), вы можете временно установить resource-stickiness 0

	pcs resource defaults resource-stickiness=0

Определения порядка запуска ресурсов

	pcs constraint order [action] resource_id then [action] resource_id [options]

	resource_id - имя ресурса
	action      - Действие (start/stop/promote/demote)
	kind        -
	              Optional  - Применяется только в том случае, если оба ресурса выполняют указанное действие.
	              Mandatory - Всегда (по умолчанию) Если первый указанный вами ресурс останавливается или не может быть заущен, второй указанный вами ресурс должен быть остановлен.
	              Serialize - Убедитесь, что для набора ресурсов одновременно не выполняются два действия остановки / запуска.
	symmetrical - true (умолчание) останавливать в обратном орядке

Пример

	 pcs constraint order VirtualIP then dummy_resource kind=Optional

