Полезные команды Postgres
====================================

Список баз кластера

	SELECT * FROM pg_database;
	psql -l
	\l

Список ролей

	SELECT * FROM pg_shadow;

Подключение к базе

	psql dbname
	\connect dbname
	\с dbname

Размер базы

	SELECT pg_size_pretty(pg_database_size('dbname'));

Размер таблицы

		SELECT pg_size_pretty(pg_relation_size('file'));
