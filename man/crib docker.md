crib docker
====================
https://docs.docker.com/install/linux/docker-ce/fedora/

Ставим docker

	sudo dnf remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine
	sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
	sudo dnf install docker-ce docker-ce-cli containerd.io
	sudo systemctl start docker

Скачиваем образ из dockerhub

	docker pull ubuntu:18.10

Список скачанных образов

	docker images

Список контенеров (в том числе завершенных)

	docker ps -a

Стартуем контейнер

	docker run <key> <image> <command>

Ключи

	-ir -запуск интерактивной консол в контейнере
	-p прокинуть порт в контейнер <8080:80>
	-v прокинуть каталаог в контейер <./tmp:/mnt/tmp>
	-d запуск в режиме демона (освобождает консоль)
	-P ???

Команды

	exec - исполнить команду внутри контейнера

Запустить ранее остановленный контейнер

	docker start <CONTAINER_ID>

Остановка и удаление контейнера и образа

	docker stop <CONTAINER_ID>
	docker rm <CONTAINER_ID>
	docker rmi <CONTAINER_ID>

Удаление всех остановленых контейнеров

	docker rm $(docker ps -a -q -f status=exited)

Просмотреть все  прокинутые порты

	docker port <srvice>

