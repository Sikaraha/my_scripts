Удаляем старые задания резервного копирования
==============================================================================================

Проверяем дату последнего заполненного джоба
> bconsole

Удаляем джоб из шедулинг скрипта
> vim /etc/bacula/scripts/jobs_schedule.1 

Удаляем из скрипта очистки
> vim /etc/bacula/scripts/del_old_volume_1.sh

Смотрим куда писались бекапы
> vim bacula-dir/jobname.conf 

Запускаем скрипт удаления заданий
> /etc/bacula/scripts/delete_job_from_backup.sh jobname-fd jobname

Проверяем изминения в текущем коммите
> svn st

Смотрим симлинк
> ll /mnt/backup_mirror_global/jobname

Смотрим файлы  по пути симлинка
> ll /mnt/md9/jobname/

Чистим файлы на дисках
> rm -rf /mnt/md9/jobname

Удаляем симлинк
> rm /mnt/backup_mirror_global/jobname

Коммитим изминения в svn
> svn ci -m "DIT-355" .

Смотрим  изминения в истории svn
svn log bacula-dir/archive/jobname.conf.old


rw-rdf1
rw-rdf2
rw-web1
rw-web2
rw-pd1
rw-deliv1
rw-deliv2
cert

F12m_D3m_I1d		очищать все старше Diff