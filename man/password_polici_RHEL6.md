Парольная политика RHEL6
===============================

	/etc/pam.d/system-auth

В соответствии с приведенным выше строкой, пароль должен содержать не менее 8 символов, минимум одну прописную букву, одну строчную букву, одну цифру и еще один специальный символ. 

	password requisite pam_cracklib.so try_first_pass retry=3 type= minlen=8 dcredit=-1 ucredit=-1 lcredit=-1 ocredit=-1


Запрещаем использовать старый пароль

	password     sufficient     pam_unix.so sha512 shadow nullok try_first_pass use_authtok remember=5

