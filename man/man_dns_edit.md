MAN DNS EDIT
--------------------

Ищим необходимую зону

	./zone-view.sh sputniknews.com

Изменем запси

	vim include/aixao3You

Коммитим изминения

	./zone-commit.sh

Перечитываем зону

	./zone-reload.sh ext

Проверяем

	dig mail.finaudit.expert ANY +noall +answer @8.8.8.8

	curl -L -H "Host: br.sputniknews.com" br.sputniknews.com.edgesuite.net

Подтвердить триггеры в zabbix!
-------------------------------