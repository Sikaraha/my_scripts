Обновление BACULA
==============================
1 Общие соображения
--------------------------------------------
Это руководство должно подходить для любого обновления - не только с версии 7.x до 9x.  
Вы должны обновить «Director» и «Storage», а так же «FileDaemon» (находящийся на одной машине с «Director» и «Storage») одновременно.  
Остальные клиенты могут обновляться постепенно.  
Файлы конфигурации старых версий могут быть сохранены без проблем.  
При обновлении BD необходимо пересеобрать «Bacula»!!!!

2 Обновление «Director» и «Storage» (Bacula Server ..., который также включает в себя собственный «FileDaemon»)
--------------------------------------------
2.1 Сделайте резервную копию /etc/bacula и init scripts

	mkdir /opt/update_bac /opt/update_bac/init
	cp -Rpd /etc/bacula /opt/update_bac/
	cp -Rpd /etc/init.d/bacula-* /opt/update_bac/init/

2.2 Резервное копирование базы данных (Catalog) (Скрипт делает pg_dump):
	
	/etc/bacula/make_catalog_backup -u bacula -p [пароль db]

2.3 Загружаем tar.gz c "Bacula.org" или "sourceforge.net" в каталог /usr/src/bacula-9.x.x.

	cd /usr/src/bacula/
	wget http://downloads.sourceforge.net/project/bacula/bacula/9.4.4/bacula-9.4.4.tar.gz?use_mirror=ufpr

2.4 Разархивируем в /usr/src/bacula/

	tar -xzf bacula-9.4.4.tar.gz
	cd /usr/src/bacula/bacula-9.4.4

2.5 Конфигугрируем и ставим bacula

	./configure  '--prefix=/opt/bacula-9.4.4' '--with-working-dir=/var/bacula/working' '--with-postgresql=/usr/pgsql-10' '--disable-conio' '--enable-readline' '--enable-smartalloc' '--enable-batch-insert' '--enable-lockmgr'
	make
	make install

2.6 Cоздаем (заменяем) симлинк

		rm -v /etc/bacula
		ln -s /opt/bacula-9.4.4/etc /etc/bacula

Переносим, правим и адаптируем старые файлы конфигурации (не забывает проверить пути к mtx-changer, bsmtp и другим используемым утилитам)

2.7 Генерируем init scripts (и вносим изминения из резервных копий)

заменяем пути к бинарникам и конфигам в старых init scripts или выполняем:

	make install-autostart

2.8 Теперь необходимо обновить базу данных:

	/opt/bacula/update_bacula_tables

2.9 Перезапускаем db.

	service postgres10 restart

2.10 Запускаем "Bacula"
	
	service bacula-dir start
	service bacula-sd start
	service bacula-fd start

Теперь подключаемся к «Bacula» через «bconsole» и выполняем job на выбор в качестве теста. Не забываем проверить связь с каким-либо клиентом через команду status client=.

И когда все наконец заработает как ожидалось... Радуемся. И начинаем обновлять клиентов...

3 Обновление клиента на удаленных машинах
--------------------------------------------
3.1 Делаем резервную копию /etc/bacula.

	mkdir /update_bac
	cp -Rpd /etc/bacula/update_bac/

3.2 Загружаем tar.gz из "Bacula" в папку /usr/src/
	
	cd /usr/src/bacula
	wget http://downloads.sourceforge.net/project/bacula/bacula/9.4.4/bacula-9.4.4.tar.gz?use_mirror=ufpr

3.3 Распаковываем в /usr/src/ .tar.gz
	
	tar -xzf bacula-9.4.4.tar.gz

3.4 Переходим в созданный каталог:

	cd /usr/src/bacula-9.4.4

3.5 Конфигурируем:

	./configure '--prefix=/opt/bacula-9.4.4' '--enable-client-only'
	make
	make install
	make install-autostart-fd

в systemd

	systemctl enable bacula-fd.service

в upstart

	chkconfig bacula-fd on

3.6 Перезапускаем «FileDaemon»:

	service bacula-fd restart
	systemctl restart bacula-fd.service 

Готово! Теперь переходим на сервер «Bacula» и проверяем работу: status client=.






Bacula установленная из исходников в сиситему...
--------------------------------------------


		cat > /home/nevdokimov/tmp.list

	/usr/local/lib64/libbaccats-5.2.13.so
	/usr/local/lib64/libbacsql-5.2.13.so
	/usr/local/lib64/libbacsql-5.2.2.so
	/usr/local/lib64/libbaccats-5.2.2.so
	/usr/lib64/libbac-7.2.0.so
	/usr/lib64/libbaccats.la
	/usr/lib64/libbacsql-7.2.0.so
	/usr/lib64/libbaccats-postgresql-7.2.0.so
	/usr/lib64/libbac.la
	/usr/lib64/libbaccfg-7.2.0.so
	/usr/lib64/libbacfind-7.0.5.so
	/usr/lib64/libbaccats-postgresql.la
	/usr/lib64/libbacfind.la
	/usr/lib64/libbacsql.la
	/usr/lib64/libbaccfg.la
	/usr/lib64/libbaccfg-7.0.5.so
	/usr/lib64/libbacfind-7.2.0.so
	/usr/lib64/libbac-7.0.5.so
	/usr/lib/libbacsql-5.2.2.so
	/etc/bacula
	/etc/bacula-20130930
	/etc/bacula-20141112
	/etc/bacula_5.2.2
	/etc/bacula.old
	/opt/bacula
	/opt/bacula-5.2.13
	/opt/bacula-5.2.2-main
	/opt/bacula-5.2.2
	/opt/bacula_7.0.5
	/opt/bacula-7.2.0
	/opt/bacula_7.2
	/opt/bacula-7.4.4
	/opt/bacula-7.4.7
	/opt/src/bacula-7.2.0
	/opt/src/bacula-7.4.4
	/root/bacula
	/sbin/bacula
	/sbin/bacula-dir
	/sbin/bacula-fd
	/sbin/bacula-sd
	/sbin/bconsole
	/sbin/bcopy
	/sbin/bextract
	/sbin/bls
	/sbin/bregex
	/sbin/bscan
	/sbin/bsmtp
	/sbin/btape
	/sbin/btraceback
	/sbin/bwild
	/sbin/dbcheck
	/usr/share/doc/bacula
	/usr/share/man/man1/bat.1.gz
	/usr/share/man/man1/bsmtp.1.gz
	/usr/share/man/man8/bacula.8.gz
	/usr/share/man/man8/bacula-dir.8.gz
	/usr/share/man/man8/bacula-fd.8.gz
	/usr/share/man/man8/bacula-sd.8.gz
	/usr/share/man/man8/bconsole.8.gz
	/usr/share/man/man8/bcopy.8.gz
	/usr/share/man/man8/bextract.8.gz
	/usr/share/man/man8/bls.8.gz
	/usr/share/man/man8/bpluginfo.8.gz
	/usr/share/man/man8/bregex.8.gz
	/usr/share/man/man8/bscan.8.gz
	/usr/share/man/man8/btape.8.gz
	/usr/share/man/man8/btraceback.8.gz
	/usr/share/man/man8/bwild.8.gz
	/usr/share/man/man8/dbcheck.8.gz

		cat /home/nevdokimov/tmp.list |while read f_name ;do mv -v ${f_name} ${f_name}.bac_to_delete ;done




	mv -v /usr/lib64/bpipe-fd.so /usr/lib64/bpipe-fd.so.bac_to_delete
	mv -v /usr/lib64/libbaccfg.so /usr/lib64/libbaccfg.so.bac_to_delete
	mv -v /usr/lib64/libbacpy.so /usr/lib64/libbacpy.so.bac_to_delete
	mv -v /usr/lib64/libbac.so /usr/lib64/libbac.so.bac_to_delete
	mv -v /usr/lib64/libbacfind.so /usr/lib64/libbacfind.so.bac_to_delete
	mv -v /usr/lib64/libbaccats-7.2.0.so /usr/lib64/libbaccats-7.2.0.so.bac_to_delete
	mv -v /usr/lib64/libbacsql.so /usr/lib64/libbacsql.so.bac_to_delete
	mv -v /usr/lib64/libbaccats-postgresql.so /usr/lib64/libbaccats-postgresql.so.bac_to_delete
	mv -v /usr/lib64/libbaccats.so /usr/lib64/libbaccats.so.bac_to_delete


--------------------------------------- standby

	/opt/bacula
	/opt/bacula7.2
	/opt/bacula-7.4.0.tar.gz
	/opt/bacula-7.4.7
	/opt/bacula-pgsql
	/opt/bak747.tar.gz
	/opt/bk747.tar.gz
	/etc/bacula.slave
	/etc/bacula-sd
	/etc/bacula.old
	/etc/bacula.bak
	/etc/bacula.after5213_update
	/etc/bacula.20160517
	/etc/bacula_2013-10-07
	/etc/bacula
	/sbin/bacula
	/sbin/bacula-dir
	/sbin/bacula-fd
	/sbin/bacula-sd
	/sbin/bconsole
	/sbin/bcopy
	/sbin/bextract
	/sbin/bls
	/sbin/bregex
	/sbin/bscan
	/sbin/bsmtp
	/sbin/btape
	/sbin/btraceback
	/sbin/bwild
	/sbin/dbcheck
	/usr/lib64/libbaccats-postgresql.la
	/usr/lib64/libbaccfg.so
	/usr/lib64/libbac-7.4.7.so
	/usr/lib64/libbaccats-7.2.0.so
	/usr/lib64/libbac-7.2.0.so
	/usr/lib64/libbaccats-postgresql-7.2.0.so
	/usr/lib64/libbac.so
	/usr/lib64/libbacsql-7.2.0.so
	/usr/lib64/libbaccats-postgresql.so
	/usr/lib64/libbak/libbaccats-postgresql.la
	/usr/lib64/libbak/libbaccfg.so
	/usr/lib64/libbak/libbaccats-7.2.0.so
	/usr/lib64/libbak/libbac-7.2.0.so
	/usr/lib64/libbak/libbaccats-postgresql-7.2.0.so
	/usr/lib64/libbak/libbac.so
	/usr/lib64/libbak/libbacsql-7.2.0.so
	/usr/lib64/libbak/libbaccats-postgresql.so
	/usr/lib64/libbak/libbacsql.so
	/usr/lib64/libbak/libbacfind-7.2.0.so
	/usr/lib64/libbak/libbaccats.so
	/usr/lib64/libbak/libbacsql.la
	/usr/lib64/libbak/libbacfind.so
	/usr/lib64/libbak/libbacsql.lai
	/usr/lib64/libbak/libbaccfg.la
	/usr/lib64/libbak/libbaccfg-7.2.0.so
	/usr/lib64/libbak/libbaccats.la
	/usr/lib64/libbak/libbacfind.la
	/usr/lib64/libbak/libbaccats.lai
	/usr/lib64/libbak/libbac.la
	/usr/lib64/libbacfind-7.4.7.so
	/usr/lib64/libbacsql.so
	/usr/lib64/libbacfind-7.2.0.so
	/usr/lib64/libbaccats.so
	/usr/lib64/libbacsql.la
	/usr/lib64/libbaccfg-7.4.7.so
	/usr/lib64/libbacfind.so
	/usr/lib64/libbacsql.lai
	/usr/lib64/libbaccfg.la
	/usr/lib64/libbaccfg-7.2.0.so
	/usr/lib64/libbaccats.la
	/usr/lib64/libbacfind.la
	/usr/lib64/libbaccats.lai
	/usr/lib64/libbac.la
	/usr/lib64/bpipe-fd.so
	


	find / \( -path "/proc" -o -path "/sys" -o -path "/mnt" \) -prune -o -name *bac_to_delete