Использование пакета Samba4 в качестве DC
===========================================

Ставим необходимые пакеты

		apt install -y samba winbind
		mv -v /etc/samba/smb.conf /etc/samba/smb.conf.sample
Инициализируем домен

		samba-tool domain provision --use-rfc2307 --interactive
Принимаем настройки по умолчанию:

		Realm [NAME.ZONE]:
		 Domain [NAME]:
		 Server Role (dc, member, standalone) [dc]:
		 DNS backend (SAMBA_INTERNAL, BIND9_FLATFILE, BIND9_DLZ, NONE) [SAMBA_INTERNAL]:
		 DNS forwarder IP address (write 'none' to disable forwarding) [111.111.111.111]:
Задаем пароль администратора домена:

		Administrator password: Pa$$w0rd
		Retype password: Pa$$w0rd
Проверяем конфигурацию и перезагружаемся

		testparm
		reboot
Команды для администрирования - samba-tool (можно управлять так но лучше использовать остнастки MS)

		samba-tool domain 
		samba-tool domain info 127.0.0.1 
		samba-tool dns
		samba-tool user add <username>
		samba-tool user
