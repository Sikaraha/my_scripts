

Division--CIAD 		- Центр исследований и анализа данных (Слинько Елена Викторовна <e.slinko@rian.ru>)
Division--DC 		- Дирекция медиадизайна, Центр дизайна коммуникаций и рекламы (Золотарев Денис Александрович <d.zolotarev@rian.ru>, Киселев Глеб Дмитриевич <g.kiselev@rian.ru>)
Division--DIT 		- Дирекция информационных технологий (ДИТ//Отдел сопровождения информационных систем <DEP_MIA_DIT_OtdSIS@msk.rian>, Ганин Сергей Вячеславович <s.ganin@rian.ru>, Белокуров Андрей Леонидович <a.belokurov@rian.ru>)
Division--DMP 		- Дирекция мобильных проектов (Приёмов Дмитрий Александрович <d.priemov@rian.ru>, ДМП-разработчики <DMP-razrabotchiki@msk.rian>)
Division--DRPO 		- Дирекция разработки ПО (ДРПО++Руководители направлений <WG_DRPO_Rukovoditeli_napravleniy@msk.rian>)
Division--DTSRI 	- Дирекция технического сопровождения радио и иновещания (Грибанов Эльдар Сергеевич <e.gribanov@rian.ru>) - ONLY SMRV
Division--OOMP 		- Отдел обеспечения мультимедийных проектов (Леконцев Владислав Юрьевич <v.lekontsev@rian.ru>)
Division--KG 		- 
Division--KZ
#Division--MMPC 		- Международный мультимедийный пресс-центр (Колева Ольга Николаевна) - ONLY WINDOWS

Ищем RHEL хосты без группы "Division--" (таких быть не должно! Если есть по хосту нужно искать владельца!)
==========================================

SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name IN ('System--Linux - RHEL6', 'System--Linux - RHEL7', 'System--Linux - RHEL8')
AND groups.groupid != 514
AND hosts.status < 3
AND hosts.host NOT IN (SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name LIKE 'Division--%'
AND groups.groupid != 514
AND hosts.status < 3);

Выбираем DEVEl хосты из "Division--CIAD" (по ним нужны уведомления)
==========================================
SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name = 'Level--DEV'
AND groups.groupid != 514
AND hosts.host NOT LIKE '-ha%'
AND hosts.host NOT LIKE '-URL'
AND hosts.host NOT LIKE 'rhg-storage%'
AND hosts.host NOT LIKE 'msrb-storage%'
AND hosts.host NOT LIKE 'smrv-storage%'
AND hosts.status < 3
AND hosts.host IN (SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name = 'Division--CIAD'
AND groups.groupid != 514
AND hosts.status < 3);

Выбираем "НЕ DEVEL" хосты из "Division--CIAD" (по ним нужны регаламенты)
==========================================
SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name = 'Division--DIT'
AND groups.groupid != 514
AND hosts.host NOT LIKE '-ha%'
AND hosts.host NOT LIKE '-URL'
AND hosts.host NOT LIKE 'rhg-storage%'
AND hosts.host NOT LIKE 'msrb-storage%'
AND hosts.host NOT LIKE 'smrv-storage%'
AND hosts.status < 3
AND hosts.host NOT IN (SELECT hosts.host FROM hosts, groups, hosts_groups
WHERE hosts.hostid = hosts_groups.hostid
AND groups.groupid = hosts_groups.groupid
AND groups.name = 'Level--DEV'
AND groups.groupid != 514
AND hosts.status < 3);


cat > /tmp/Division--CIAD_nodevel.sql
cat /tmp/Division--CIAD_nodevel.sql |psql -U zabbix -At


Возможно попытаться автоматизировать рассылку и обновление DELEV машин
1. Необходимо составить график обновления DEVEL машин.
2. Согласовать график по спискам рассылки из "Division--" (возможно стоит начать пользоваться механизмм согласования OUTLOOK)
3. Приступить к работам согласно согласованному графику (машины обнолвение которых не удалось завершить в указанный по графику день, согласовываются отдельно по окончании уже согласованных работ)

4. Необходимо составить график обновления PROD машин (ориентировочно 7-10 машин за день)
5. Согласовать график по спискам рассылки из "Division--" (Для машинс с БД в группу рассылки включаются ДИТ++Администраторы Oracle <WG_DIT_ORACLE@msk.rian>)
6. Приступить к работам согласно согласованному графику

Следовать данный инструкции для каждого "Division--"

Необходимо учитывать!
==============================
Обновления лушчше произволить с помощю ansible
Машины добавленные в балансировку желательно выводить из балансировки перед обновлением
На машинах с MongoDB необходисо перед рестартом штатно остановить Mongo
Во время обовления желательно отбновлять список репозиториев на машинах (удалять устаревшие, добавлять актуальные)





ШАБЛОН! T_KLMS, T_KLMSUI Division--DIT 

