Подключение принтера на Unix 
======================================================
Ставим cups если его нет на борту

    sudo apt|dnf install -y cups

Переходим в web интерфейс и авторизуемся 

http://localhost:631/

во вкладке Администрирование прожимаем:
Добавить принтер --> Windows Printer via SAMBA  --> 

    smb://${domain_name}/${domain_user}:${domain_pass}@ps3.msk.rian/KONICA%20MINOLTA%20Universal%20PS%20Secure

!!!Обращаю внимание что ${domain_name} ${domain_user} ${domain_pass} это соответствено имя домена пользователя и его пароль!!!

Название - Minolta_Zebra

В качестве драйвера можно выбрать идущий в комплекте KONICA MINOLTA 500/420/360PS(P)
Либо скачать PPD [скачать](https://d1d2pfgq9ayqeu.cloudfront.net/DL/201201/12063342/BH423PPDLinux_100000000MU.zip?Expires=1554280255&Signature=yzONxejSRUihQ52wmUvlIeig30mZzidzo2HimF~E7CnYlaW5okpR7pDaSpRx2D95bT2SJ7bmxyKUeKK0Gn6AhoVVG-f-J4CFrFySIS77nxpEoe5Hr9EM-w1RfNZolMbdDvrUSHjAW~HcQHRs1cVXMkbtaccVAehAJl3f0JOYU4w_&Key-Pair-Id=APKAIYSBPTP22BRPRZFA&ext=.zip)

--> Добавить принтер

Если добавить принтер стандартными средствами cups не получается для этого можно использовать сторонние пакеты например:

    sudo apt|dnf install -y system-config-printer python3-smbc

Проверить корректность конфига можно проверить в

    /etc/cups/printers.conf

Должно быть что то вида:

    <DefaultPrinter Minolta_Zebra>
    UUID urn:uuid:cc4d7896-0ccc-3644-462e-74dffa7bc13e
    Info Minolta_Zebra
    Location Zebra
    MakeModel KONICA MINOLTA 500/420/360PS(P)
    DeviceURI smb://${domain_name}/${domain_user}:${domain_pass}@ps3.msk.rian/KONICA%20MINOLTA%20Universal%20PS%20Secure
    State Idle
    StateTime 1554305882
    ConfigTime 1554305415
    Type 8401092
    Accepting Yes
    Shared No
    JobSheets none none
    QuotaPeriod 0
    PageLimit 0
    KLimit 0
    OpPolicy default
    ErrorPolicy stop-printer
    </DefaultPrinter>
