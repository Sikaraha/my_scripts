Редактируем разделы parted
=====================================

Смотрим информацию  разделах

	parted:
		print

Удаляем старый раздел

	parted:
		rm /dev/sdb

Создаем новую схему разметики

	parted:
		mklabel msdos

Создаем новый раздел

	parted:
		mkpart primary ext4 1MiB 100GB

Форматируем новый раздел

	mkfs.ext4 /dev/sdb1

Увеличиваем размер раздела

	parted:
		resizepart 1 200GB

Восстанавливаем потерянный раздел

	parted:
		rescue <НАЧАЛО>  <КОНЕЦ>

Редактируем размер lvm
======================================

Смотрим существующие PV

	pvdisplay

Смотрим состав VG

	vgdisplay

Создаем PV

	pvcreate /dev/sdb1

Создаем VG (если ее еще нет, или  если нам нужна еще одна)

	vgcreate -s 32M vg1 /dev/sda /dev/sdb2

Создаем LV в нашей VG (блочные устройства /dev/vg1/lv1 и /dev/vg1/lv2)

	lvcreate -n lv1 -L 20G vg1

Создаем на устройствах файловую систему

	mkfs.ext4 /dev/vg1/lv1

Добавляем новый PV в VG (далее нужно создать ещё один логический диск - lvcreate, или увеличить размер существующего - lvresize, либо переместить на этот pv lv со старого pv (если его нужно изьять) 

	vgextend vg1 /dev/sdc

Увеличить размер LV

	lvextend -L 12G /dev/vg1/home
	lvextend -l +100%FREE /dev/vg1/home

Увеличить размер файловой системы (xfs - только в смонтированном состоянии)

	xfs_growfs /home

Переместитьь LV на другой PV (убрать все разделы с физ диска)

	pvmove /dev/sdb
	pvmove -n data /dev/mapper/mpatha2 /dev/mapper/mpathb2

Вывести PV из VG

	vgreduce vg1 /dev/sdb

Удалить PV

	pvremove /dev/sdb

Снапшоты LVM

