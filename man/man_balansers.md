
Согласуем регламентные
=========================
mgr1

	balancers/scripts/reglament.sh

Правим конфиг
=========================

	/www/conf/ha-balancer/haproxy.cfg

	capture
	
	acl crimea_rian_url hdr_dom(host) -i crimea.rian.ru
	redirect prefix https://crimea.ria.ru code 301 if crimea_rian_url

Проверяем конфигурацию, рестартуем
=========================

	/etc/init.d/haproxy check
	/etc/init.d/haproxy restart

Коммитим изминения
=========================

	svn ci -m'DIT-' /www/conf/ha-balancer/haproxy.cfg

Вывести хост из балансировки
=========================
echo "disable server m-out/m1"|socat stdio /var/run/haproxy.admin

echo "enable server m-out/m1"|socat stdio /var/run/haproxy.admin