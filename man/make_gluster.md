
		lvmdiskscan

		ll /dev/mapper/ |grep "35000cca0ac003c6c"

		pvcreate /dev/dm-2 /dev/dm-3 /dev/dm-4 /dev/dm-5 /dev/dm-6 /dev/dm-7

		vgcreate v3 /dev/dm-4 /dev/dm-5

		lvcreate -n brick01 -l 4768766 v2

		mkdir -p /rhgs/brick01 /rhgs/brick02 /rhgs/brick03

for fstab

		/dev/v2/brick01 /rhgs/brick01 xfs defaults 0 0
		/dev/v3/brick02 /rhgs/brick02 xfs defaults 0 0
		/dev/v4/brick03 /rhgs/brick03 xfs defaults 0 0


		gluster peer probe 10.24.13.10
		gluster peer probe 10.24.13.11

		gluster volume create backup-stor disperse-data 4 redundancy 2 transport tcp
		10.24.13.10:/rhgs/brick01/backup-stor
		10.24.13.11:/rhgs/brick01/backup-stor
		10.24.13.10:/rhgs/brick02/backup-stor
		10.24.13.11:/rhgs/brick02/backup-stor
		10.24.13.10:/rhgs/brick03/backup-stor
		10.24.13.11:/rhgs/brick03/backup-stor

volume create: backup-stor: failed: Multiple bricks of a disperse volume are present on the same server. This setup is not optimal. Bricks should be on different nodes to have best fault tolerant configuration. Use 'force' at the end of the command if you want to override this behavior.


		gluster volume create backup-stor replica 2 transport tcp
		10.24.13.10:/rhgs/brick01/backup-stor
		10.24.13.11:/rhgs/brick01/backup-stor
		10.24.13.10:/rhgs/brick02/backup-stor
		10.24.13.11:/rhgs/brick02/backup-stor
		10.24.13.10:/rhgs/brick03/backup-stor
		10.24.13.11:/rhgs/brick03/backup-stor


Red Hat поддерживает форматирование логического тома с использованием файловой системы XFS на brick.

#Управление gluster

Добавляем в доверенный пул новую ноду

		gluster peer probe server2

Смотрим статус нод в доверенном пуле

		gluster peer status

Удаляем ноду из пула доверенных серверов

		gluster peer detach server4

Создайте том Red Hat Storage, используя подкаталоги в качестве brick.

		gluster volume create backup-stor replica 2 transport tcp
		10.24.13.10:/rhgs/brick01/backup-stor
		10.24.13.11:/rhgs/brick01/backup-stor
		10.24.13.10:/rhgs/brick02/backup-stor
		10.24.13.11:/rhgs/brick02/backup-stor
		10.24.13.10:/rhgs/brick03/backup-stor
		10.24.13.11:/rhgs/brick03/backup-stor

Стартуем том.

		gluster volume start distdata01

Проверьте состояние тома.

		gluster volume status distdata01

#Типы томов


DISTRIBUTED
-------------
Распределяет файлы по brick в томе
Используйте этот тип тома, если требования к масштабированию и избыточности не важны или предоставляются другими аппаратными или программными уровнями.

REPLICATED
-------------
Реплицирует файлы между brick в томе.
Используйте этот тип тома в средах, где критически важны высокая доступность и высокая надежность.

DISTRIBUTED REPLICATED
-------------
Распределяет файлы по отзеркаленным brick в томе.
Используйте этот тип тома в средах, где критически важны высокая надежность и масштабируемость. Этот тип тома предлагает улучшенную производительность чтения в большинстве сред.

ARBITRATED REPLICATED
-------------
Реплицируемый том с арбитражем аналогичен тому с двусторонней репликацией, поскольку он содержит две полные копии файлов в томе. Арбитражные тома имеют дополнительный brick арбитра для каждых двух блоков данных в томе. Арбитры не хранят файловые данные; они хранят только имена файлов, структуру и метаданные. Brick арбитра используют клиентский кворум для сравнения метаданных арбитра с метаданными других узлов, чтобы обеспечить согласованность в объеме и предотвратить условия разделения мозга.

DISPERSED
-------------
dispersed тома основаны на кодировании стирания. Erasure-coding (EC) - это метод защиты данных, при котором данные разбиваются на фрагменты, расширяются и кодируются избыточными частями данных и хранятся во множестве разных мест. Это позволяет восстанавливать данные, хранящиеся на одном или нескольких кирпичах, в случае сбоя. Количество блоков, которые могут потерпеть неудачу без потери данных, настраивается путем установки счетчика избыточности.
Для рассеянного тома требуется меньше места для хранения по сравнению с реплицированным томом. Это эквивалентно реплицированному пулу второго размера, но требует 1,5 ТБ вместо 2 ТБ для хранения 1 ТБ данных, когда уровень избыточности установлен на 2. В рассредоточенном томе каждый блок хранит некоторые порции данных и четность или избыточность. , Рассеянный том выдерживает потерю данных в зависимости от уровня избыточности.

DISTRIBUTED DISPERSED
-------------
Distributed dispersed тома тоже поддерживают erasure-coding (EC), как и dispersed тома. 
Количество кирпичей в распределенном распределенном объеме должно быть кратным (K + M). В этом выпуске поддерживаются следующие конфигурации:
6 brick с уровнем резервирования 2
10 brick с уровнем резервирования 2
11 brick с уровнем резервирования 3
12 brick с уровнем резервирования 4
20 brick с уровнем резервирования 4


##################
/etc/lvm/lvm.conf
dmsetup ls

Управлем gluster
=======================

	gluster

	help

Смотрим как собран gluster (тип томов gluster)

	gluster volume info

gluster volume create home replica 2 mgr-1.rian.off:/gluster/home mgr-2.rian.off:/gluster/home

gluster volume start home

gluster volume status home

mkdir /home

mgr-1.rian.off:/home /gluster/home glusterfs backupvolfile-server=mgr-2.rian.off:/home,_netdev 0 0

gluster volume create backup-stor replica 2 transport tcp
		10.24.13.10:/rhgs/brick01/backup-stor
		10.24.13.11:/rhgs/brick01/backup-stor