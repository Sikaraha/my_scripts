CRIB SUBVERSION
======================================================

В какой мы ветке и на какой серер смотрим
>svn info

Текущие изминения в каталоге
> svn st | status 

Затягиваем поледние изминения
> svn up

Просмотреть историю изминений
> svn log file

> svn log --diff

Добавить файл
> svn add file

Удалить файл
> svn del file

Lock/unlock чтобы файл никто не мог менять кроме нас
> svn lock file.txt

> svn unlock file.txt

Commit only my file!
>svn ci <file> <file> -m "comment"

Выяснить в каком коммите в последний раз изменялась строка
>svn blame <file> |grep <string>

Просмотреть конкретный коммит
>svn log -c <commit number> <file>