crib MDADM
======================================================
	mdadm [mode] [array] [options]

Режимы:

-A, --assemble — режим сборки

-B, --build — режим построения

-C, --create — режим создания

-F, --follow, --monitor — режим наблюдения

-G, --grow — режим расширения

-I, --incremental — режим инкрементальной сборки

-a --add - добавить диск в существующий массив

-r --remove - удалить устройство из массива

-f --fail - пометить диск неисправным

--force - пересобирает массив игнорируя состояние суперблоков(стоит быть очень осторожным )

--stop - отключает массив осводбождая все ресурсы

--zero-superblock - перезапись суперблока нулями

-D --detail - выводит дополнительную инфу

----------------------------------------------------------

Создание массива из указанных устройств (!!!НИКОГДА НЕ ПРИМЕНЯТЬ К СЕЩЕСТВУЮЩЕМУ МАССИВУ!!!)
----------------------------------------------------------
	mdadm --create --verbose /dev/md0 --level=1  --raid-devices=2 /dev/sda1 /dev/sdb1

После чего нужно создать конфигурационный файл mdadm.conf

    mdadm --detail --scan     /etc/mdadm.conf 

И записать изменения в загрузочный образ:		???

    update-initramfs -u

Пометка диска как сбойного 
-----------------------------------------------------------
Диск в массиве можно условно сделать сбойным, ключ --fail (-f):

    mdadm /dev/md0 --fail /dev/hde1

    mdadm /dev/md0 -f     /dev/hde1

Удаление сбойного диска
----------------------------------------------------------
Сбойный диск можно удалить с помощью ключа --remove (-r):

    mdadm /dev/md0 --remove /dev/hde1

    mdadm /dev/md0 -r       /dev/hde1

Добавление нового диска
-----------------------------------------------------------
Добавить новый диск в массив можно с помощью ключей --add (-a) и --re-add:

    mdadm /dev/md0 --add /dev/hde1

    mdadm /dev/md0 -a    /dev/hde1

Сборка существующего массива
------------------------------------------------------------
Собрать существующий массив можно с помощью mdadm --assemble. Как дополнительный аргумент указывается, нужно ли выполнять сканирование устройств, и если нет, то какие устройства нужно собирать.

    mdadm --assemble /dev/md0 /dev/hde1 /dev/hdf2 /dev/hdg1
    mdadm --assemble --scan

Если по каким либо причинам массив не собрался

    mdadm /dev/md0 --stop

И далее собираем его снова

Проверить состояние массива и получить информацию и массиве
------------------------------------------------------------
Состояние массива:

	cat /proc/mdstat

Информация о массиве:

    mdadm --detail /dev/md0

Информация об элементе массива:

    mdadm --examine /dev/sda1

Проверка целостности.

    echo check     /sys/block/md1/md/sync_action

Убить массив:
------------------------------------------------------------
    mdadm --stop /dev/md0
    mdadm --zero-superblock /dev/sda..

Other
------------------------------------------------------------
Посмотреть какого устройства нет в массивах

    multipath -ll |grep dm-[0-9][0-9] |awk '{print $2}' |sort |while read mass ;do cat /proc/mdstat |grep ${mass}; echo "-----------" ;done