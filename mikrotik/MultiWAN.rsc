/ip firewall mangle
add action=mark-connection chain=input in-interface=ether1 new-connection-mark=con-WAN1 passthrough=yes
add action=mark-connection chain=input in-interface=ether2 new-connection-mark=con-WAN2 passthrough=yes

add action=mark-routing chain=output connection-mark=con-WAN1 new-routing-mark=WAN1 passthrough=yes
add action=mark-routing chain=output connection-mark=con-WAN2 new-routing-mark=WAN2 passthrough=yes

/ip route
add distance=1 gateway=213.80.152.1 routing-mark=WAN1
add distance=1 gateway=83.167.87.153 routing-mark=WAN2

add distance=1 gateway=213.80.152.1
add distance=2 gateway=83.167.87.153