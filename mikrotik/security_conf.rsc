/interface list
add name=WAN
add name=LAN
/ip neighbor discovery-settings
set discover-interface-list=!WAN
/ip smb
set enabled=no
/ip socks
set enabled=no
/ip traffic-flow
set enabled=no
/ip upnp
set enabled=no
/ip proxy set enabled=no
/snmp
set enabled=no
/ip dns
set servers=1.1.1.1,8.8.8.8
/ip firewall address-list
add address=0.0.0.0/8 list=BOGON
add address=10.0.0.0/8 list=BOGON
add address=100.64.0.0/10 list=BOGON
add address=127.0.0.0/8 list=BOGON
add address=169.254.0.0/16 list=BOGON
add address=172.16.0.0/12 list=BOGON
add address=192.0.0.0/24 list=BOGON
add address=192.0.2.0/24 list=BOGON
add address=192.168.0.0/16 disabled=yes list=BOGON
add address=198.18.0.0/15 list=BOGON
add address=198.51.100.0/24 list=BOGON
add address=203.0.113.0/24 list=BOGON
add address=224.0.0.0/4 list=BOGON
/ip firewall filter
add action=drop chain=input comment=Bogon_Wan_Drop in-interface-list=WAN \
    src-address-list=BOGON
add chain=input comment=Allow_limited_pings in-interface-list=WAN limit=\
    50/5s,2:packet protocol=icmp
add action=drop chain=input comment=Pings_Drop in-interface-list=WAN \
    protocol=icmp
add action=add-src-to-address-list address-list=dns_spoofing \
    address-list-timeout=12h chain=input comment=DNS_Spoofing dst-port=53 \
    in-interface-list=WAN protocol=udp
add action=drop chain=input dst-port=53 protocol=udp src-address-list=\
    dns_spoofing
add action=add-src-to-address-list address-list=22_port_drop \
    address-list-timeout=1w chain=input comment=Trap_22_port dst-port=22 \
    in-interface-list=WAN log-prefix=Attack protocol=tcp
add action=drop chain=input comment=22_port_list_drop in-interface-list=WAN \
    src-address-list=perebor_portov_drop
add action=add-src-to-address-list address-list=3389_port_drop \
    address-list-timeout=1w chain=input comment=Trap_3389_port dst-port=3389 \
    in-interface-list=WAN log-prefix=Attack protocol=tcp
add action=drop chain=input comment=3389_port_list_drop in-interface-list=WAN \
    src-address-list=3389_port_drop
add action=drop chain=input comment=Port_scanner_drop src-address-list=\
    port_scanners
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp psd=21,3s,3,1
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=fin,!syn,!rst,!psh,!ack,!urg
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=fin,syn
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=syn,rst
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=fin,psh,urg,!syn,!rst,!ack
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=fin,syn,rst,psh,ack,urg
add action=add-src-to-address-list address-list=port_scanners \
    address-list-timeout=2w chain=input in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp tcp-flags=!fin,!syn,!rst,!psh,!ack,!urg
add action=drop chain=input comment=Drop_black_list dst-port=5522,8291 \
    in-interface-list=WAN protocol=tcp src-address-list=black_list
add action=add-src-to-address-list address-list=black_list \
    address-list-timeout=1w chain=input comment=add_black_list \
    connection-state=new dst-port=5522,8291 in-interface-list=WAN log=yes \
    log-prefix=Attack protocol=tcp src-address-list=ssh_stage3
add action=add-src-to-address-list address-list=ssh_stage3 \
    address-list-timeout=1m chain=input comment=ssh_stage3 connection-state=\
    new dst-port=5522,8291 in-interface-list=WAN protocol=tcp \
    src-address-list=ssh_stage2
add action=add-src-to-address-list address-list=ssh_stage2 \
    address-list-timeout=1m chain=input comment=ssh_stage2 connection-state=\
    new dst-port=5522,8291 in-interface-list=WAN protocol=tcp \
    src-address-list=ssh_stage1
add action=add-src-to-address-list address-list=ssh_stage1 \
    address-list-timeout=1m chain=input comment=ssh_stage1 connection-state=\
    new dst-port=5522,8291 in-interface-list=WAN protocol=tcp
add chain=input comment=Accept_winbox_ssh dst-port=5522,8291 \
    in-interface-list=WAN protocol=tcp
add action=accept chain=input comment=Established_Wan_Accept \
    connection-state=established disabled=yes
add action=accept chain=input comment=Related_Wan_Accept connection-state=\
    related disabled=yes
add action=drop chain=input comment=Drop_WAN disabled=yes in-interface-list=\
    WAN
/ip firewall service-port
set ftp disabled=yes
set tftp disabled=yes
set irc disabled=yes
set h323 disabled=yes
set sip disabled=yes
set pptp disabled=yes
set udplite disabled=yes
set dccp disabled=yes
set sctp disabled=yes
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set ssh address=192.168.254.0/23 port=5522
set api disabled=yes
set winbox address=192.168.254.0/23
set api-ssl disabled=yes
/system clock
set time-zone-name=Europe/Moscow
/system routerboard settings
set silent-boot=no
/system ntp client
set enabled=no
/tool bandwidth-server
set enabled=no
/tool mac-server
set allowed-interface-list=none
/tool mac-server mac-winbox
set allowed-interface-list=LAN
/tool mac-server ping
set enabled=no
/tool romon
set enabled=no
/tool sms
set receive-enabled=no
