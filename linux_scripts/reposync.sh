#!/bin/bash

LOG=/tmp/reposync.log

date > ${LOG}

for i in rhel-8-for-x86_64-baseos-rpms rhel-8-for-x86_64-appstream-rpms rh-gluster-3-client-for-rhel-8-x86_64-rpms
do
    echo " -=-=-=-=-=-=- ${i} reposync: -=-=-=-=-=-=- " >> ${LOG}
    /usr/bin/reposync -p /data/repo/ --download-metadata --setopt=repo_id.module_hotfixes=1 --repoid=${i} >> ${LOG}
    echo "Return code=$?" >> ${LOG}
    echo " -=-=-=-=-=-=- ${i} end -=-=-=-=-=-=- " >> ${LOG}
done

# Копирование новых пакетов tzdata в спец репозиторий и его обновление
echo -e "\n -=-=-=-=-=-=- Copy tzdata packages -=-=-=-=-=-=- " >> ${LOG}
cp -Rpvn /data/rhel-8-for-x86_64-baseos-rpms.current/Packages/t/tzdata* /data/mia-rhel-8.current/Packages/t/
createrepo --update /data/mia-rhel-8
