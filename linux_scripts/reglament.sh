#!/bin/bash
NORMAL='\033[0m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'

if [ $# -gt 0 ]; then
    echo "использовать $0"
    exit 255
fi

svndir="$(dirname $(readlink -f $0))/../"
echo -en "Performing svn up balancer configs\n"
svn up $svndir || (echo "svn error, exiting" ; exit 1)
read -n 1 -s -p "Press any key to continue"

	
rand=`cat /dev/urandom | tr -dc 'A-Z0-9' | fold -w 10 | head -n 1`
boundary="${rand}.11122333/${HOSTNAME}"
send_mail() {
	subj_1=`echo -en "Регламентные работы "|base64`
	subj_2=`echo -en "на серверах балансировки "|base64`
	subj_3=`echo -en "нагрузки ${ha_balancer}\n"|base64`
	( cat <<EOF;
From: $myemail
To: $myemail
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="${boundary}"
Subject: =?UTF-8?B?${subj_1}?=
 =?UTF-8?B?${subj_2}?=
 =?UTF-8?B?${subj_3}?=

This is a MIME-encapsulated message" 
 
--${boundary}
Content-Type: text/html; charset=UTF-8"
<html>
	<head>
	<title>HTML E-mail</title>
	</head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<body>
		<p style="margin-bottom: 0cm"><font face="Tahoma" color="black" size="2" style="font-size: 11pt">
		<br>
		<b>Прошу согласовать регламентные работы на серверах балансировки нагрузки ${ha_balancer}
		<br><br>
		Цель:</b> ${target}
		<br><br>
		<b>Время проведения работ:</b> <font color="red">${work_time}</font>
		<br><br>
		<b>Состав работ:</b> ${plan}
		<br><br>
		<b>Причина:</b> ${reason}
		<br><br>
		<b>Риски:</b> ${risk}
		<br><br>
		<b>Что затронет:</b> ${affected}
		<br><br>
		${description}
		<br>
		<b>Период неработоспособности:</b> ${downtime}
		<br><br>
		<b>Ответственный:</b> ${responsible_person}
		<br><br>
		<b>В работах задействованы:</b> ${person_helping}
		<br><br>
		<b>Проверка работоспособности систем и сервисов:</b> ${person_checker}
		<br><br>
		<b>Согласовано</b>:
		<br><br>
		</font></p>
	</body>
</html>

--${boundary}--
EOF
	)| sendmail -t 
}

selection()
{
        case "$select" in
                y|Y)
                        echo -en "\nотправляю регламент\n"
			send_mail $haproxyconf $myemail
                ;;
                n|N)
                        echo "Введите все сначала"
                        getparams
                ;;
                *)
                        read -N1 -e -p "Введите y или n: " select
                        selection
                ;;
        esac

}

#haproxyconf=$1

sethaproxyconf()
{
	haproxyconf=${svndir}${1%%\{1,2\}}/haproxy.cfg
}

getdefaultparams()
{	
	echo ""
	PS3="Балансировщик, выберите номер: "
	#default
	recipients=1
        options=("ha-balancer3{1,2}" "ha-balancer6{1,2}" "ha-balancer7{1,2}" "ha-balancer8{1,2}" "ha-balancer9{1,2}" "ha-balancer10{1,2}" "ha-balancer11{1,2}" "ha-balancer12{1,2}" "sputnik-balancer" "ha-balancer-stream")
	select opt in "${options[@]}"
	do
	    case $opt in
		"ha-balancer3{1,2}")
		    ha_balancer=$opt
		    sethaproxyconf $opt
                    break
		    ;;
		"ha-balancer6{1,2}")
		    ha_balancer=$opt
		    sethaproxyconf $opt
                    recipients="6"
		    break
		    ;;
		"ha-balancer7{1,2}")
                    ha_balancer=$opt
		    sethaproxyconf $opt
                    break
		    ;;
		"ha-balancer8{1,2}")
                    ha_balancer=$opt
		    sethaproxyconf $opt
                    break
		    ;;
		"ha-balancer9{1,2}")
                    ha_balancer=$opt
		    sethaproxyconf $opt
		    break
		    ;;
		"ha-balancer10{1,2}")
                    ha_balancer=$opt
		    sethaproxyconf $opt
                    recipients="10"
		    break
		    ;;
		"ha-balancer11{1,2}")
                    ha_balancer=$opt
                    sethaproxyconf $opt
                    recipients="11"
                    break
                    ;;
		"ha-balancer12{1,2}")
                    ha_balancer=$opt
                    sethaproxyconf $opt
                    recipients="12"
                    break
                    ;;
		"sputnik-balancer")
		    ha_balancer=$opt
  		    sethaproxyconf $opt
 		    recipients="sputnik"
		    break
		    ;;	
		"ha-balancer-stream")
		    ha_balancer=$opt
  		    sethaproxyconf $opt
 		    recipients="6"
		    break
			;;
		*) 
		    echo попробуйте еще раз
		    ;;
	    esac
	done
	clear	
        echo "Выбран ${ha_balancer}"
	read -e -p "Цель: "  target
        read -e -p "Причина: " reason
	read -e -p "Время: " -i "Сегодня `date "+%d.%m.%Y"` 00:00" work_time
        PS3="Исполнитель, выберите: "
        options=("Морозов П.С." "Петров А.В." "Савин П.А." "Теплынин М.А." "Евдокимов Н.А." "Волков Д.Б.")
        select opt in "${options[@]}"
        do
            case $opt in
                "Морозов П.С.")
                    person=$opt
		    myemail="p.morozov@rian.ru"
		    break
                    ;;
                "Петров А.В.")
                    person=$opt
		    myemail="av.petrov@rian.ru"
		    break
                    ;;
                "Савин П.А.")
                    person=$opt
		    myemail="p.savin@rian.ru"
		    break
                    ;;
                "Теплынин М.А.")
                    person=$opt
		    myemail="m.teplynin@rian.ru"
		    break
                    ;;
                "Евдокимов Н.А.")
                    person=$opt
                    myemail="n.evdokimov@rian.ru"
                    break
                    ;;
		"Волков Д.Б.")
		    person=$opt
		    myemail="d.volkov@rian.ru"
		    break		
		    ;;
                *) 
                    echo попробуйте еще раз
		    ;;
            esac
        done
	PS3="Ответственный, выберите: "
	options=("Морозов П.С." "Белокуров А.Л." "Музалевский И.С.")
	select opt in "${options[@]}"
        do
            case $opt in
                "Морозов П.С.")
                    responsible_person=$opt
                    break
                    ;;
                "Белокуров А.Л.")
                    responsible_person=$opt
                    break
                    ;;
                "Музалевский И.С.")
                    responsible_person=$opt
                    break
                    ;;
		*)
                    echo попробуйте еще раз
                    ;;
            esac
        done

	#read -e -p "Исполнитель: " person
	if [[ ! -f $haproxyconf ]] ; then
		echo -en "\nexiting, no haproxy config found at: ${haproxyconf}\n"
		exit 1
	fi
	description=`grep --no-group-separator -A1 description $haproxyconf|sed 'N;s/\n/ /'|awk '{$1=$2="";if (! $NF)next;print $0}'|awk -F"# domain:" '{print $2" - "$1"<br>"}'|sort -u`
	
	
}
getparams()
{
	clear
	getdefaultparams
	#read -e -p "Состав: " -i "Изменить конфигурацию haproxy, перезапустить haproxy" plan
	PS3="Выберете состав работ: "
	options=("Изменить конфигурацию haproxy, reload haproxy" "Изменить конфигурацию keepalived, haproxy ; reload keepalived, reload haproxy")
	select opt in "${options[@]}"
	do
	    case $opt in
		"Изменить конфигурацию haproxy, reload haproxy")
		    plan=$opt
		    break
		    ;;
		"Изменить конфигурацию keepalived, haproxy ; reload keepalived, reload haproxy")
		    plan=$opt
		    break
		    ;;
		*)
		    echo попробуйте еще раз
		    ;;
	    esac
	done
	risk="Риск минимальный"
	affected="Кратковременно (не более 1 секунды) может быть нарушена работа перечисленных ниже ресурсов"
	person_helping="$person"
	downtime="0 минут, откат назад в случае сбоя – 2 минуты."
	person_checker="$person, дежурный инженер 6615, основываясь на заявках пользователей."
	recipients_email="ДИТ//Отдел сопровождения информационных систем <DEP_MIA_DIT_OtdSIS@msk.rian>; ДРПО++Руководители направлений <WG_DRPO_Rukovoditeli_napravleniy@msk.rian>; 6615 <6615@rian.ru>; Ганин Сергей Вячеславович <s.ganin@rian.ru>; Белокуров Андрей Леонидович <a.belokurov@rian.ru>; IB_ OTZI <ib_otzi@msk.rian>; Приёмов Дмитрий Александрович <d.priemov@rian.ru>; Мингалев Михаил Евгеньевич <m.mingalev@rian.ru>"
        if [[ $recipients -eq "6" ]]; then
                recipients_email="${recipients_email}; Головин Юрий Александрович <y.golovin@rian.ru>; Грибанов Эльдар Сергеевич <e.gribanov@rian.ru>; Дежурный инженер АСК <control_room@rian.ru>; Леконцев Владислав Юрьевич <v.lekontsev@rian.ru>; Ткачев Геннадий Геннадьевич <g.tkachev@rian.ru>"
        fi
        if [[ $recipients -eq "10" ]]; then
		recipients_email="${recipients_email}; Сафаров Александр Местанович <a.safarov@1prime.ru>; Техническая поддержка <8080@1prime.ru>"
	fi	
        echo -en "\n\n${GREEN}Ваш email: $myemail\nНазвание балансировщков: ${ha_balancer}\nЦель: ${target}\nВремя: ${work_time}\nСостав: ${plan}\nПричина: ${reason}"\
		 "\nРиски: ${risk}\nЧто затронет: ${affected}\nПериод неработоспособности: ${downtime}\nОтветственный: $responsible_person"\
		 "\nВ работах задействованы: ${person_helping}\nПроверка работоспособности систем и сервисов: ${person_checker}\n\n"\
		 "${YELLOW}Список рассылки:\n${recipients_email}${NORMAL}\n\n"

        echo -n "Все верно? (y/n) "
        read -N1 select
        selection
}
getparams


