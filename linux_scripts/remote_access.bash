#!/bin/bash
################################################################################
#                                                                              #
#                 Add/Remove Remote Access on Terminal Server                  #
#                                                                              #
################################################################################

# Constant's ###################################################################

FLAG="${1}"

#-------------------------------------------------------------------------------
function main {
  local iptables="/sbin/iptables"
  local destIP="192.168.0.4"      # Пробрасываем сюда
  local destPort="3389"           # Пробрасываемый порт
  local sourIP="77.77.77.77"     # Удаленный IP
  local sourPort="11111"          # Порт перенаправления
  local interface="eth1"          # Интерфейс, что смотрит в инет

  if [ "${FLAG}" == "add" ]; then
    # Очищаем правила, чтобы не дублировались.
    ${iptables} -t nat -D PREROUTING -i ${interface} -p tcp -m tcp --dport ${sourPort} -j DNAT --to-destination ${destIP}:${destPort}
    ${iptables} -t nat -D PREROUTING -s ${destIP}/32 -d ${sourIP}/32 -p tcp -m multiport --dports 21,22 -j DNAT --to-destination ${sourIP}:21
    ${iptables} -D FORWARD -s ${destIP}/32 -d ${sourIP}/32 -j ACCEPT
    ${iptables} -D FORWARD -s ${destIP}/32 -o ${interface} -j DROP
    # Добавляем правила
    ${iptables} -t nat -A PREROUTING -i ${interface} -p tcp -m tcp --dport ${sourPort} -j DNAT --to-destination ${destIP}:${destPort}
    ${iptables} -t nat -A PREROUTING -s ${destIP}/32 -d ${sourIP}/32 -p tcp -m multiport --dports 21,22 -j DNAT --to-destination ${sourIP}:21
    ${iptables} -A FORWARD -s ${destIP}/32 -d ${sourIP}/32 -j ACCEPT
    ${iptables} -A FORWARD -s ${destIP}/32 -o ${interface} -j DROP
  elif [ "${FLAG}" == "remove" ]; then
    ${iptables} -t nat -D PREROUTING -i ${interface} -p tcp -m tcp --dport ${sourPort} -j DNAT --to-destination ${destIP}:${destPort}
    ${iptables} -D FORWARD -s ${destIP}/32 -d ${sourIP}/32 -j ACCEPT
  else
    echo ""
    echo "remote_access.bash [add]|[remove]"
    echo ""
  fi
}
#-------------------------------------------------------------------------------

# Script's entry point: ########################################################
main "$@"





















sudo /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp -m tcp --dport 16409 -j DNAT --to-destination 192.168.0.4:3389
