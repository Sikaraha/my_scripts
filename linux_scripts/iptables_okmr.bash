#!/bin/bash
################################################################################
#                                                                              #
#                          New IP Tables Rules v 0.9                           #
#                                                                              #
################################################################################

# Constant's ###################################################################
IPTABLES="/sbin/iptables"
EXTIF="eth0"                                # Сетевой интерфейс
IP="192.168.0.1"                            # IP адрес
LOCAL_SUBNET="192.168.55.0/24"              # Подсеть 3DATA
### Implementation #############################################################
function main {
  inputTraff
  outputTraff
}
#-------------------------------------------------------------------------------
function resetRules {
  # Сбрасываем правила и удаляем цепочки
  "${IPTABLES}" -F
  "${IPTABLES}" -t nat -F
  "${IPTABLES}" -t mangle -F
  "${IPTABLES}" -X
  "${IPTABLES}" -t nat -X
  "${IPTABLES}" -t mangle -X
}
#-------------------------------------------------------------------------------
function inputTraff {
  # Запрет на вход для определенных адресов
  #"${IPTABLES}" -A INPUT -s 150.145.60.4/32 -j DROP
  # Разрешение на вход для определенных адресов
  #"${IPTABLES}" -A INPUT -s 150.145.60.4/32 -j ACCEPT
  # Открывает доступ входящему трафику
  "${IPTABLES}" -A INPUT -i eth0 -j ACCEPT
  "${IPTABLES}" -A INPUT -i lo -j ACCEPT
  "${IPTABLES}" -A INPUT -p tcp -m multiport --dports 22,80,443,3306,8080 -j ACCEPT
  #"${IPTABLES}" -A INPUT -p udp -m multiport --dports 4500 -j ACCEPT
  #Дропаем все остальные пакеты
  "${IPTABLES}" -P INPUT DROP
}
#-------------------------------------------------------------------------------
function outputTraff {
  #Правила для исходящего траффика
  #"${IPTABLES}" -A OUTPUT -s "${LOCAL_SUBNET}" -j ACCEPT
  #"${IPTABLES}" -A OUTPUT -p icmp -j icmp-out
  #"${IPTABLES}" -A OUTPUT -o "lo" -j ACCEPT
  #Дропаем все остальные пакеты
  #"${IPTABLES}" -P OUTPUT DROP
}
# Script's entry point: ########################################################
main "$@"