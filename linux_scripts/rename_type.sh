#!/bin/bash

cd /sys/class/scsi_generic

SKIP=':'
[ "$1" == --do ] && SKIP= && shift
GRP="${1^^}"  # '' - all; kc6|kc22|... - only that group

## [cached] _serial ${DEV_sg} SERIAL
# return: exit code 0 == matches
__ser() {
    tapeinfo -f /dev/$1 |awk '$1=="SerialNumber:"{gsub("'\''","",$2);print $2}';
}

_serial() {
    if ! [ "${SER_LAST}" = "$1" ]; then
        SER_LAST="$1"           # Global vars - cache last dev s/n.
        SER_VAL="$( __ser "$1")"
    fi
    [ "${SER_VAL}" = "$2" ]
}

_id_w_inode() {
    stat -c %D-%i-%t-%T "$1" 2>/dev/null || echo "-$1"
}

__do() {
    [ "$SKIP$SKIP2" ] && echo ".skipped.}> $*"
    $SKIP $SKIP2 "$@"
}

is_used() {
    lsof "$@" >&/dev/null
}  # File(s) in use?

## _drv GRoup ${DEV_sg} SERIAL tapeX 'NAme coMMEnt'
_drv(){
    local GG="${1^^}" DEV="$2" SN="$3" TAP="$4" MAP=X SKIP2=
    GRPALL["$GG"]=1
    [ "$GRP" -a "$GRP" != "${GG}" ] && return
    GRPMCH="${GG}"

    if _serial "$DEV" "${SN}"; then
        MAP="$(sg_map 2>&1 |awk -v D="/dev/$DEV" '$1==D{m=$2; exit}END{print m?m:D}')"

        local id1="$(_id_w_inode "${MAP}")" id2="$(_id_w_inode "/dev/${TAP}")"
        
        if [ "$id1" = "$id2" ]; then
            echo "#  SKIP: ${MAP} /dev/${TAP} are matching  as  ${id1}."
            return
        else
            echo "# Relink: /dev/${TAP} [$id2]  to  ${MAP} [$id1]."
        fi

        [ -e "/dev/$TAP" ] && is_used "/dev/$TAP" && SKIP2=':'
        echo "$* -- /dev/$TAP  is $($SKIP2 echo 'not ')in use."

        [ -e "/dev/${TAP}" ] &&  __do unlink "/dev/${TAP}"
        __do ln "${MAP}" "/dev/${TAP}"
    fi
}

GRPMCH=
declare -A GRPALL

for DEV in sg*
do
    ######DRIVES#######
    _drv 'kc6'  "${DEV}"  'HUL826AK1H'  'tape0'  'DRIVE 1 KC6'
    _drv 'kc6'  "${DEV}"  'HUJ5384LU1'  'tape1'  'DRIVE 2 KC6'
    _drv 'kc6'  "${DEV}"  'HUJ4432356'  'tape2'  'DRIVE 3 KC6'
    _drv 'kc22' "${DEV}"  'HUJ549591C'  'tape3'  'DRIVE 1 KC22'
    _drv 'kc22' "${DEV}"  'HUJ548573H'  'tape4'  'DRIVE 2 KC22'
    _drv 'kc22' "${DEV}"  'HUJ5374KTR'  'tape5'  'DRIVE 3 KC22'
    _drv 'ibm'  "${DEV}"  '1310027991'  'tape6'  'DRIVE LTO4 IBM_TS3310 KC22'
    _drv 'ibm'  "${DEV}"  '1310027981'  'tape7'  'DRIVE 1 IBM_TS3310 KC22'
    # _drv "${DEV}"  '?'        'tape8'  'DRIVE 2 IBM_TS3310 KC22'
    _drv 'ibm'  "${DEV}"  '1068076870'  'tape9'  'DRIVE LTO5 IBM_TS3310 KC22'
    _drv 'ibm'  "${DEV}"  '1068076682'  'tape10' 'DRIVE LTO5 IBM_TS3310 KC22'

##echo "//$DEV  ..$SER_LAST  ..$SER_VAL.."

    #######CHANGERS######
    _drv 'kc6'  "${DEV}"  '464970G+1553SY5037'  'changer0'  'CHANGER ORACLE KC6'
    _drv 'kc22' "${DEV}"  '464970G+1553SY5036'  'changer1'  'CHANGER ORACLE KC22'
    _drv 'ibm'  "${DEV}"  '000001311063_LLA'    'changer2'  'CHANGER IBM_TS3310 KC6'

    [ "$GRP" ] && [ -z "$GRPMCH" ] \
        && echo "[EE] No group (${!GRPALL[*]}) match for ${GRP}." >&2 \
        && break
done
