#!/bin/bash
################################################################################
# Бэкап, перемещение и восстановление 
################################################################################

# Откуда
OUT=""
# Куда
IN=""
# Сегодня
TODAY="$(date +%Y%m%d)"


function main {
while (1>0); do
  echo Упаковать сайт - 1
  echo Снять дамп с DB - 2
  echo Копировать дамп на удаленный ПК - 3
  echo Копировать сайт на удаленный ПК - 4
  echo Распаковать сайт на удаленной систему в папку - 5
  echo Восстановить дамп DB на удаленной системе в базу - 6
  echo Выйти... - 0
  echo "введи пункт меню: "
  read x
  case $x in
       1)
            echo "Пакую сайт в" okmr_"${TODAY}".tar.gz
            #compressSite()
            ;;
       2)
            echo "Снимаю дамп с DB"
            #dumpDB()
            ;;
       3)
            echo "Копирую дамп на удаленный ПК"
            #copyDb()
            ;;
       4)
            echo "Копирую сайт на удаленный ПК"
            #copySite()
            ;;
       5)
            echo "Распаковать сайт на удаленной систему в папку"
            #extractSite()
            ;;
       6)
            echo "Восстановить дамп DB на удаленной системе в базу"
            #restoreDB()
            ;;
       0)
            echo "Выход..."
            break
            ;;
       *)
            echo "Не туда жмешь"
            ;;
  esac
done
}

#function compressSite {
#  dirBackup="~/backup/"
#  mkdir "${dirBackup}"
#  ex1="/var/www/bitrix/okmr/bitrix/backup/*"
#  ex2="/var/www/bitrix/okmr/.git/*"
#  tar -zcvf "${dirBackup}"okmr_"${TODAY}".tar.gz "/var/www/" \
#                              --exclude="${ex1}" \
#                              --exclude="${ex2}"
#}
#
#function dumpDB {
#  dirBackup="~/backup/"
#  dbUser="USER"
#  dbPass="PASSWORD"
#  dbName="DB_NAME"
#  mysqldump -u"${dbUser}" -p"${dbPass}" "${dbName}" > "${dirBackup}"db_"${TODAY}"
#}
#
#function copyDb {
#  #copyTo(okmr_${TODAY}.tar.gz)
#}
#
#function copySite {
#  #copyTo()
#}
#
#function extract {
#  tar -xvf "okmr_${TODAY}.tar.gz" -C /  
#}
#
#function restoreDB {
#  dirBackup="~/backup/"
#  dbUser="USER"
#  dbPass="PASSWORD"
#  dbName="DB_NAME"
#  mysql -u"${dbUser}" -p"${dbPass}" -f "${dbName}" < mydb_dump.sql
#}
#
#function copyTo {
#  fName="${1}"
#  userName="${2}"
#  hostName="${3}"
#  remoteDir="${4}"
#  scp "{fName}" "${userName}"@"${hostName}":"${remoteDir}"
#}

# Script's entry point:
################################################################################
main "$@"