#!/bin/bash
################################################################################
#                                                                              #
################################################################################

# Constant's ###################################################################
THIS_SCRIPT_NAME=$(basename $0)


### Implementation #############################################################
function main {
  log_msg "#####################################################################"
  log_msg "#              Установка и настройка Exim + Dovecot                 #"
  log_msg "#####################################################################"
  log_msg ""

  #installSoft
  #createMailUser
  #createDBMail
  #installPostfixadmin
  #addVirtualHost
  configPostfixAdmin
}

#-------------------------------------------------------------------------------
function installSoft {
  log_msg "Установка ПО"     #1>/dev/null
  sudo apt-get install -y apache2 1>/dev/null 2>&1 && \
      log_msg "  Apache2 - успешно"
  sudo apt-get install -y mysql-server && \
      log_msg "  MySQL - успешно"
  sudo apt-get install -y exim4 exim4-base exim4-config exim4-daemon-heavy 1>/dev/null 2>&1 && \
      log_msg "  exim4 - успешно"
  sudo apt-get install -y dovecot-common dovecot-imapd dovecot-pop3d 1>/dev/null 2>&1 && \
      log_msg "  dovecot - успешно"
  sudo apt-get install -y php5-imap 1>/dev/null 2>&1 && \
      log_msg "  php5-imap - успешно"
  log_msg ""
}
#-------------------------------------------------------------------------------
function createMailUser {
  local pathVmail="/var/vmail"

  log_msg "Создаем пользователя mail"
  sudo useradd -r -u 1150 -g mail -d "${pathVmail}" -s /sbin/nologin -c 'Virtual Mailbox' vmail
  if [ ! -d "${pathVmail}" ]; then
    sudo mkdir -p "${pathVmail}"
  fi
  sudo chown vmail:mail "${pathVmail}"
  sudo chmod 770 "${pathVmail}"
  log_msg ""
}
#-------------------------------------------------------------------------------
function createDBMail {
  local dbPass="R$e3w2q1"

  log_msg "Создаем базу данных MySQL"
   echo "Введите пароль root для базы данных"
  mysql -u root -p
  #Enter password:
  #mysql>CREATE DATABASE mail;
  #mysql>GRANT ALL PRIVILEGES ON mail.* TO postmaster@localhost IDENTIFIED BY '${dbPass}';
  log_msg ""
}
#-------------------------------------------------------------------------------
function installPostfixadmin {
  #http://downloads.sourceforge.net/project/postfixadmin/postfixadmin/postfixadmin-2.92/postfixadmin-2.92.tar.gz
  local pstfxUrl="http://downloads.sourceforge.net/project/"
  local pstfxVer="2.92"
  local pstfx="postfixadmin"
  local pathWww="/var/www/"
  local pathHome="/home/user/"

  log_msg "Устанавливаем PostfixAdmin"
  wget "${pstfxUrl}${pstfx}/${pstfx}/${pstfx}-${pstfxVer}/${pstfx}-${pstfxVer}.tar.gz"
  tar -xzvf "${pathHome}${pstfx}-${pstfxVer}.tar.gz" 1>/dev/null 2>&1
  sudo mv "${pathHome}${pstfx}-${pstfxVer}" "${pathWww}${pstfx}"
  sudo chown -R www-data:www-data "${pathWww}${pstfx}"
  sudo chmod -R 700 "${pathWww}${pstfx}"
  log_msg ""
}
#-------------------------------------------------------------------------------
function addVirtualHost {
  log_msg "Настройка виртуального хоста"
  #Test conf ----------------------------------------------------------------!!!
  touch "/etc/apache2/apache2.conf.tst"
  cat >> "/etc/apache2/apache2.conf.tst" <<EOFaddVirtualHost
<VirtualHost *:81>
  ServerName postfixadmin
  DocumentRoot /var/www/postfixadmin
  <Directory /var/www/postfixadmin>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    allow from all
  </Directory>
  ErrorLog /var/log/apache2/error_postfixadmin.log
  LogLevel warn
  CustomLog /var/log/apache2/access_postfixadmin.log combined
</VirtualHost>
EOFaddVirtualHost
  log_msg "  Успешно"
  log_msg ""
}
#-------------------------------------------------------------------------------
function configPostfixAdmin {
  local path="/var/www/postfixadmin/"
  local confName="config.inc.php"
  local file="${path}${confName}"
  local addressBase=""
  local portBase=""
  local loginBase=""
  local passBase=""

  log_msg "Настройка конфигурации PostfixAdmin"
  #Test conf ----------------------------------------------------------------!!!
  cp "${file}" "/home/user/${confName}.tst"
  file="/home/user/${confName}.tst"

  #cat "${file}"|sed 's/^$CONF['$CONF['database_type'] = 'mysqli'   ;/$CONF['$CONF['database_type'] = 'mysqli';/' 2>&1
  #cat "${file}"|sed 's/^$CONF['$CONF['database_host'] = 'localhost';/$CONF['$CONF['database_host'] = 'localhost'; 2>&1
  #cat "${file}"|sed 's/^$CONF['$CONF['database_user'] = 'postfix'  ;/
  #cat "${file}"|sed 's/^$CONF['$CONF['database_password'] = 'postfixadmin';/
  #cat "${file}"|sed 's/^$CONF['$CONF['database_name'] = 'postfix';/
  cat "${file}"|sed 's/^$CONF['configured'] = false;/$CONF['configured'] = true;/g' > "${file}.bak" 2>&1
}
#-------------------------------------------------------------------------------
function dovecotConf {
  #Test conf ----------------------------------------------------------------!!!
  cat > "/etc/dovecot/dovecot.conf.tst" <<EOFdovecotConf
# Директория для временных файлов
base_dir = /var/run/dovecot/

# Протоколы, по которым обслуживаем пользователей. Доступны еще imaps и pop3s (SSL).
protocols = imap pop3

# IMAP слушаем на 143-м порту
protocol imap {
    listen = 127.0.0.1:143
    # Если надо открыть доступ снаружи, то надо указать *:143
}
# POP3 слушаем на 110-м порту
protocol pop3 {
    listen = 127.0.0.1:110
    # Если надо открыть доступ снаружи, то надо указать *:110
}

# Здесь можем отключить аутентификаию открытым текстом, если не включён SSL/TLS
# С локалхоста, впрочем, он всё равно будет разрешать такую аутентификацию
disable_plaintext_auth = no

# Убивать все процессы IMAP и POP3 при закрытии родительского процесса dovecot
shutdown_clients = yes

# Логфайл вместо syslog()
log_path = /var/log/dovecot.log

# Сюда пишутся информационные и дебаг-сообщения
info_log_path = /var/log/dovecot.log

# Формат таймстэмпа (strftime()) для записи в лог
log_timestamp = "%Y-%m-%d %H:%M:%S "

# Чем будем логиниться в syslog()
syslog_facility = mail

# Отключаем SSL/TLS. Все равно оно нужно только если используем pop3s и imaps
ssl_disable = yes

# Директория где аутентификационный процесс размещает UNIX сокеты
# которые требуются для процесса логина. Сокеты создаются от суперпользователя,
# поэтому можно не беспокоится насчёт прав. При старте dovecot всё
# содержимое этой директории удаляется.
login_dir = /var/run/dovecot/login

# Делаем chroot для процесса аутентификации dovecot
login_chroot = yes

# Юзер, использующийся в процессе логина
login_user = dovecot

# Каждый логин должен быть обработан своим собственным процессом ('yes'),
# или один процесс может обрабатывать несколько соединений ('no'). 'yes' более
# секьюрно, особенно если включено SSL/TLS. 'no' быстрее работает, ибо нет
# необходимости создавать процесс на каждое соединение
login_process_per_connection = yes

# Число запускаемых процессов логина. Если 'login_process_per_connection'
# равно 'yes', то это число свободных процессов ожидающих подключения
# пользователей.
login_processes_count = 3

# Максимальное число соединений разрешённых в сосоянии 'логина'. Когда
# достигается указанный тут лимит, самые старые связи разрываются
login_max_processes_count = 128

# Приветственное сообщение для клиентов.
login_greeting = Dovecot ready on mydomain.ru.

# Разделённый пробелами лист элементов, которые будут записаны в лог. Непустые
# элементы будут объединены через запятую.
login_log_format_elements = user=<%u> method=%m rip=%r lip=%l %c

# Расположение пользовательских ящиков
mail_location = maildir:/var/vmail/%d/%u

# Группа, которой разрешены некоторые привилегированные операции
mail_privileged_group = mail

# Включаем режим дебага, на первых порах очень полезно
mail_debug = yes

#Разрешённый диапазон UIDов для пользователей.
#Так мы защитимся от подключения демонов или системных пользователей.
#Как root подключиться не получится в любом случае
first_valid_uid = 1150
last_valid_uid = 1150

protocol imap {
    # Месторасположение исполняемого файла авторизации
    login_executable = /usr/lib/dovecot/imap-login
    # Исполняемый файл IMAP
    mail_executable = /usr/lib/dovecot/imap
    imap_max_line_length = 65536
}
protocol pop3 {
    login_executable = /usr/lib/dovecot/pop3-login
    mail_executable = /usr/lib/dovecot/pop3
    pop3_uidl_format = %08Xu%08Xv
}
protocol lda {
    # Куда слать письмо об отказах и превышении квот
    postmaster_address = webmaster@mydomain.ru
    hostname = mydomain.ru
    sendmail_path = /usr/lib/sendmail
    # Путь к сокету для поиска пользователей
    auth_socket_path = /var/run/dovecot/auth-master
}

# Парметры для дебага
auth_verbose = yes
auth_debug = yes
auth_debug_passwords = yes

auth default {
    # Список требуемых механизмов аутентификации, разделённый пробелами
    #   plain login digest-md5 cram-md5 ntlm rpa apop anonymous gssapi
    mechanisms = plain login digest-md5 cram-md5

    passdb sql {
        # Путь к файлу, где у нас прописаны все параметры базы данных
        args = /etc/dovecot/dovecot-sql.conf
    }
    userdb sql {
        # Path for SQL configuration file
        args = /etc/dovecot/dovecot-sql.conf
    }

    # Юзер, от которого будет работать процесс аутентификации
    user = nobody

    socket listen {
        master {
        path = /var/run/dovecot/auth-master
        mode = 0600
        user = vmail
        group = mail
        }
        client {
            # Этот сокет мы будем использовать для аутентификации Exim
            path = /var/run/dovecot/auth-client
            mode = 0660
        }
    }
}
dict {
}
plugin {
}
EOFdovecotConf
}
#-------------------------------------------------------------------------------
function dovecotSQLConf {
  #Test conf ----------------------------------------------------------------!!!
  cat > "/etc/dovecot/dovecot-sql.conf.tst" <<EOFdovecotSQLConf
driver = mysql
connect = host=localhost dbname=mail user=postmaster password=mypassword
# Об этом выше уже говорилось. Здесь указываем механизм шифрования паролей
default_pass_scheme = MD5-CRYPT
password_query = SELECT username as user, password, '/var/vmail/%d/%n' as userdb_home, 'maildir:/var/vmail/%d/%n' as userdb_mail, 1150 as userdb_uid, 8 as userdb_gid FROM mailbox WHERE username = '%u' AND active = '1'
user_query = SELECT '/var/vmail/%d/%n' as home, 'maildir:/var/vmail/%d/%n' as mail, 1150 AS uid, 8 AS gid, concat('dirsize:storage=', quota) AS quota FROM mailbox WHERE username = '%u' AND active = '1'
# Вышеуказанные команды представлены для случая, если в поле username указаны целиком почтовые ящики в виде username@domain. При этом можно устраивать несколько почтовых доменов на одном сервере, но и для аутентификации придётся предоставлять этот username целиком. Если в поле username в базе содержатся только логины, то команды можно переделать так:
# password_query = SELECT username as user, password, concat('/var/vmail/',domain,'/%n') as userdb_home, concat('maildir:/var/vmail/',domain,'/%n') as userdb_mail, 1150 as userdb_uid, 8 as userdb_gid FROM mailbox WHERE username = '%n' AND active = '1'
# user_query = SELECT concat('/var/vmail/',domain,'/%n') as home, concat('maildir:/var/vmail/',domain,'/%n') as mail, 1150 AS uid, 8 AS gid, concat('dirsize:storage=', quota) AS quota FROM mailbox WHERE username = '%n' AND active = '1'
EOFdovecotSQLConf
}
#-------------------------------------------------------------------------------
function configurateExim {
  sudo dpkg-reconfigure exim4-config
  #Test conf ----------------------------------------------------------------!!!
  cat > "/etc/exim4/exim4.conf.tst" <<EOFconfigurateExim
# Имя хоста. Используется в EHLO.
# Фигурирует в других пунктах, если они не заданы
# По умолчанию используется то, что вернёт функция uname()
primary_hostname = mydomain.ru

# Данные для подключения к базе данных
# hide в начале означает, то нерутовые пользователи командой exim -bV не увидят
# этих значений
hide mysql_servers = localhost/mail/postmaster/password

# Задаём список локальных доменов. В данном случае спрашиваем у MySQL

DOMAIN_QUERY    = SELECT domain FROM domain WHERE \
domain='${domain}' AND active='1'
domainlist local_domains = ${lookup mysql{DOMAIN_QUERY}}

# Таким же образом задаём список доменов, с которых разрешён релей.
domainlist relay_to_domains = ${lookup mysql{DOMAIN_QUERY}}

# Список хостов, с которых разрешён релей без авторизации.
hostlist   relay_from_hosts = localhost:127.0.0.1/8:192.168.0.0/24

# Списки ACL для проверки почты
acl_smtp_rcpt = acl_check_rcpt
acl_smtp_data = acl_check_data

# Здесь указываем сокет внешнего антивируса ClamAV. Пока что оставим закомменченным
# Включим его позже
# av_scanner = clamd:/var/run/clamav/clamd.ctl
# Здесь укажем TCP/IP сокет для SpamAssassin
# spamd_address = 127.0.0.1 783

# Порт, на котором SMTP демон будет слушать входящие подключения
daemon_smtp_ports = 25 : 465

# Имя домена добавляемое для локальных отправителей (реальных
# юзеров системы) т.е. почта отправляемая от root, будет от
# root@домен_указанный_здесь. Если пункт незадан, то используется
# имя хоста из primary_hostname.
qualify_domain = mydomain.ru

# Имя домена добавляемое для локальных получателей
qualify_recipient = mydomain.ru

# запрещаем работу доставки под юзером root - в целях безопасности
never_users = root

# Проверяем соответствие прямой и обратной зон для всех хостов.
# При необходимости лучше раскомментировать это позже
#host_lookup = *

# Здесь можно включить запросы ident на входящие SMTP запросы.
# Вещь ненужная и неактуальная. Отключаем
#rfc1413_hosts = *
rfc1413_query_timeout = 0s

# Период повторных попыток доставки сообщений об ошибке
ignore_bounce_errors_after = 1d

# Через пару недель удалим то, что так и не смогли доставить
timeout_frozen_after = 14d

# Выбираем, что мы будем логировать
# + - писать в логи,
# - - Не писать в логи.
# +all_parents - все входящие?
# +connection_reject - разорваные соединения
# +incoming_interface - интерфейс (реально - IP)
# +lost_incoming_connections - потеряные входящие
# соединения
# +received_sender - отправитель
# +received_recipients - получатель
# +smtp_confirmation - подтверждения SMTP?
# +smtp_syntax_error - ошибки синтаксиса SMTP
# +smtp_protocol_error - ошибки протокола SMTP
# -queue_run - работа очереди (замороженные мессаги)
log_selector = \
+all_parents \
+connection_reject \
+incoming_interface \
+lost_incoming_connection \
+received_sender \
+received_recipients \
+smtp_confirmation \
+smtp_syntax_error \
+smtp_protocol_error \
-queue_run

begin acl

# Правила для всех получателей. Выше мы включили этот ACL

acl_check_rcpt:

# Сразу принять то, что пришло с локалхоста не по TCP/IP
accept  hosts = :

# Запрещаем письма для локальных доменов, содержащие в локальной части
# символы @; %; !; /; |.
deny    message       = Restricted characters in address
domains       = +local_domains
local_parts   = ^[.] : ^.*[@%!/|]

# Проверяем недопустимые символы для
# нелокальных получателей:
deny    message       = Restricted characters in address
domains       = !+local_domains
local_parts   = ^[./|] : ^.*[@%!] : ^.*/\\.\\./

# Принимать почту на постмастера, не проверяя отправителя.
# Может использоваться для спама
accept  local_parts   = postmaster
domains       = +local_domains

# Здесь можно запретить отправку от непроверенных пользователей
# Если нужно отправлять почту от logwatch etc., то лучше убрать

require verify        = sender

accept  hosts         = +relay_from_hosts
control       = submission
require message = relay not permitted
domains = +local_domains : +relay_to_domains
require verify = recipient

# Все, что сюда дошло, пропускаем
accept

# Здесь мы проверяем тело сообщения

acl_check_data:
# Здесь проверка на вирусы
warn    malware    = *
     message    = This message contains a virus ($malware_name).

# А здесь - проверка на спам
warn    spam       = nobody
    add_header = X-Spam-Flag: YES\n\
        X-Spam_score: $spam_score\n\
        X-Spam_score_int: $spam_score_int\n\
        X-Spam_bar: $spam_bar\n\
        X-Spam_report: $spam_report

# Остальное пропускаем
accept

begin routers

# Поиск маршрута к хосту в DNS. Если маршрут не найден в DNS -
# то это `унроутабле аддресс`. Не проверяются локальные
# домены, 0.0.0.0 и 127.0.0.0/8
dnslookup:
driver = dnslookup
domains = ! +local_domains
transport = remote_smtp
ignore_target_hosts = 0.0.0.0 : 127.0.0.0/8
no_more

# смотрим альясы

system_aliases:
driver = redirect
allow_fail
allow_defer
data = ${lookup mysql{SELECT goto FROM alias WHERE \
address='${quote_mysql:$local_part@$domain}' OR \
address='${quote_mysql:@$domain}'}}

# Всё что осталось - это локальные адресаты.
# Доставляем почту в dovecot
dovecot_user:
driver = accept
condition = ${lookup mysql{SELECT goto FROM \
alias WHERE \
address='${quote_mysql:$local_part@$domain}' OR \
address='${quote_mysql:@$domain}'}{yes}{no}}
transport = dovecot_delivery

begin transports

# На удалённые хосты доставляем по SMTP

remote_smtp:
driver = smtp

# Доставка локальным адресатам - в dovecot
# Надо заметить что тут использовалась ранее прямая доставка
# в директорию, но щас с портами стал-таки устанавливаться deliver
# программа dovecot занимающаяся доставкой мессаг.
# Соответственно юзаем его.
dovecot_delivery:
driver = pipe
command = /usr/lib/dovecot/deliver -d $local_part@$domain
message_prefix =
message_suffix =
delivery_date_add
envelope_to_add
return_path_add
log_output
user = vmail

# Доставка через пайп

address_pipe:
driver = pipe
return_output

# Транспорт для автоответов

address_reply:
driver = autoreply

begin retry

# Правила для повторных попыток доставки
# Сначала попытки раз 15 мин в течение 2 часов, потом, начиная с
# интервала в 1 час, увеличивая его в 1.5 раза, пытаемся доставить 16 часов.
# Потом раз в 6 часов, до истечения 4 суток

# Address or Domain    Error       Retries
# -----------------    -----       -------

*                      *           F,2h,15m; G,16h,1h,1.5; F,4d,6h

# Преобразование адресов нам не нужно

begin rewrite

begin authenticators

# Здесь разные механизмы авторизации для разных клиентов
auth_plain:
driver = plaintext
public_name = PLAIN
server_prompts = Username:: : Password::
server_condition = ${if crypteq{$auth3}{${lookup mysql{SELECT password FROM \
mailbox WHERE username = '${quote_mysql:$auth2}'}}}{yes}{no}}
server_set_id = $auth2

auth_login:
driver = plaintext
public_name = LOGIN
server_condition = ${if crypteq{$auth2}{${lookup mysql{SELECT password FROM \
mailbox WHERE username = '${quote_mysql:$auth1}'}}}{yes}{no}}
server_prompts = Username:: : Password::
server_set_id = $auth1

# А вот так мы можем передать аутентификацию на Dovecot SASL.
# Впрочем, CRAM-MD5 все равно не прокатит
auth_cram_md5:
driver = dovecot
public_name = CRAM-MD5
server_socket = /var/run/dovecot/auth-client
server_set_id = $auth2
EOFconfigurateExim
}





#-------------------------------------------------------------------------------
function log_msg() {
  local marker="$2"
  local text="$1"
  local mdate="$(date +%d-%m-%Y\ %H:%M:%S) "
#  echo "----------------|${marker}|-----------------"
#  if [ ! -d "${DIR_BUILD}" ]; then
#    mkdir -p "${DIR_BUILD}"
#  fi
  if [[ "${marker}"="$%" ]]; then
    echo "${text}"
    echo "${text}" >> "/home/user/${THIS_SCRIPT_NAME}.log"
  elif [[ "${marker}"!="$%" ]]; then
    echo "${mdate}${text}"
    echo "${mdate}${text}" >> "/home/user/${THIS_SCRIPT_NAME}.log"
  fi
}

# Script's entry point: #######################################################
main "$@"