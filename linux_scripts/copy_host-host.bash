#!/bin/bash
################################################################################
# 1. Копирование файла с удаленного сервера на локальный компьютер.
# 2. Копируем файл "file.txt" с локального компьютера на удаленный сервер.
# 3. Копируем папку "dir1" с локального хоста в директорию "dir2" на удаленном хосте.
# 4. Копируем файл "file.txt" с одного удаленного сервера "remote.host1" на другой удаленный сервер "remote.host2".
# 5. Копируем файлы "file1.txt" и "file2.txt" с локального компьютера в Ваш домашний каталог на удаленном сервере.
# 6. Копируем файл "file.txt" с локального хоста на удаленный хост, используя порт 2222.
# 7. Копируем файл "file.txt" с локального компьютера в Ваш домашний каталог на удаленном сервере. Сохраняем время изменения и время доступа и права копируемого фала.
# 8. Копируем файл "file.txt" с локального компьютера в Ваш домашний каталог на удаленном сервере. Увеличиваем скорость работы SCP изменяя алгоритм шифрования с AES-128 (по умолчанию) на Blowfish.
# 9. Копируем файл "file.txt" с локального компьютера в Ваш домашний каталог на удаленном сервере. Ограничиваем ширину канала используемого командой SCP до 100 Kbit/s.
# 10. Копируем несколько файлов с удаленного хост в текущую директорию на Вашем локальном хосте.
################################################################################

### Constants #################################################################

#USERNAME="$1" root
#HOSTNAME="$2" 62.176.6.60
#FULLPATH="$3" /home/firewall.sh
#TARGETPATH="$4" c:\
USERNAME="root"
HOSTNAME="62.176.6.60"
FULLPATH="/home/firewall.sh"
TARGETPATH="c:\ "
###############################################################################

function main {
  copyHostToLocal
}


function copyHostToLocal {
  local userName="$1"
  local hostName="$2"
  local fullPath="$3"
  local targetPath="$4"

  #1.
  scp "$userName"@"$hostName":"$fullPath" "$targetPath"
}

case $1  in

    backup)
        log_msg "System Backup Mode Selected"
        ;;

    clean)
        echo "Removing the build directory now..."
        rm -rf $WORKDIR
        echo "Done...Exiting"
        exit 0
        ;;

    dist)
        log_msg "Distribution Mode Selected"
        ;;


    *)
        echo "Usage of remastersys $REMASTERSYSVERSION is as follows:"


##2.
#scp file.txt user@remote.host:/some/remote/directory
##3.
#scp -r dir1 user@remote.host:/some/remote/directory/dir2
##4.
#scp user@remote.host1:/directory/file.txt user@remote.host2:/some/directory/
##5.
#scp file1.txt file2.txt user@remote.host:~
##6.
#scp -P 2222 file.txt user@remote.host:/some/remote/directory
##7.
#scp -p file.txt user@remote.host:~
##8.
#scp -c blowfish file.txt user@remote.host:~
##9.
#scp -l 100 file.txt user@remote.host:~
##10.
#scp user@remote.host:~/\{file1,file2,file3\} .



# Script's entry point: #######################################################
main "$@"