#!/bin/bash

#VARS
###########################################
MOUNTS=$(awk '!/pipefs|\/boot|\ \/\ |\/proc|\/pts|\/shm|\/sys|\/data|devtmp/ {print $2}' /proc/mounts)
PGDATA="/data/pgsql/10"

if [ -z ${1} ];then
	echo "Select run mode or run script with parameters: ${0##*/} {net|exec}"
	read -p '<net|exec> : ' OPT
else
	OPT=${1}
fi


#FUNC
###########################################
check_ok() {
	if [ "$?" = "0" ];then
		printf "%5s\e[1;32m[OK]\e[0m\n"
	else
		printf "%5s\e[1;31m[FAIL]\e[0m\n"
	fi
}

# Check_master
MASTER=$(cat /etc/bacula/scripts/switch/files/whois_master)
if [ "$MASTER" != $HOSTNAME ];then
	echo "Master server is $MASTER, aborting."
	kill -9 $$
fi

# Unmount ALL storages
storumnt() {
	echo "Unmounting storages"
	for MPOINT in ${MOUNTS}
	do
		echo -ne "Trying ${MPOINT}..."
		umount ${MPOINT} 2> /dev/null
		lv=$?
		COUNTER=$[COUNTER+$lv]
		TOTCOUNTER=$[TOTCOUNTER+$lv];
		
		if [ "$COUNTER" = "0" ];then
			printf "%5s\e[1;32m[OK]\e[0m\n"
		else
			printf "%5s\e[1;31m[FAIL]\e[0m\n"
			COUNTER=0
		fi
	done

	if [ "$TOTCOUNTER" = "0" ];then
		printf "%5s\e[1;32m[Umount SUCESSFUL]\e[0m\n"
	else
		printf "%5s\e[1;31m[Umount FAILED]\e[0m\n"
	fi
}

# Switch fstab
mvfstab() {
	echo -ne "Switching fstab..."
	cat /etc/bacula/scripts/switch/files/fstab.backup1 >> /etc/fstab
	check_ok
}

# Configure_cron
docron() {
	echo -ne "Setting up cron..."
	/etc/bacula/scripts/cronfiles/cron.sh > /dev/null
	check_ok
	/etc/init.d/crond restart
}

# Reassemble RAID's and set to rw
mdmnt() {
	echo "Preapraing RAID's..."
	echo -ne "Config file..."
	cp /etc/bacula/scripts/switch/files/mdadm.conf /etc/mdadm.conf
	check_ok
	echo "Reassembling..."
	mdadm --assemble --scan; wait
}

# Mount storages
mfst() {
	echo -ne "Mounting fstab..."
	mount -a
	check_ok
}

# Switch postgres ot standalone
pswitch() {
	echo "Postgres to standalone... "
	TFILE=$(awk -v q="'" '/trigger_file/{gsub( "^'\''|"q"$", "", $3); print $3}' ${PGDATA}/recovery.conf)
	touch ${PGDATA}/${TFILE}
	check_ok
}

# Set Virtual IP
setip() {
	middleHost=${MASTER}
	middleIP16="10.24.2.76/16"
	middleIP24="10.1.1.158/24"

	echo -e "\nNetwork interfaces..."

	###Check that middle addresses isn't already set.
	#chk=$(ip a | grep -A5 'bond[0-9]\:' | grep 'inet\ ' | awk '#'$a'#{a=1}#'$b'#{b=1}END{exit!(a&&b)}'; echo $?)
	#if [ $chk = "0" ];
	#then
	#	echo "Addresses already in use, exiting."
	#	exit 1
	#fi

	ping ${middleHost} -c 10 -q -w 30 &>/dev/null
	pingHost=$?
	ping ${middleIP16} -c 10 -q -w 30 &>/dev/null
	pingIP16=$?
	ping ${middleIP24} -c 10 -q -w 30 &>/dev/null
	pingIP24=$?
	pingTot=$((pingHost+pingIP16+pingIP24))
	if [ ${pingTot} -ne 3 ];then
		if [ ${pingHost} -ne 1 ];then
			echo "Ping backup1.rian.off returned exit status ${pingHost}."
		elif [ ${middleIP16} -ne 1 ];then
			echo "Ping 10.24.2.76 (Middle bond0.100) returned exit status ${pingIP16}."
		elif [ ${middleIP1024} -ne 1 ];then
			echo "Ping 10.1.1.158 (Middle bond0.101) returned exit status ${pingIP24}."
		fi
		printf "%5s\e[1;31m[Setting IP Addresses FAILED]\e[0m\n"
		exit 1
	else
		echo "ifconfig bond0.100:1 $middleIP16 up"
		ifconfig bond0.100:1 ${middleIP16} up
		echo "arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255" 
		arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255
		echo "ifconfig bond0.101:1 $middleIP24 up"
		ifconfig bond0.101:1 ${middleIP24} up
		echo "arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255"
		arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255
		printf "%5s\e[1;32m[IP set SUCESSFULLY]\e[0m\n"
	fi
}

#BEGIN
###########################################
case $OPT in
	net) echo -n "Setting IP"
		setip;;
	exec) echo "Starting full procedure."
		storumnt
		mvfstab
		docron
		#mdmnt
		mfst
		pswitch		# uncommented by apetrov 2018-10-03
		setip;;
	*) echo "Usage: ${0##*/} {net|exec}"
		exit 1;;
esac

# Up bacula
#/etc/init.d/bacula-sd start 
#/etc/init.d/bacula-dir start
#/etc/init.d/bacula-fd start

###-- пересмотреть переключение db
###-- имя мастера берется из /etc/bacula/scripts/switch/files/whois_master почему?
