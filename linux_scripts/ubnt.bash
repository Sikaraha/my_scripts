#! /bin/bash
########################################################
function main {
  local PASSW="QwAssa312"
  local USR="okmr"
  local AP1="192.168.254.240"
  local AP2="192.168.254.241"
  local AP3="192.168.254.242"
#  packagesInstall
  ssh_cycle "$PASSW" "$USR" "$AP1"
  ssh_cycle "$PASSW" "$USR" "$AP2"
  ssh_cycle "$PASSW" "$USR" "$AP3"
}
########################################################
function ssh_cycle {
  local pass="${1}"
  local user="${2}"
  local serv="${3}"
  sshpass -p ${1} ssh "${2}"@"${3}" 'reboot'
  if [ $? -eq 0 ]; then
     echo `date` "Соединение с ${3} установлено!"
  else
     echo `date` "${3} ошибка авторизации!"
  fi
}
########################################################
function packagesInstall {
  apt -y install sshpass 
}
########################################################