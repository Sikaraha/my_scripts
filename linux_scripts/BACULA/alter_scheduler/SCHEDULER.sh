#!/bin/bash

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/etc/bacula/scripts/alter_scheduler:$PATH
bconsole="/etc/bacula/bconsole"
psql="psql -U bacula"
PIDFILE=/var/run/`basename $0`.pid
DEBUG_FILE=/var/log/bacula/${0##*/}.debug

jobs_max=12

# Через сколько повторять запуск заданий предыдущий запуск которых закончился ошибкой
ignorefault="1 day ago"
# Если флаг установлен задание будет выполнено через $ignorefault (если флаг отсутствует будет игнорировать сфеилившиеся задания и перезапускаться по расписанию)
f=1

declare -a scheduler # Обьявляем переменную массивом

############################################################################################
source backup_scheme.list #|| { echo -e "listig job import error\nExit."; exit 1 }
source schedule.lib #|| { echo -e "Lib import error\nExit."; exit 1 }

############################################################################################
_diff() {       # Исключить повторяющиеся элементы левой части
    for i in $1; do
        a=0
        for n in $2; do
            [ "$i" = "$n" ] && a=1 && break
        done
        [ "$a" = 0 ] && echo $i
    done
}

_log() {
# ВНИМАНИЕ: использование более одного аргумента приведет к выполнению команды
# ВНИМАНИЕ: если вы хотите использовать $@, это можно сделать так: _log "$ (echo $ @)"
    local s i p="$@"
  
    for ((i=3; i<${#FUNCNAME[*]}; i++)); do
        s="${s}  "
    done

    echo "${s}${FUNCNAME[1]}: ${@}" >>$DEBUG_FILE
  
    [[ ${#@} > 1 ]] && ${@}
}

jobsqueue() {
    echo "SELECT Job.JobId AS jobid,
        Job.Name  AS jobname,
        Job.Level     AS level,
        Job.StartTime AS starttime,
        Job.JobFiles  AS jobfiles,
        Job.JobBytes  AS jobbytes,
        Job.JobStatus AS jobstatus, (CURRENT_TIMESTAMP - starttime) AS duration,
        Client.Name AS clientname
        FROM Job INNER JOIN Client USING (ClientId)
        WHERE                                      
        JobStatus IN ('R','B','D','F','S','m','M','s','j','c');" |eval ${psql}
}

# Вернуть список job-ов для исключения из заявленных
sqlreq() {
    local level=$1 starttime=$2 ignoreolder sqlexec outcome

    if [ "${f}" ]; then
        if [ "${ignorefault}" ]; then
            ignoreolder=$(date -d"${ignorefault}" "+%Y-%m-%d %H:%M:%S") || { echo "ERR: Wrong variable \$ignorefault. See 'man date' (-d)" >&2; return 1; }
            sqladd1="AND (Job.jobstatus in ('T','C') OR (Job.jobstatus in ('f','E','A') AND Job.starttime>'${ignoreolder}')) AND (Job.starttime>'${starttime}' OR Job.starttime is NULL)"
        else
            # Принимать jobs failed как ОК
            sqladd1="AND Job.jobstatus in ('T','C','f') AND (Job.starttime>'${starttime}' OR Job.starttime is NULL)"
        fi
    else
        # Игонорировать jobs failed
        sqladd1="AND Job.jobstatus in ('T','C') AND (Job.starttime>'${starttime}' OR Job.starttime is NULL)"
    fi
  
    # Выбираем job's level
    case ${level} in
        F) level="'F'";;
        D) level="'F','D'";;
        I) level="'F','D','I'";;
    esac
  
    sqlexec="SELECT DISTINCT Job.name
            FROM Job
            WHERE Job.type='B'
            AND Job.level IN (${level}) ${sqladd1}
            AND Job.name IN ('$(echo ${jobsdef} |sed "s/\ /','/g")')
            OR Job.jobstatus in ('R')"
  
    _log "SQL REQUEST: $(echo ${sqlexec})"
  
    outcome=$(echo "${sqlexec}" |eval ${psql} -At) || return 1
  
    _log "SQL OUTCOME: $(echo ${outcome})"
    echo "${outcome}"
}

_jobs_full() {
    local period=$(date -d"$1" "+%Y-%m-%d %H:%M:%S") jobsdef sqlrun
    # Возвращает список обьявленных job-ов
    jobsdef=$(eval echo \"\${${FUNCNAME[1]}_jobs}\")

    if [ "${jobsdef}" ];then :
    else
        return 1
    fi
    # Возвращает список текущих job-ов
    sqlrun=$(sqlreq F "${period}") || return 1
    jobsfull=$(_diff "${jobsdef}" "${sqlrun}")

    _log "$(echo ${jobsfull})"
 
    for i in ${jobsfull}; do
        scheduler=("${scheduler[@]}" "$i|Full")
    done
}

_jobs_diff() {
    local period=$(date -d"$1" "+%Y-%m-%d %H:%M:%S") jobsdef sqlrun
    # Возвращает список обьявленных job-ов
    jobsdef=$(eval echo \"\${${FUNCNAME[1]}_jobs}\")

    if [ "${jobsdef}" ]; then :
    else
        return 1
    fi
    # Возвращает список текущих job-ов
    sqlrun=$(sqlreq D "${period}") || return 1
    jobsdiff=$(_diff "${jobsdef}" "${sqlrun} ${jobsfull}")

    _log "$(echo ${jobsdiff})"

    for i in ${jobsdiff}; do
        scheduler=("${scheduler[@]}" "$i|Differential")
    done
}

_jobs_inc() {
    local period=$(date -d"$1" "+%Y-%m-%d %H:%M:%S") jobsdef sqlrun
    # Возвращает список обьявленных job-ов
    jobsdef=$(eval echo \"\${${FUNCNAME[1]}_jobs}\")

    if [ "${jobsdef}" ];then :
    else
        return 1
    fi
    # Возвращает список текущих job-ов
    sqlrun=$(sqlreq I "${period}") || return 1
    jobsinc=$(_diff "${jobsdef}" "${sqlrun} ${jobsdiff} ${jobsfull}")
    _log "$(echo ${jobsinc})i"

    for i in ${jobsinc}; do
        scheduler=("${scheduler[@]}" "$i|Incremental")
    done
}

############################################################################################
#
# Main
#
case "$1" in
    fake) fake=1;;
    queue) jobsqueue; exit;;
    run) ;;
    *) echo "Usage: ${0##*/} {run|fake|queue}"; exit 1;;
esac

#Проверки доступности бд и консоли bacula
printf "\n\n$(date)\n"
echo |eval ${psql} || { echo "ERR: Can not connect to \"${psql}\"" >&2; exit 1; }
echo |eval ${bconsole} >/dev/null || { echo "ERR: Can not connect to \"${bconsole}\"" >&2; exit 1; }

#Проверка pid
if [ -f "$PIDFILE" ]; then
    if ps $(cat $PIDFILE) >/dev/null; then
        echo "ERR: `basename $0` already running!"
        exit 1
    else
        rm -f $PIDFILE
    fi
fi

echo $$ >"$PIDFILE" || { echo "ERR: couldn't create the \$pidfile!"; exit 1; }
trap "rm $PIDFILE" 0 1 3 15

exec 2> >(tee -a $DEBUG_FILE >&2) || exit 1
_log "$(echo)$(date) ###################################################################################"

F6h
F1d
I12h
I1d
F1d
F1w_I1d
F1w_I1d_axa
I1w
D1m_I1d
F1w
F1m_I2h
F1m_I3h
F1m_I1d
F1m_D1w_I1d
F1m_D1w_I1d_nightrun
F1m_D1w_I1d_mssql
F1m
F3m_D1m_I6h
F3m_I1d
F3m_D1m_I1d
F3m_D1m_I1d_nightrun
F6m_D1m_I1d
F6m_I3m
F12m_D3m_I1d

jobs_running=$(jobsqueue| sed -n 's/^(\(.*\) \(row\|стро\).*)$/\1/p')
let jobs_canbe_run=jobs_max-jobs_running
echo "jobs_max=${jobs_max}: jobs_running=${jobs_running}: jobs_canbe_run=${jobs_canbe_run}"

#Проверка возможности запуска относительно общего количества работающий job-ов
if [ ${jobs_canbe_run} -gt 0 ];then :
else
    echo "Limit JOBs exceeded!"
    exit
fi

#Вывод запускаемых job-ов в лог
if [ ${#scheduler[@]} = 0 ];then
    echo "empty"
else
    ind=0
    while [ ${ind} -lt ${#scheduler[@]} ];do
        _log echo "Scheduler: ${scheduler[${ind}]}"
        ((ind++))
    done
fi

#Запуск job-ов
n=0

for i in ${scheduler[@]}; do
    jobname=${i%|*} #Cut all after |
    joblevel=${i#*|} #Cut al before |
    
    #checks
    if [ $n -le ${jobs_canbe_run} ];then :
    else
        break
    fi

    #manual debug
    if [ "${fake}" ]; then
        ((n++))
        continue 
    fi

    echo -n ": run job=${jobname} level=${joblevel} yes. " |tee -a $DEBUG_FILE
    rez=$(echo "run job=${jobname} level=${joblevel} yes" |eval ${bconsole}) || { echo "False!"; continue; }
    echo "${rez}" | grep "JobId=" |tee -a $DEBUG_FILE
    sleep 120
    ((n++))
done

if [ "${fake}" ]; then
    _log echo "FAKE MODE"
fi
