#! /bin/sh
#
# bareos       This shell script takes care of starting and stopping
#        the bareos Storage daemon.
#
# chkconfig: 2345 90 9
# description: Backup Archiving REcovery Open Sourced.
#
#  For Bareos release  (30 January 2019) -- redhat
#

# Source function library
. /etc/rc.d/init.d/functions

SD_USER=bareos
SD_GROUP=bareos
SD_OPTIONS=''
OS=`uname -s`

# if /lib/tls exists, force Bareos to use the glibc pthreads instead
if [ -d "/lib/tls" -a $OS = "Linux" -a `uname -r | cut -c1-3` = "2.4" ] ; then
    export LD_ASSUME_KERNEL=2.4.19
fi

# pull in any user defined SD_OPTIONS, SD_USER, or SD_GROUP
[ -f /etc/sysconfig/bareos ] && . /etc/sysconfig/bareos

#
# Disable Glibc malloc checks, it doesn't help and it keeps from getting
#   good dumps
MALLOC_CHECK_=0
export MALLOC_CHECK_

RETVAL=0
case "$1" in
    start)
        if [ "${SD_GROUP}" != '' ]; then
            SD_OPTIONS="${SD_OPTIONS} -g ${SD_GROUP}"
        fi
        echo -n "Starting Bareos Storage services: "
        daemon --user ${SD_USER} /usr/sbin/bareos-sd $2 ${SD_OPTIONS}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/bareos-sd
        daemon --user ${SD_USER} /usr/sbin/bareos-sd -c bareos-sd-ORACLE-K6.conf $2 ${SD_OPTIONS}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/bareos-sd-D-ORACLE-K6
        daemon --user ${SD_USER} /usr/sbin/bareos-sd -c bareos-sd-ORACLE-KC22.conf $2 ${SD_OPTIONS}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/bareos-sd-ORACLE-KC22
        daemon --user ${SD_USER} /usr/sbin/bareos-sd -c bareos-sd-IBM-KC22 $2 ${SD_OPTIONS}
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/bareos-sd-IBM-KC22
        ;;
    stop)
        echo -n "Stopping Bareos Storage services: "
        killproc /usr/sbin/bareos-sd
        RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && rm -f /var/lock/bareos-sd*
        ;;
    restart)
        $0 stop
        sleep 5
        $0 start
        ;;
    status)
        status /usr/sbin/bareos-sd
        RETVAL=$?
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
        ;;
esac
exit $RETVAL
