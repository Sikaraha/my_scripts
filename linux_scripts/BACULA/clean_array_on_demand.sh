#!/bin/bash

bconsole="/usr/sbin/bconsole"
psql="psql -At -U postgres bareos"

#Вводим номер массива
if [ -z ${1} ]; then
	echo "Enter the ordinal number of the array"
	read -p '/mnt/md_ : ' md_number
else
	md_number=${1}
fi

#Проверяем существует ли точка монтирования 
if [ -d /mnt/md${md_number} ]; then :
else
	echo "Please enter a valid array number!!!"
	exit 1
fi

#Вводим желаемый обьем
if [ -z ${2} ]; then
	echo "Enter the amount of disk space you want to clear (GB)"
	read -p '<100|250|500|1000|5000> : ' FREE_GB
else
	FREE_GB=${2}
fi

case ${FREE_GB} in
	100) FREE="104857600";;
	250) FREE="262144000";;
	500) FREE="524288000";;
	1000) FREE="1048576000";;
	5000) FREE="5242880000";;
	*)	 echo "You must enter a numeric value <100|250|500>"
		 exit 1;;
esac

#Формируем список заданий из имен каталогов в директории с симлинками
list_dir_on_md=$(find /mnt/backup_mirror_global/ -maxdepth 1 -type l -printf '%l\n' | awk -F "/" '$3=="md'"${md_number}"'"{print $4}')

#Преребираем список заданий
for i in ${list_dir_on_md[@]}
do
	jobname=${i}
	#Проверяем существовало ли такое задание
    actual_job=$(echo -e "SELECT DISTINCT name
                        FROM job
                        WHERE name='$jobname';" | ${psql})
    if [ -n "${actual_job}" ]; then
    	#Формирем список скопированных заданий
        copied_vol=$(echo -e "SELECT DISTINCT media.volumename
						FROM job
						JOIN pool ON pool.poolid = job.poolid
						JOIN jobmedia ON jobmedia.jobid = job.jobid
						JOIN media ON media.mediaid = jobmedia.mediaid
						WHERE pool.name IN ('${jobname}-quarter','${jobname}-Full','${jobname}','${jobname}-daily','${jobname}-Inc','${jobname}-Incremental','${jobname}-monthly','${jobname}-Diff','${jobname}-Differential')
						    AND job.type='B'
						    AND job.jobstatus IN ('T','W')
						    AND Job.jobBytes > 0
						    AND job.jobid IN (SELECT priorjobid FROM job WHERE type IN ('B','C') AND job.jobstatus IN ('T','W') AND priorjobid !=0)
						ORDER BY media.volumename;" | ${psql})
		#Проверяем если есть скопированные задания
		if [ -n "${copied_vol}" ]; then
			for volumename in ${copied_vol[@]}
			do
				#Проверяем Дисковое пространство на массиве и если его меньше необходимого удаляем файлы и записи о них в базе
				disk_space_array=$(df /mnt/md${md_number} |awk 'NR==2 {print $4}')
				if [[ ${disk_space_array} -lt ${FREE} ]]; then
					echo "Delete volume file:"
					find -L /mnt/md${md_number} -maxdepth 2 -name ${volumename} -print -delete
					echo "Deleting volume records ${volumename} in db... "
					echo "delete volume=${volumename} yes" |${bconsole} > /dev/null
				else
					echo "Cleaning up is completed!"
					exit 0
				fi
			done
		else
			echo "There are no copied job in pools ${i}"
		fi
	else
		echo "Job ${i} does not exist"
	fi
done

