#!/bin/sh
#
# Shell script to downgrade PostgreSQL tables from Bacula Community version 
#  9.4.x, 7.4.x, 7.2.x, 7.0.x, 5.2.x, 5.0.x
#
echo " "
echo "This script will downgrade a Bacula PostgreSQL database from version 16 to 12"
echo " "
echo "Depending on the current version of your catalog,"
echo "you may have to run this script multiple times."
echo " "

bindir=/usr/pgsql-10/bin
PATH="$bindir:$PATH"
db_name=bacula

ARGS=$*

getVersion()
{   
    DBVERSION=`psql -d ${db_name} -t --pset format=unaligned -c "select VersionId from Version LIMIT 1" $ARGS`
}

getVersion

# For all versions, we need to drop the Index on Media(PoolId/StorageId)
# It may fail, but it's not a big problem
psql -f - -d ${db_name} $* <<END-OF-DATA
set client_min_messages = fatal;
DROP INDEX media_poolid_idx;
DROP INDEX media_storageid_idx;
END-OF-DATA

if [ "$DBVERSION" -eq 16 ] ; then
    if psql -f - -d ${db_name} $* <<END-OF-DATA
begin;
DROP INDEX job_jobtdate_idx;
ALTER TABLE basefiles ALTER COLUMN baseid SET DATA TYPE integer;
ALTER TABLE media DROP COLUMN voltype;
ALTER TABLE media DROP COLUMN volcloudparts;
ALTER TABLE media DROP COLUMN lastpartbytes;
ALTER TABLE media DROP COLUMN cacheretention;
ALTER TABLE pool DROP COLUMN cacheretention;

UPDATE Version SET VersionId=15;
commit;
END-OF-DATA
    then
    echo "Downgrade of Bacula PostgreSQL tables 16 to 15 succeeded."
    getVersion
    else
    echo "Downgrade of Bacula PostgreSQL tables 16 to 15 failed."
    exit 1
    fi
fi

if [ "$DBVERSION" -eq 15 ] ; then
    # from 5.2
    if psql -f - -d ${db_name} $* <<END-OF-DATA
DELETE FROM Status
WHERE JobStatus = 'I'
AND JobStatusLong = 'Incomplete Job'
AND Severity = 25;

ALTER TABLE Media DROP COLUMN volabytes;
ALTER TABLE Media DROP COLUMN volapadding;
ALTER TABLE Media DROP COLUMN volholebytes;
ALTER TABLE Media DROP COLUMN volholes;
ALTER TABLE Media ALTER VolWrites TYPE integer;

DROP INDEX snapshot_idx;
DROP TABLE Snapshot;

UPDATE Version SET VersionId=14;

END-OF-DATA
    then
    echo "Downgrade of Bacula PostgreSQL tables 15 to 14 succeeded."
    getVersion
    else
    echo "Downgrade of Bacula PostgreSQL tables 15 to 14 failed."
    exit 1
    fi
fi

if [ "$DBVERSION" -eq 14 ] ; then
    if psql -f - -U postgres $* <<END-OF-DATA
ALTER DATABASE bacula RENAME TO bareos;
ALTER USER bacula RENAME TO bareos;
ALTER USER bareos PASSWORD '';
END-OF-DATA
    then
    echo "Update database name bacula --> bareos succeeded."
    else
    echo "Update database name bacula failed."
    exit 1
    fi
fi

#######################################################################################

#if [ "$DBVERSION" -eq 14 ] ; then
#    # from 4.0
#    if psql -f - -d ${db_name} $* <<END-OF-DATA
#BEGIN; -- Necessary for Bacula core
#
#ALTER TABLE File DROP COLUMN DeltaSeq;
#
#UPDATE Version SET VersionId=13;
#COMMIT;
#
#-- ANALYSE;
#
#END-OF-DATA
#    then
#    echo "Downgrade of Bacula PostgreSQL tables from 14 to 13 succeeded."
#    getVersion
#    else
#    echo "Downgrade of Bacula PostgreSQL tables failed."
#    exit 1
#    fi
#fi
#
#if [ "$DBVERSION" -eq 13 ] ; then
#    # from 5.0
#    if psql -f - -d ${db_name} $* <<END-OF-DATA
#BEGIN; -- Necessary for Bacula core
#DROP INDEX restore_jobid_idx on RestoreObject(JobId);
#
#DROP TABLE RestoreObject;
#UPDATE Version SET VersionId=12;
#
#COMMIT;
#END-OF-DATA
#    then
#    echo "Downgrade of Bacula PostgreSQL tables 13 to 12 succeeded."
#    getVersion
#    else
#    echo "Downgrade of Bacula PostgreSQL tables 13 to 12 failed."
#    exit 1
#    fi
#fi
