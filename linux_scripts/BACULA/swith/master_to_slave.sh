#!/bin/env bash

#####VARS
declare -i COUNTER MAX_CNT
COUNTER=0
DRIVES=$(ls /dev/tape[0-9])
MOUNTS=$(awk '!/pipefs|\/boot|\ \/\ |\/proc|\/pts|\/shm|\/sys|\/data|devtmp/ {print $2}' /proc/mounts)

#####Func

reset_preventions () {
for DRIVE in $DRIVES; do
sg_prevent --allow --verbose $DRIVE;
done
}

check_ok () { if [ "$?" = "0" ];
then
	printf "%5s\e[1;32m[OK]\e[0m\n"
	 else
	 printf "%5s\e[1;31m[FAIL]\e[0m\n"
fi
}


#####BEGIN

###Stop Bacula
echo "Stopping bacula"
service bacula-dir stop && wait

STOP_SD='service bacula-sd stop'
while [ "$(pgrep bacula-sd)" != "" ];
do $STOP_SD
	sleep 30
		COUNTER=$[$COUNTER+1]
		if [ "$COUNTER" > "5" ];
			then
			kill -9 $(pgrep bacula-sd | xargs)
			check_ok
			reset_preventions
		COUNTER=0
		fi
done
service bacula-fd stop && wait

##Switch fstab
echo -ne "Switching fstab...        "
sed -i.bak '/# ###BEGIN BACKUP1###/,/# ###END BACKUP1###/d' /etc/fstab
check_ok

#
###Unmount ALL storages
echo -ne "Unmounting storages...    "
for MPOINT in $MOUNTS; do 
umount $MPOINT 2> /dev/null; COUNTER=$[COUNTER+$?];
if [ "$COUNTER" != "0" ]; then
echo -e "\t$MPOINT [FAIL]"
	else
	echo -e "\t$MPOINT [OK]"
COUNTER=0
fi
done

###Stop cron
echo -ne "Cleaning up cron jobs...  " &&
find /etc/cron.d/ -type l -delete > /dev/null
/etc/init.d/crond reload
check_ok

##Disable Virtual IP
echo -ne "Disabling Virtual IP's... "
ip addr delete 10.24.2.76/16 dev bond0.100 &&
ip addr delete 10.1.1.158/24 dev bond0.101
check_ok

##Reset preventions
reset_preventions

#mdadm off auto assemble
mv /etc/mdadm.conf /etc/mdadm.conf.old

###END

echo "Don't forget to create postgres standby!"
