#!/bin/bash

date=`date +%Y%m%d`
gateway_ip="$(ip r |awk '/default/{print $3}')"
middle_ip="10.24.2.76"
pgdata_dir="/data/pgsql/10"
mount_points=$(awk '!/pipefs|\/boot|\ \/\ |\/proc|\/pts|\/shm|\/sys|\/data|devtmp/ {print $2}' /proc/mounts)
md_device=$(cat /proc/mdstat |awk '$1~"md" {print $1}')
if tape_drives=$(ls /dev/tape[0-9]) ;then :
else
    echo "WARNING! Tape libraries not found!"
fi
sync_dir_list="
/mnt/backup_mirror_global
/mnt/drive
"
fstab_list="

###BEGIN BACKUP1###

UUID=5302b415:a067097e:eb4278b5:fbd595f8    /mnt/md0    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=dd14e249:fabd1944:96b423f0:2cf5e3ae    /mnt/md1    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=6b51ad1a:de6d7c42:0735cca2:a9bfc5f5    /mnt/md2    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=8a416abb:5dda6cce:0c3b7a10:fb3b7a97    /mnt/md3    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=d028c458:85e9d35d:e4daa134:a22b964e    /mnt/md4    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=546a7c1b:7e944f38:d165e9c3:1cf1425c    /mnt/md5    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=56f79994:7e172380:27368754:ca68a895    /mnt/md6    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=98640836:cd0eed5e:655371b0:203b7c89    /mnt/md7    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=dafad3a9:a5aadb7d:efad647f:7d4db55c    /mnt/md8    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=986ddb24:e7f4d1b4:9389b279:1f78e027    /mnt/md9    xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=afc1d113:1ca0ae5b:503535ab:05600732    /mnt/md10   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=76be90d5:856c81f8:7bd0e516:0a4da231    /mnt/md11   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=aa361d14:500a35b0:16cfb474:eeddf962    /mnt/md12   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=2c21e116:a0f2e486:67825de2:78ac673d    /mnt/md13   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=be3b3b39:6f555f19:6115a6dd:e27b663f    /mnt/md14   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=dfa5df6e:ea7be761:c609247f:d12f4a8c    /mnt/md15   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=6b3ba9e3:f6a87258:fcd54c76:6c85ffb9    /mnt/md16   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=2fa95a9d:b931bd1b:2d4ac447:254fc0ac    /mnt/md17   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=919d70f3:17e27b1f:49f21f0a:cdef468d    /mnt/md18   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=42e6135c:7178a30c:f38b4c3e:bc4089ea    /mnt/md19   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
UUID=f8fb704f:197148cd:62fd7bea:72302f42    /mnt/md20   xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0

#SHARE
#rhg-storage1.rian.off:/sputnikimages	/mnt/rhs/rhg_sputnikimages	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
#rhg-storage1.rian.off:/sputnikimages_arch1	/mnt/rhs/rhg_sputnikimages_arch1	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
#rhg-storage1.rian.off:/sputnikimages_arch2	/mnt/rhs/rhg_sputnikimages_arch2	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
#rhg-storage1.rian.off:/sputnikimages_arch3	/mnt/rhs/rhg_sputnikimages_arch3	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
#rh-storage1-test.rian.off:/testmedia /mnt/rhs/rhg_testmedia glusterfs backupvolfile-server=rhg-storage2-test.rian.off,_netdev 0 0
//qn-0001.msk.rian/foto	/mnt/nas/qn-0001	cifs	credentials=/root/.qn0001_cred	0	0
rhg-storage1.rian.off:/sputnik_media	/mnt/rhs/rhg_sputnik_media	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,rw	0	0
rhg-storage1.rian.off:/ria22_media /mnt/rhs/rhg_ria22_media glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/rian21_media /mnt/rhs/rhg_rian21_media glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/rsport /mnt/rhs/rhg_rsport glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/sputnikimages_vol1 /mnt/rhs/rhg_sputnikimages_vol1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/sputnikimages_vol2 /mnt/rhs/rhg_sputnikimages_vol2 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/sputnikimages_vol3 /mnt/rhs/rhg_sputnikimages_vol3 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/sputnikimages_vol4 /mnt/rhs/rhg_sputnikimages_vol4 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
rhg-storage1.rian.off:/sputnikimages_preview1 /mnt/rhs/rhg_sputnikimages_preview1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0

#NAS-SWZ1
#nas-swz1.msk.rian:/ibm/Postgres_Backup/Postgres_Backup	/mnt/storwize/nas-swz1/postgres	nfs	defaults	0	0
//nas-swz1.msk.rian/WD	/mnt/storwize/WD	cifs	credentials=/root/.swize_cred	0	0
//nas-swz1.msk.rian/PhotoArchives/PhotoArchive19 /mnt/storwize/nas-swz1/PhotoArchive19 cifs credentials=/root/.swize_cred   0   0
//nas-swz1.msk.rian/PhotoArchives/PhotoArchive20 /mnt/storwize/nas-swz1/PhotoArchive20 cifs credentials=/root/.swize_cred   0   0
//nas-swz1.msk.rian/PhotoArchives/PhotoArchive21 /mnt/storwize/nas-swz1/PhotoArchive21 cifs credentials=/root/.swize_cred   0   0
nas-swz1.msk.rian:/ibm/sputnikimages_vol1/sputnikimages_vol1 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol1 nfs rw,hard,intr,timeo=300  0   0
nas-swz1.msk.rian:/ibm/sputnikimages_vol2/sputnikimages_vol2 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol2 nfs rw,hard,intr,timeo=300  0   0
nas-swz1.msk.rian:/ibm/sputnikimages_vol3/sputnikimages_vol3 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol3 nfs rw,hard,intr,timeo=300  0   0
nas-swz1.msk.rian:/ibm/sputnikimages_vol4/sputnikimages_vol4 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol4 nfs rw,hard,intr,timeo=300  0   0
nas-swz1.msk.rian:/ibm/sputnikimages_preview1/sputnikimages_preview1 /mnt/storwize/nas-swz1/rhg_sputnikimages_preview1 nfs rw,hard,intr,timeo=300  0   0

#NAS-SWZ2
nas-swz2.msk.rian:/ibm/rh-storage_backup/rh-storage_backup	/mnt/storwize/rh-storage_backup	nfs	rw,hard,intr,timeo=300	0	0
#nas-swz2.msk.rian:/ibm/Postgres_Backup/postgres_backup	/mnt/storwize/nas-swz2/postgres	nfs	defaults	0	0
#//nas-swz2.msk.rian/video/Authors	/mnt/storwize/nas-swz2/Authors	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Correspondent	/mnt/storwize/nas-swz2/Correspondent	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Exchange	/mnt/storwize/nas-swz2/Exchange	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/Fabnews_Online/volume31	/mnt/storwize/nas-swz2/fabnews-online-vol31	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Archive	/mnt/storwize/nas-swz2/Archive	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Aurora	/mnt/storwize/nas-swz2/Aurora	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Online	/mnt/storwize/nas-swz2/Online	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/RIAN-Video	/mnt/storwize/nas-swz2/RIAN-Video	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/video/Scala	/mnt/storwize/nas-swz2/Scala	cifs	credentials=/root/.swize_cred	0	0
#//nas-swz2.msk.rian/Photo_FTP	/mnt/storwize/Photo_FTP	cifs	credentials=/root/.swize_cred	0	0

#MSSQLDB
#//us-qnap2.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/us-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
//fr-qnap1.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/fr-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0	
//kr-qnap1.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/kr-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
//rs-qnap2.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/rs-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
//dedup03.msk.rian/MSSQL_backup /mnt/nas/mssql    cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0

rhg-storage1.rian.off:/sputnik_media_vol1 /mnt/rhs/rhg_sputnik_media_vol1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
rhg-storage1.rian.off:/sputnik_media_vol2 /mnt/rhs/rhg_sputnik_media_vol2 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
rhg-storage1.rian.off:/sputnik_media_vol3 /mnt/rhs/rhg_sputnik_media_vol3 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
rhg-storage1.rian.off:/sputnik_media_vol4 /mnt/rhs/rhg_sputnik_media_vol4 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
rhg-storage1.rian.off:/sputnik_media_vol5 /mnt/rhs/rhg_sputnik_media_vol5 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0

nas-swz2.msk.rian:/ibm/sputnik_media_vol1/sputnik_media_vol1/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol1 nfs rw,hard,intr,timeo=300  0   0
nas-swz2.msk.rian:/ibm/sputnik_media_vol2/sputnik_media_vol2/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol2 nfs rw,hard,intr,timeo=300  0   0
nas-swz2.msk.rian:/ibm/sputnik_media_vol3/sputnik_media_vol3/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol3 nfs rw,hard,intr,timeo=300  0   0
nas-swz2.msk.rian:/ibm/sputnik_media_vol4/sputnik_media_vol4/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol4 nfs rw,hard,intr,timeo=300  0   0
nas-swz2.msk.rian:/ibm/sputnik_media_vol5/sputnik_media_vol5/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol5 nfs rw,hard,intr,timeo=300  0   0

###END BACKUP1###
"
list_cronfiles="
/etc/bacula/scripts/cronfiles/gluster_backups.cron
/etc/bacula/scripts/cronfiles/rsync-gluster.cron
/etc/bacula/scripts/cronfiles/delete_job_from_backup.cron
/etc/bacula/scripts/cronfiles/kerb_ticket.cron
/etc/bacula/scripts/cronfiles/delete_old_volumes.cron
/etc/bacula/scripts/cronfiles/table_size_stats.cron
/etc/bacula/scripts/cronfiles/schedule.cron
/etc/bacula/scripts/cronfiles/bacula_statistics.cron
/etc/bacula/scripts/cronfiles/tape_usage_zb.cron
/etc/bacula/scripts/cronfiles/sync_global_link.cron
/etc/bacula/scripts/cronfiles/disk_usage_zb.cron
"
arr_list="
# mdadm configuration file

AUTO -all

MAILADDR root

ARRAY /dev/md/0  metadata=1.1 UUID=5302b415:a067097e:eb4278b5:fbd595f8 name=backup1:0
ARRAY /dev/md/1  metadata=1.1 UUID=dd14e249:fabd1944:96b423f0:2cf5e3ae name=backup1:1
ARRAY /dev/md/2  metadata=1.1 UUID=6b51ad1a:de6d7c42:0735cca2:a9bfc5f5 name=backup1:2
ARRAY /dev/md/3  metadata=1.1 UUID=8a416abb:5dda6cce:0c3b7a10:fb3b7a97 name=backup1:3
ARRAY /dev/md/4  metadata=1.1 UUID=d028c458:85e9d35d:e4daa134:a22b964e name=backup1:4
ARRAY /dev/md/5  metadata=1.1 UUID=546a7c1b:7e944f38:d165e9c3:1cf1425c name=backup1:5
ARRAY /dev/md/6  metadata=1.1 UUID=56f79994:7e172380:27368754:ca68a895 name=backup1:6
ARRAY /dev/md/7  metadata=1.2 UUID=98640836:cd0eed5e:655371b0:203b7c89 name=backup1-1.rian.off:7
ARRAY /dev/md/8  metadata=1.1 UUID=dafad3a9:a5aadb7d:efad647f:7d4db55c name=backup1:8
ARRAY /dev/md/9  metadata=1.1 UUID=986ddb24:e7f4d1b4:9389b279:1f78e027 name=backup1:9
ARRAY /dev/md/10  metadata=1.1 UUID=afc1d113:1ca0ae5b:503535ab:05600732 name=backup1:10
ARRAY /dev/md/11  metadata=1.1 UUID=76be90d5:856c81f8:7bd0e516:0a4da231 name=backup1:11
ARRAY /dev/md/12  metadata=1.1 UUID=aa361d14:500a35b0:16cfb474:eeddf962 name=backup1:12
ARRAY /dev/md/13  metadata=1.1 UUID=2c21e116:a0f2e486:67825de2:78ac673d name=backup1:13
ARRAY /dev/md/14  metadata=1.1 UUID=be3b3b39:6f555f19:6115a6dd:e27b663f name=backup1:14
ARRAY /dev/md/15  metadata=1.1 UUID=dfa5df6e:ea7be761:c609247f:d12f4a8c name=backup1:15
ARRAY /dev/md/16  metadata=1.1 UUID=6b3ba9e3:f6a87258:fcd54c76:6c85ffb9 name=backup1:16
ARRAY /dev/md/17  metadata=1.1 UUID=2fa95a9d:b931bd1b:2d4ac447:254fc0ac name=backup1:17
ARRAY /dev/md/18  metadata=1.1 UUID=919d70f3:17e27b1f:49f21f0a:cdef468d name=backup1:18
ARRAY /dev/md/19  metadata=1.1 UUID=42e6135c:7178a30c:f38b4c3e:bc4089ea name=backup1:19
ARRAY /dev/md/20  metadata=1.1 UUID=f8fb704f:197148cd:62fd7bea:72302f42 name=backup1:20
"

check_network() {
    if [ -f /sys/class/net/bond0/operstate ];then :
    else
        echo -e "no interface: bond0 \t\e[1;31m[ERROR!]\e[0m\n"
        echo "Exit."
        exit 1
    fi
	if [ "$(cat /sys/class/net/bond0/operstate)" = "up" ];then :
	elif [ $(ping -c4 -W1 ${gateway_ip} &>/dev/null ;echo $?) -eq 0 ];then :
    else
		echo -e "⚠️  Network error! Exit!"
		exit 1
	fi
}

check_server_role() {
    local srv_hostname="`hostname`"
	local middle_address=$(ip a|grep ${middle_ip})
	if [ -n "${middle_address}" ];then
		echo "Server role: MASTER"
		my_role="master"
		case ${srv_hostname} in
			backup1-1.rian.off ) master_srv=${srv_hostname}
								slave_srv="backup1-2.rian.off";;
			backup1-2.rian.off ) master_srv=${srv_hostname}
								slave_srv="backup1-1.rian.off";;
			*) echo "ERROR: unknown server name! Exit."
				exit 1
		esac
	else
		echo "Server role: SLAVE"
		my_role="slave"
		case ${srv_hostname} in
			backup1-1.rian.off ) master_srv="backup1-2.rian.off"
								slave_srv=${srv_hostname};;
			backup1-2.rian.off ) master_srv="backup1-1.rian.off"
								slave_srv=${srv_hostname};;
			*) echo "ERROR: unknown server name! Exit."
				exit 1
		esac
	fi
}

role_menu() {
	echo -e "\nMake the server role ${for_role_menu}?"
	read -p "Enter [yes/no]: " yn
	case ${yn} in
	    Yes|yes );;
	    No|no ) echo "Exit."
				exit 0;;
	    * ) echo 'Error! Please enter "yes" or "no"'
			role_menu;;
	esac
}

check_ok() {
	if [ "$?" = "0" ];then
		echo -e "\t\e[1;32m[OK]\e[0m"
	else
		echo -e "\t\e[1;31m[FAIL]\e[0m"
	fi
}

sync_link() {
	echo -e "\nSync symlinks..."
	for s_dir in ${sync_dir_list[@]}
	do
		if [ -e ${s_dir} ];then
			rsync -aulv --delete-after ${master_srv}:${s_dir} ${slave_srv}:${s_dir} #&>/dev/null
			if [ "$?" -ge 1 ];then
				echo "Synchronization completed with errors."
				echo "Get out to continue the process of seme roles?"
				read -p "Enter [yes/no] other (resync): " y_n
				case ${y_n} in
				    Yes|yes );;
				    No|no ) echo "Exit."
							exit 0;;
				    * ) echo 'You chose resync.'
						sync_link;;
				esac
			else
				echo -e "Sync ${s_dir} complete: \t\e[1;32m[OK]\e[0m"
			fi
		else
			echo "ERROR ${s_dir} : source directory not found! Exit."
			exit 1
		fi
	done
}

bacula_stop() {
	echo -e "\nStopping bacula daemons..."
	local demon_name_list=(dir sd fd)
	for d in ${demon_name_list[@]}
	do
		/etc/init.d/bacula-${d} stop
		if [ -n "$(pgrep bacula-${d})" ];then
			kill -9 $(pgrep bacula-${d} | xargs)
			check_ok ;echo ": bacula-${d}"
		fi
	done
}

postgres_reconf() {
	echo -e "\nUpgrade postgres DB to ${1}..."
	if [ "${1}" = "master" ] ;then
		rm -v ${pgdata_dir}/recovery.conf
	else
		if [ "${1}" = "slave" ] ;then
			echo "standby_mode = 'on'" > ${pgdata_dir}/recovery.conf
			echo "primary_conninfo = 'host=${master_srv} port=5432 user=postgres'" >> ${pgdata_dir}/recovery.conf
			echo "restore_command = 'scp ${master_srv}:/data/pgsql/10/pg_wal/archive/%f \"%p\"'" >> ${pgdata_dir}/recovery.conf
			echo "archive_cleanup_command = 'ssh ${master_srv} \"/usr/pgsql-10/bin/pg_archivecleanup /data/pgsql/10/pg_wal/archive/ %r\"'" >> ${pgdata_dir}/recovery.conf
		fi
	fi
	service postgresql-10 restart
}

fstab_reconf() {
	echo -e "\nUpdate fstab to ${1}..."
	if [ "${1}" = "master" ] ;then
		echo "${fstab_list}" >> /etc/fstab
		check_ok ;echo ""
	else
		if [ "${1}" = "slave" ] ;then
			cp -Rpv /etc/fstab /etc/fstab.bak
			sed -i '/###BEGIN BACKUP1###/,/###END BACKUP1###/d' /etc/fstab
			check_ok ;echo ""
		fi
	fi
}

unmount_volumes() {
	echo -e "\nUnmount volumes..."
	for u_mpoint in ${mount_points}
	do
		umount ${u_mpoint} 2> /dev/null
		lv=$?
		if [ ${lv} -ne 0 ];then
			echo -e "\t${u_mpoint} \t\e[1;31m[FAIL]\e[0m\n"
		else
			echo -e "\t${u_mpoint} \t\e[1;32m[OK]\e[0m\n"
		fi
	done
}

update_slink_for_cron() {
	echo -e "\nUpdate cron simlinks to ${1}..."
	if [ "${1}" = "master" ] ;then
		echo -e "\nCreate simlink in crom.d..."
		echo "${list_cronfiles}" |while read cron_f
		do
			ln -s ${cron_f} /etc/cron.d/ #> /dev/null
		done
	else
		if [ "${1}" = "slave" ] ;then
			find /etc/cron.d/ -type l -print -delete
		fi
	fi
	service crond restart
}

update_array_status() {
	if [ "${1}" = "master" ] ;then
		echo -e "\nEnable mdadm array..."
		echo "${arr_list}" > /etc/mdadm.conf
		if [ -n ${md_device} ] ;then
			for md_num in ${md_device[@]}
			do
				/sbin/mdadm --stop /dev/${md_num} || echo -e "Failed to stopping device: /dev/${md_num} \t\e[1;31m[FAILED]\e[0m\nExit." ;exit 1
			done
		fi
		/sbin/mdadm --assemble --scan
	else
		if [ "${1}" = "slave" ] ;then
			echo -e "\nDisable mdadm array..."
			cp /etc/mdadm.conf /etc/mdadm.conf.${date}
			echo "AUTO -all" > /etc/mdadm.conf
			if [ -n ${md_device} ] ;then
				for md_num in ${md_device[@]}
				do
					/sbin/mdadm --stop /dev/${md_num}
					check_ok ;echo " :stop device /dev/md${md_num}"
				done
			fi
		fi
	fi
}

reset_preventions() {
	echo -e "\nDisable tape drives..."
	for t_drive in ${tape_drives}
	do
		/usr/bin/sg_prevent --allow --verbose ${t_drive}
		check_ok ;echo ": disable ${t_drive}"
	done
}

remount_fstab() {
	echo -e "\nMount storages..."
	mount -a
}

set_ip() {
	if [ "${1}" = "master" ] ;then
		echo -e "\nEnable middle addresses..."
		middleIP16="10.24.2.76/16"
		middleIP24="10.1.1.158/24"
		pingIP16=$(ping ${middleIP16} -c4 -q -W1 &>/dev/null ;echo $?)
		pingIP24=$(ping ${middleIP24} -c4 -q -W1 &>/dev/null ;echo $?)
		pingTot=$((pingIP16+pingIP24))
		if [ ${pingTot} -le 2 ];then
			if [ ${middleIP16} -le 1 ];then
				echo "⚠️  The address(Middle bond0.100) is already in the network."
			elif [ ${middleIP1024} -le 1 ];then
				echo "⚠️  The address (Middle bond0.101) is already in the network."
			fi
			echo -e "\r\e[1;31m[Setting IP Addresses FAILED]\e[0m\n"
			exit 1
		else
			echo "ifconfig bond0.100:1 ${middleIP16} up"
			ifconfig bond0.100:1 ${middleIP16} up
			echo "arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255" 
			arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255
			echo "ifconfig bond0.101:1 ${middleIP24} up"
			ifconfig bond0.101:1 ${middleIP24} up
			echo "arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255"
			arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255
			echo -e "\r\e[1;32m[IP set SUCESSFULLY]\e[0m\n"
		fi
	else
		if [ "${1}" = "slave" ] ;then
			echo -e "\nDisable middle address..."
			ip addr delete ${middleIP16} dev bond0.100
			check_ok ;echo " :disable bond0.100"
			ip addr delete ${middleIP24} dev bond0.101
			check_ok ;echo " :disable bond0.101"
		fi
	fi
}

bacula_start() {
	echo -e "\nStarting bacula daemons..."
	local demon_name_list=(fd sd dir)
	if [ $(service postgresql-10 status &>/dev/null ;echo $?) -eq 0 ];then
		for d in ${demon_name_list[@]};
		do
			/etc/init.d/bacula-${d} start
		done
	else
		echo -e "Server postgresql10 is not running! \t\e[1;31m[FAILED]\e[0m\n"
		echo -e "Bacula daemons not running! \t\e[1;31m[FAILED]\e[0m\n"
	fi
}

main() {
	clear
    check_network
	check_server_role
	if [ "${my_role}" = "master" ];then
		echo -e "\n################⚠️  WARNING! ⚠️ ################"
		echo "# Attention! DB recovery time on standby server is more than 4 hours!"
		echo "#"
		for_role_menu="slave"
		role_menu
		bacula_stop
		update_slink_for_cron ${for_role_menu}
		sync_link
		fstab_reconf ${for_role_menu}
		remount_fstab
		postgres_reconf ${for_role_menu}
		unmount_volumes
		update_array_status ${for_role_menu}
		set_ip ${for_role_menu}
		reset_preventions
		echo "⚠️  Don't forget to create postgres standby base!"
		echo "⚠️  Run on the slave server: pg_basebackup -D ${pgdata_dir} -Xf -P  -h `hostname` -U postgres"
		echo "⚠️  or used script: create_pgsq_standby.sh"
	else
		avail_ceck=$(ping -c4 -W1 ${middle_ip} &>/dev/null ;echo $?)
		if [ ${avail_ceck} -eq 0 ];then
			echo -e "\n################⚠️  WARNING! ⚠️ ################"
			echo '# The role of this server is "slave"!'
            echo "# Master server available!"
			echo "# Please first run this script on the master server!"
			echo "Exit."
			exit 1 
		else
			echo -e "\n################⚠️  WARNING! ⚠️ ################"
			echo "# Attention! DB recovery time on standby server is more than 4 hours!"
			echo "#"
			for_role_menu="master"
			role_menu
			sync_link
			unmount_volumes
			fstab_reconf ${for_role_menu}
			remount_fstab
			update_slink_for_cron ${for_role_menu}
			update_array_status ${for_role_menu}
			postgres_reconf ${for_role_menu}
			set_ip ${for_role_menu}
			bacula_start
		fi
	fi
}

#######################################################
main "$@"
