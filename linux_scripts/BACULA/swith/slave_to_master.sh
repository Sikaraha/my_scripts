#!/bin/bash


#	Стартю
#	Проверяем свою роль!
#		Если я master предлагаю понизить сервер до slave (ломаем postgres init?)							!!!!!!!
#		Если я слейв проверяю доступность mastera
#			Если доступен выдаю WARNING и предлагаю запустить скрипт на master.
#			Если недоступен предлагаю повыситься, выдаю WARNING время обратного переключения более 4 часов. !!!!!!!
#	
#	
#	
#	
#	

middle_ip="10.24.2.76"

server_role() {
	local bound_address=$(ip a|grep ${middle_ip})
	if [ -n "${bound_address}" ];then
		my_role="master"
	else
		my_role="slave"
	fi
}

master_avail_check() {
	local avail_ceck=$(ping -c4 -W1 ${middle_ip} &>/dev/null ;echo $?)		#А если нет сети?
	if [ ${avail_ceck} -ne 0 ];then
		#
	fi
}

main() {
	server_role
	if [ "${my_role}" = "master" ];then
		#предлагаю понизить сервер до slave (ломаем postgres init?)
	else
		avail_ceck=$(ping -c4 -W1 ${middle_ip} &>/dev/null ;echo $?)
		if [ ${avail_ceck} -eq 0 ];then
			#выдаю WARNING и предлагаю запустить скрипт на master.
		else
			#предлагаю повыситься, выдаю WARNING время обратного переключения более 4 часов
		fi
	fi
}

###############################################################

PGDATA=/data/pgsql/10
MOUNTS=$(awk '!/pipefs|\/boot|\ \/\ |\/proc|\/pts|\/shm|\/sys|\/data|devtmp/ {print $2}' /proc/mounts)
OPT=${2}

menu() {
	echo -e "\nEnter master server:"
	echo "1) backup1-1.rian.off"
	echo "2) backup1-2.rian.off"
	read -p 'Tape pool [1-2]: ' master_sw
	case ${master_sw} in
		1) master="backup1-1.rian.off";;
		2) master="backup1-2.rian.off";;
		*) echo "You need to choose which of the servers will be the next master!" 
			menu;;
	esac
	if [ "${master}" != "${HOSTNAME}" ];then
		echo "master server is ${master}, aborting."
		exit 1
	fi
}

check_ok () {
if [ "$?" = "0" ];then
	printf "%5s\e[1;32m[OK]\e[0m\n"
else
	printf "%5s\e[1;31m[FAIL]\e[0m\n"
fi
}

storumnt() {
	for MPOINT in ${MOUNTS}
	do
		umount ${MPOINT} 2> /dev/null
		lv=$?
		COUNTER=$[COUNTER+${lv}]
		TOTCOUNTER=$[TOTCOUNTER+${lv}]
		if [ "${COUNTER}" = "0" ];then
			printf "%5s\e[1;32m[OK]\e[0m\n"
		else
			printf "%5s\e[1;31m[FAIL]\e[0m\n"
			COUNTER=0
		fi
	done
	if [ "${TOTCOUNTER}" = "0" ];then
		printf "%5s\e[1;32m[Umount SUCESSFUL]\e[0m\n"
	else
		printf "%5s\e[1;31m[Umount FAILED]\e[0m\n"
	fi
}

mvfstab() {
	local fstab_list="
	
	###BEGIN BACKUP1###
	
	/dev/md0 /mnt/md0                                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md1 /mnt/md1                                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md2 /mnt/md2                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md3 /mnt/md3                               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md4 /mnt/md4                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md5 /mnt/md5                                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md6 /mnt/md6                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md7 /mnt/md7                                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md8 /mnt/md8                                xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md9 /mnt/md9                               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md10 /mnt/md10                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md11 /mnt/md11               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md12 /mnt/md12                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md13 /mnt/md13                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md14 /mnt/md14                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md15 /mnt/md15                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md16 /mnt/md16                     xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md17 /mnt/md17               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md18 /mnt/md18               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md19 /mnt/md19               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	/dev/md20 /mnt/md20               xfs rw,noquota,logbufs=8,logbsize=256k,inode64 0 0
	
	#SHARE
	#rhg-storage1.rian.off:/sputnikimages	/mnt/rhs/rhg_sputnikimages	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
	#rhg-storage1.rian.off:/sputnikimages_arch1	/mnt/rhs/rhg_sputnikimages_arch1	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
	#rhg-storage1.rian.off:/sputnikimages_arch2	/mnt/rhs/rhg_sputnikimages_arch2	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
	#rhg-storage1.rian.off:/sputnikimages_arch3	/mnt/rhs/rhg_sputnikimages_arch3	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,ro	0	0
	#rh-storage1-test.rian.off:/testmedia /mnt/rhs/rhg_testmedia glusterfs backupvolfile-server=rhg-storage2-test.rian.off,_netdev 0 0
	//qn-0001.msk.rian/foto	/mnt/nas/qn-0001	cifs	credentials=/root/.qn0001_cred	0	0
	rhg-storage1.rian.off:/sputnik_media	/mnt/rhs/rhg_sputnik_media	glusterfs	backupvolfile-server=rhg-storage2.rian.off,_netdev,rw	0	0
	rhg-storage1.rian.off:/ria22_media /mnt/rhs/rhg_ria22_media glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/rian21_media /mnt/rhs/rhg_rian21_media glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/rsport /mnt/rhs/rhg_rsport glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/sputnikimages_vol1 /mnt/rhs/rhg_sputnikimages_vol1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/sputnikimages_vol2 /mnt/rhs/rhg_sputnikimages_vol2 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/sputnikimages_vol3 /mnt/rhs/rhg_sputnikimages_vol3 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/sputnikimages_vol4 /mnt/rhs/rhg_sputnikimages_vol4 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	rhg-storage1.rian.off:/sputnikimages_preview1 /mnt/rhs/rhg_sputnikimages_preview1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev,rw 0 0
	
	#NAS-SWZ1
	#nas-swz1.msk.rian:/ibm/Postgres_Backup/Postgres_Backup	/mnt/storwize/nas-swz1/postgres	nfs	defaults	0	0
	//nas-swz1.msk.rian/WD	/mnt/storwize/WD	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz1.msk.rian/PhotoArchives/PhotoArchive19 /mnt/storwize/nas-swz1/PhotoArchive19 cifs credentials=/root/.swize_cred   0   0
	//nas-swz1.msk.rian/PhotoArchives/PhotoArchive20 /mnt/storwize/nas-swz1/PhotoArchive20 cifs credentials=/root/.swize_cred   0   0
	//nas-swz1.msk.rian/PhotoArchives/PhotoArchive21 /mnt/storwize/nas-swz1/PhotoArchive21 cifs credentials=/root/.swize_cred   0   0
	nas-swz1.msk.rian:/ibm/sputnikimages_vol1/sputnikimages_vol1 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol1 nfs rw,hard,intr,timeo=300  0   0
	nas-swz1.msk.rian:/ibm/sputnikimages_vol2/sputnikimages_vol2 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol2 nfs rw,hard,intr,timeo=300  0   0
	nas-swz1.msk.rian:/ibm/sputnikimages_vol3/sputnikimages_vol3 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol3 nfs rw,hard,intr,timeo=300  0   0
	nas-swz1.msk.rian:/ibm/sputnikimages_vol4/sputnikimages_vol4 /mnt/storwize/nas-swz1/rhg_sputnikimages_vol4 nfs rw,hard,intr,timeo=300  0   0
	nas-swz1.msk.rian:/ibm/sputnikimages_preview1/sputnikimages_preview1 /mnt/storwize/nas-swz1/rhg_sputnikimages_preview1 nfs rw,hard,intr,timeo=300  0   0
	
	#NAS-SWZ2
	nas-swz2.msk.rian:/ibm/rh-storage_backup/rh-storage_backup	/mnt/storwize/rh-storage_backup	nfs	rw,hard,intr,timeo=300	0	0
	nas-swz2.msk.rian:/ibm/Postgres_Backup/postgres_backup	/mnt/storwize/nas-swz2/postgres	nfs	defaults	0	0
	//nas-swz2.msk.rian/video/Authors	/mnt/storwize/nas-swz2/Authors	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Correspondent	/mnt/storwize/nas-swz2/Correspondent	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Exchange	/mnt/storwize/nas-swz2/Exchange	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/Fabnews_Online/volume31	/mnt/storwize/nas-swz2/fabnews-online-vol31	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Archive	/mnt/storwize/nas-swz2/Archive	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Aurora	/mnt/storwize/nas-swz2/Aurora	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Online	/mnt/storwize/nas-swz2/Online	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/RIAN-Video	/mnt/storwize/nas-swz2/RIAN-Video	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/video/Scala	/mnt/storwize/nas-swz2/Scala	cifs	credentials=/root/.swize_cred	0	0
	//nas-swz2.msk.rian/Photo_FTP	/mnt/storwize/Photo_FTP	cifs	credentials=/root/.swize_cred	0	0
	
	#MSSQLDB
	#//us-qnap2.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/us-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
	//fr-qnap1.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/fr-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0	
	//kr-qnap1.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/kr-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
	//rs-qnap2.msk.rian/Backup/MSSQLbackup	/mnt/nas/mssql/rs-digispotsql	cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
	//dedup03.msk.rian/MSSQL_backup /mnt/nas/mssql    cifs    credentials=/root/.swize_mssql_cred,dom=MSK.RIAN,sec=krb5   0   0
	
	rhg-storage1.rian.off:/sputnik_media_vol1 /mnt/rhs/rhg_sputnik_media_vol1 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
	rhg-storage1.rian.off:/sputnik_media_vol2 /mnt/rhs/rhg_sputnik_media_vol2 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
	rhg-storage1.rian.off:/sputnik_media_vol3 /mnt/rhs/rhg_sputnik_media_vol3 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
	rhg-storage1.rian.off:/sputnik_media_vol4 /mnt/rhs/rhg_sputnik_media_vol4 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
	rhg-storage1.rian.off:/sputnik_media_vol5 /mnt/rhs/rhg_sputnik_media_vol5 glusterfs backupvolfile-server=rhg-storage2.rian.off,_netdev 0 0
	
	nas-swz2.msk.rian:/ibm/sputnik_media_vol1/sputnik_media_vol1/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol1 nfs rw,hard,intr,timeo=300  0   0
	nas-swz2.msk.rian:/ibm/sputnik_media_vol2/sputnik_media_vol2/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol2 nfs rw,hard,intr,timeo=300  0   0
	nas-swz2.msk.rian:/ibm/sputnik_media_vol3/sputnik_media_vol3/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol3 nfs rw,hard,intr,timeo=300  0   0
	nas-swz2.msk.rian:/ibm/sputnik_media_vol4/sputnik_media_vol4/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol4 nfs rw,hard,intr,timeo=300  0   0
	nas-swz2.msk.rian:/ibm/sputnik_media_vol5/sputnik_media_vol5/ /mnt/storwize/nas-swz2/rhg_sputnik_media_vol5 nfs rw,hard,intr,timeo=300  0   0
	
	###END BACKUP1###
	"
	echo "${fstab_list}" >> /etc/fstab
	check_ok
}

docron () {
	local list_cronfiles="
	/etc/bacula/scripts/cronfiles/gluster_backups.cron
	/etc/bacula/scripts/cronfiles/rsync-gluster.cron
	/etc/bacula/scripts/cronfiles/delete_job_from_backup.cron
	/etc/bacula/scripts/cronfiles/kerb_ticket.cron
	/etc/bacula/scripts/cronfiles/delete_old_volumes.cron
	/etc/bacula/scripts/cronfiles/table_size_stats.cron
	/etc/bacula/scripts/cronfiles/schedule.cron
	/etc/bacula/scripts/cronfiles/backup1_backup.cron
	/etc/bacula/scripts/cronfiles/bacula_statistics.cron
	/etc/bacula/scripts/cronfiles/tape_usage_zb.cron
	/etc/bacula/scripts/cronfiles/sync_global_link.cron
	/etc/bacula/scripts/cronfiles/disk_usage_zb.cron
	"
	echo "${list_cronfiles}" |while read cron_f
	do
		ln -s ${cron_f} /etc/cron.d/ > /dev/null
	done
	check_ok
	/etc/init.d/crond restart
}

mdmnt() {
	local arr_list="
	# mdadm configuration file
	
	AUTO -all
	
	MAILADDR root
	
	ARRAY /dev/md/0  metadata=1.1 UUID=5302b415:a067097e:eb4278b5:fbd595f8 name=backup1:0
	ARRAY /dev/md/1  metadata=1.1 UUID=dd14e249:fabd1944:96b423f0:2cf5e3ae name=backup1:1
	ARRAY /dev/md/2  metadata=1.1 UUID=6b51ad1a:de6d7c42:0735cca2:a9bfc5f5 name=backup1:2
	ARRAY /dev/md/3  metadata=1.1 UUID=8a416abb:5dda6cce:0c3b7a10:fb3b7a97 name=backup1:3
	ARRAY /dev/md/4  metadata=1.1 UUID=d028c458:85e9d35d:e4daa134:a22b964e name=backup1:4
	ARRAY /dev/md/5  metadata=1.1 UUID=546a7c1b:7e944f38:d165e9c3:1cf1425c name=backup1:5
	ARRAY /dev/md/6  metadata=1.1 UUID=56f79994:7e172380:27368754:ca68a895 name=backup1:6
	ARRAY /dev/md/7  metadata=1.2 UUID=98640836:cd0eed5e:655371b0:203b7c89 name=backup1-1.rian.off:7
	ARRAY /dev/md/8  metadata=1.1 UUID=dafad3a9:a5aadb7d:efad647f:7d4db55c name=backup1:8
	ARRAY /dev/md/9  metadata=1.1 UUID=986ddb24:e7f4d1b4:9389b279:1f78e027 name=backup1:9
	ARRAY /dev/md/10  metadata=1.1 UUID=afc1d113:1ca0ae5b:503535ab:05600732 name=backup1:10
	ARRAY /dev/md/11  metadata=1.1 UUID=76be90d5:856c81f8:7bd0e516:0a4da231 name=backup1:11
	ARRAY /dev/md/12  metadata=1.1 UUID=aa361d14:500a35b0:16cfb474:eeddf962 name=backup1:12
	ARRAY /dev/md/13  metadata=1.1 UUID=2c21e116:a0f2e486:67825de2:78ac673d name=backup1:13
	ARRAY /dev/md/14  metadata=1.1 UUID=be3b3b39:6f555f19:6115a6dd:e27b663f name=backup1:14
	ARRAY /dev/md/15  metadata=1.1 UUID=dfa5df6e:ea7be761:c609247f:d12f4a8c name=backup1:15
	ARRAY /dev/md/16  metadata=1.1 UUID=6b3ba9e3:f6a87258:fcd54c76:6c85ffb9 name=backup1:16
	ARRAY /dev/md/17  metadata=1.1 UUID=2fa95a9d:b931bd1b:2d4ac447:254fc0ac name=backup1:17
	ARRAY /dev/md/18  metadata=1.1 UUID=919d70f3:17e27b1f:49f21f0a:cdef468d name=backup1:18
	ARRAY /dev/md/19  metadata=1.1 UUID=42e6135c:7178a30c:f38b4c3e:bc4089ea name=backup1:19
	ARRAY /dev/md/20  metadata=1.1 UUID=f8fb704f:197148cd:62fd7bea:72302f42 name=backup1:20
	"
	echo "${arr_list}" > /etc/mdadm.conf
	check_ok
	mdadm --assemble --scan
}

mfst() {
	echo "Mounting fstab...         "
	mount -av
	check_ok
}

pswitch() {
	TFILE=$(awk -v q="'" '/trigger_file/{gsub( "^'\''|"q"$", "", $3); print $3}' ${PGDATA}/recovery.conf)
	touch ${PGDATA}/${TFILE}
	check_ok
}

setip() {
	middleHost=${master}
	middleIP16=10.24.2.76/16
	middleIP24=10.1.1.158/24
	echo -e "\nNetwork interfaces..."
	###Check that middle addresses isn't already set.
	#chk=$(ip a | grep -A5 'bond[0-9]\:' | grep 'inet\ ' | awk '#'$a'#{a=1}#'$b'#{b=1}END{exit!(a&&b)}'; echo $?)
	#if [ $chk = "0" ];
	#then
	#	echo "Addresses already in use, exiting."
	#	exit 1
	#fi
	ping ${middleHost} -c 10 -q -w 30 &>/dev/null
	pingHost=$?
	ping ${middleIP16} -c 10 -q -w 30 &>/dev/null
	pingIP16=$?
	ping ${middleIP24} -c 10 -q -w 30 &>/dev/null
	pingIP24=$?
	pingTot= $((pingHost+pingIP16+pingIP24))
	if [ ${pingTot} -ne 3 ];then
		if [ ${pingHost} -ne 1 ];then
			echo "Ping backup1.rian.off returned exit status ${pingHost}."
		elif [ ${middleIP16} -ne 1 ];then
			echo "Ping 10.24.2.76 (Middle bond0.100) returned exit status ${pingIP16}."
		elif [ ${middleIP1024} -ne 1 ];then
			echo "Ping 10.1.1.158 (Middle bond0.101) returned exit status ${pingIP24}."
		fi
		printf "%5s\e[1;31m[Setting IP Addresses FAILED]\e[0m\n"
		exit 1
	else
		echo "ifconfig bond0.100:1 ${middleIP16} up"
		ifconfig bond0.100:1 ${middleIP16} up
		echo "arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255" 
		arping -c 5 -I bond0.100 -s ${middleIP16%%/*} 255.255.255.255
		echo "ifconfig bond0.101:1 ${middleIP24} up"
		ifconfig bond0.101:1 ${middleIP24} up
		echo "arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255"
		arping -c 5 -I bond0.101 -s ${middleIP24%%/*} 255.255.255.255
		printf "%5s\e[1;32m[IP set SUCESSFULLY]\e[0m\n"
	fi
}

main() {
	case ${OPT} in
		net)
			setip;;
		exec)
			storumnt
			mvfstab
			docron
			mdmnt
			mfst
			pswitch		# uncommented by apetrov 2018-10-03
			setip;;
		*)
			echo "ERR!"
			exit 1;;
	esac
}

#######################################################
main "$@"

##Up bacula
#/etc/init.d/bacula-sd start 
#/etc/init.d/bacula-dir start
#/etc/init.d/bacula-fd start

###POSTGRES????

###END
