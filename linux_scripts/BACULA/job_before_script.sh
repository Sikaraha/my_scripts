#!/bin/bash

date=`date +%Y%m%d-%H.%M`
bconsole="/usr/sbin/bconsole"
jobname_at=${1}
jobid_at=${2}
level_at=${3}
pool_at=${4}
storage_at=${5}

vol_label() {
    #Проверяем свободное место на массиве и количество открытых файлов
    #Создаем фаил-volume в каталоге drive
    local arr_num=${1}
    local label_result
    local FREE=$(df -k /mnt/md${arr_num} | awk '/dev/ {print $4}')
    local open_f=$(lsof /mnt/md${arr_num} | awk '/^bareos-sd/ && !/\/mnt\/md1\/working/ {print $9}' |wc -l)
    if [[ ${FREE} -gt 943718400 ]]; then            # 943718400 Kb = 900GB
        if [[ ${open_f} -ge 2 ]]; then              # 2 = количество открытых файлов на массиве
            echo "md${arr_num} is busy with another operation"
        else
            label_result=$(echo "label volume=${jobname_at}-${jobid_at}-${level_at}-${date}_${arr_num} pool=${pool_at} storage=${storage_at} yes"|${bconsole} |awk '/failed/||/3000 OK label/')
            echo "${label_result}"
            label_result=$(echo -e "${label_result}"|awk '{if (/failed/||/3000 OK label/) print $2;}')
            case ${label_result} in
                OK ) :;;
                *) echo "label error"
                   exit 1;;
            esac
        fi
    else
        echo "disk space exhausted on the md${arr_num}"
    fi
}

linked() {
    #Проверяем наличие файла-volume в каталоге drive
    #Перемещаем его на нужный массив и создаем симлинк
    local arr_num=${1}
    local vol_file="/mnt/drive/${jobname_at}-${jobid_at}-${level_at}-${date}_${arr_num}"
    local link_file="/mnt/md${arr_num}/${jobname_at}-${jobid_at}-${level_at}-${date}_${arr_num}"
    if [[ -f ${vol_file} ]]; then
        mv ${vol_file} ${link_file}
        ln -s ${link_file} ${vol_file}
    #else
        #echo "link creation error: file ${vol_file} not found!"
    fi
}

main() {
    local pool_type=$(echo "${pool_at}"|grep "pool-lto")
    if [ -z ${pool_type} ] ;then
        for ((i=1; i < 21; i++))            # 21-Колчество массивов
        do
            vol_label ${i}
            sleep 3
            linked ${i}
        done
    else
        echo "This job is for a tape pool ${pool_at}!"
    fi
}

main


