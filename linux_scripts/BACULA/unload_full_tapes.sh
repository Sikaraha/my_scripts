#!/bin/bash

psql="psql -At -U bacula bacula"
bconsole="/etc/bacula/bconsole"

pool_menu() {
	echo -e "\nВыберете tape pool:"
	echo -e "1) \e[\e[38;5;137mpool-lto6-k6\e[0m"
	echo -e "2) \e[\e[38;5;52mpool-lto6-k6-1\e[0m"
	echo -e "3) \e[\e[38;5;50mpool-lto6-k6-2\e[0m"
	echo -e "4) \e[1;32mpool-lto6-cpp\e[0m"
	echo -e "5) \e[1;31mpool-lto6-cpp-1\e[0m"
	echo -e "6) \e[1;34mpool-lto6-cpp-2\e[0m"
	read -p 'Tape pool [1-6]: ' tape_pool
	case ${tape_pool} in
		1) tape_library=0
			storage="Storage-oracle-LTO-1"
			pool="pool-lto6-k6";;
		2) tape_library=0
			storage="Storage-oracle-LTO-2"
			pool="pool-lto6-k6-1";;
		3) tape_library=0
			storage="Storage-oracle-LTO-3"
			pool="pool-lto6-k6-2";;
		4) tape_library=1
			storage="Storage-oracle-LTO-4"
			pool="pool-lto6-cpp";;
		5) tape_library=1
			storage="Storage-oracle-LTO-5"
			pool="pool-lto6-cpp-1";;
		6) tape_library=1
			storage="Storage-oracle-LTO-6"
			pool="pool-lto6-cpp-2";;
		*) echo "Вы выбрали неверный pool. Exit" 
			pool_menu;;
	esac
}

num_cassettes() {
	echo -e "\nУкажите количество кассет которые необходимо извлеч из ${pool}."
	read -p "Введите [1-${#mslots[@]}]: " num_c
	if [ -z ${num_c} ]; then
		num_cassettes
	elif [ ${num_c} -le 0 ]; then
		num_cassettes
	elif [ ${num_c} -gt ${#mslots[@]} ]; then
		echo "⚠️  В mailslot недостаточно места!"
		num_cassettes
	fi
}

unload_cycle() {
	y=0
	while [[ ${y} -lt ${1} ]]
	do
		unload_tape ${slot_in_lib[${y}]} ${mslots[${y}]}
		((y++))
	done
}

unload_tape() {
	if mtx -f /dev/changer${tape_library} transfer ${1} ${2} ;then
		echo -e "transfer ${1} --> ${2} \t\e[1;32m[DONE]\e[0m"
	else
		echo -e "transfer ${1} --> ${2} \t\e[1;31m[ERROR]\e[0m"
		exit 1
	fi
}

main() {
	clear
	pool_menu
	mslots=($(mtx -f /dev/changer${tape_library} status |awk '{if ($4=="IMPORT/EXPORT:Empty") print $3}'))
	echo -e "\nВ mailslot сводобно слотов: ${#mslots[@]}"
	num_cassettes
	echo -e "\nUpdatyng slots in Library...\n"
	echo "update slots storage=${storage} drive=2" |${bconsole} > /dev/null
	slot_in_lib=($(echo "SELECT media.slot
					FROM media
					JOIN pool ON pool.poolid = media.poolid
					WHERE pool.name='$pool'
					AND media.slot > 0
					AND media.volstatus = 'Full'
					ORDER BY media.lastwritten
					LIMIT 4;" |${psql}))
	unload_cycle ${num_c}
}
#######################################################
main "$@"
