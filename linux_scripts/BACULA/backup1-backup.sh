#!/bin/bash

cur_date=$(date +%Y%m%d)
backup_dir="/mnt/md16/backup1/"
arch_logs_dir="/data/wal_logs/"

number_of_system_files=$(ls $backup_dir/root.file.system-*.tar.gz | wc -l)
if [[ ${number_of_system_files} -gt 7 ]]; then
   /usr/bin/find ${backup_dir} -type f -regex ".*root.file.system-.*.tar.gz.*" -ctime +14 -exec rm -f {} \;
fi

# delete old db archive logs
# http://kb.dmz.off:3000/wiki/11/Baculadb_failover
number_of_arch_files=$(ls $arch_logs_dir | wc -l)
if [ ${number_of_arch_files} -gt 100 ]; then
   /usr/bin/find ${arch_logs_dir} -type f -ctime +14 -exec rm -f {} \;
fi

/usr/bin/rsync -aq --one-file-system --exclude "/var/bacula/working" --exclude "/bacula_db" --exclude "/home" --exclude "/data" --exclude "/mnt" --exclude "/proc" --exclude "/sys" --exclude "/tmp" --exclude "/xino" / ${backup_dir}/root.file.system/

cd ${backup_dir} && tar -czf root.file.system-${cur_date}.tar.gz root.file.system/ >/dev/null 2>&1

