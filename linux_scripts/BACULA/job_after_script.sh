#!/bin/bash

psql="psql -At -U postgres bareos"
bconsole="/usr/sbin/bconsole"
name=${1}
id=${2}
sql_query1=($(echo "SELECT DISTINCT volumename
                FROM media
                WHERE volumename LIKE '%$name%'
                AND volumename LIKE '%$id%'
                AND volbytes < 1024;" |${psql}))
sql_query2=($(echo "SELECT DISTINCT volumename
                FROM media
                WHERE volumename LIKE '%$name%'
                AND volumename LIKE '%$id%'
                AND volbytes > 1024;" |${psql}))
append_vol=${#sql_query1[@]}
itr=1

#Проверяем есть пустые volune 
if [ -n "${sql_query1}" ]; then
    #Проверяем есть ли полные volume
    if [ -n "${sql_query2}" ]; then
        #Если есть полные удаляем все пустые volume
        for vol_name in ${sql_query1[@]}
        do
            echo "Deleting records volume ${vol_name} in db..."
            echo "delete volume=${vol_name} yes"|${bconsole} > /dev/null
            rm -v /mnt/*/${vol_name}
        done
    else
        #Если нет полных удаляем все пустые кроме первого
        while [ ${itr} -lt ${append_vol} ]
        do
            echo "Deleting records volume ${sql_query1[${itr}]} in db..."
            echo "delete volume=${sql_query1[${itr}]} yes"|${bconsole} > /dev/null
            rm -v /mnt/*/${sql_query1[${itr}]}
            ((itr++))
        done
        #Оставшийся пустой помечаем как Full что бы предотвратить записбь на него следующего jobа
        echo "Update volstatus volume ${sql_query1[0]} in db..."
        echo "update volstatus=Full volume=${sql_query1[0]} yes"|${bconsole} > /dev/null
    fi
fi
