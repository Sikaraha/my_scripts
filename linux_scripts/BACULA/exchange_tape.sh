#!/bin/bash

psql="psql -At -U bacula bacula"
bconsole="/etc/bacula/bconsole"
tmp_file="/tmp/bconsole_command.tmp"

pool_menu() {
	echo -e "\nВыберете tape pool:"
	echo -e "1) \e[\e[38;5;137mpool-lto6-k6\e[0m"
	echo -e "2) \e[\e[38;5;52mpool-lto6-k6-1\e[0m"
	echo -e "3) \e[\e[38;5;50mpool-lto6-k6-2\e[0m"
	echo -e "4) \e[1;32mpool-lto6-cpp\e[0m"
	echo -e "5) \e[1;31mpool-lto6-cpp-1\e[0m"
	echo -e "6) \e[1;34mpool-lto6-cpp-2\e[0m"
	read -p 'Tape pool [1-6]: ' tape_pool
	case ${tape_pool} in
		1) tape_library=0
			storage="Storage-oracle-LTO-1"
			drive="0"
			pool="pool-lto6-k6";;
		2) tape_library=0
			storage="Storage-oracle-LTO-2"
			drive="1"
			pool="pool-lto6-k6-1";;
		3) tape_library=0
			storage="Storage-oracle-LTO-3"
			drive="2"
			pool="pool-lto6-k6-2";;
		4) tape_library=1
			storage="Storage-oracle-LTO-4"
			drive="0"
			pool="pool-lto6-cpp";;
		5) tape_library=1
			storage="Storage-oracle-LTO-5"
			drive="1"
			pool="pool-lto6-cpp-1";;
		6) tape_library=1
			storage="Storage-oracle-LTO-6"
			drive="2"
			pool="pool-lto6-cpp-2";;
		*) echo "Вы выбрали неверный pool." 
			pool_menu;;
	esac
}

check_cassettes() {
	cassettes_in_base=($(echo "SELECT DISTINCT volumename
							FROM media
							WHERE volumename='$1'"|${psql}))
	if [ -n "${cassettes_in_base}" ]; then
		echo "⚠️  Кассета ${cassettes_in_base} уже использовалась!"
		#Обнаружив использованную кассету заменяем нулевым значение с таким же индексом в массиве (mslots) с номерами слотов этих кассет 
		mslots[${2}]=""
	fi
}

check_cycle() {
	local n=0
	if [[ -n ${mailslot_cassettes[@]} ]]; then
		for c in ${mailslot_cassettes[@]}
		do
			check_cassettes ${c} ${n}
			((n++))
		done
	else
		echo -e "\e[1;31m[ERROR]\e[0m: Mailslot пуст! пожалуйста вставьте чистые кассеты! Exit."
		exit 1
	fi
}

find_transfer_slot() {
	slots_in_drive=($(echo "${changer_status}" |awk '{if ($1=="Data") print $7}'))
	empty_slots=($(echo "${changer_status}" |awk '{if ($1=="Storage") if ($3 ~ ":Empty") print $3}'|sed s/[^0-9]//g))
	
	for i in ${slots_in_drive[@]}
	do
		for n in ${empty_slots[@]}
        do
            if [ ${i} != ${n} ];then
                probably_slot=${n}
            fi
        done
	done

	if [ -n ${probably_slot} ]; then
		transfer_slot=${probably_slot}
	else
		echo -e "\e[1;31m[ERROR]\e[0m: В библиотеке нет необходимого количества пустых слотов!"
		echo -e "\e[1;31m[ERROR]\e[0m: Пожалуйста освободите хотя бы один слот! Exit."
		exit 1
	fi
}

num_cassettes() {
	#Избавляемся от пустых значений в массиве mslots
	mslots=($(echo ${mslots[@]}))
	if [ ${#mslots[@]} -gt 0 ]; then
		echo "Укажите количество кассет которые необходимо заменить в ${pool}."
		read -p "Введите [1-${#mslots[@]}]: " num_c
		if [ -z ${num_c} ]; then
			num_cassettes
		elif [ ${num_c} -le 0 ]; then
			num_cassettes
		elif [ ${num_c} -gt ${#mslots[@]} ]; then
			echo -e "\e[1;31m[ERROR]\e[0m: В mailslot нет такого количества чистых кассет!"
			exit 1
		fi
	else
		echo -e "\e[1;31m[ERROR]\e[0m: В mailslot нет чистых кассет для замены! Exit."
        exit 1
	fi
}

exchange_tape() {
	local y=0
	while [[ ${y} -lt ${1} ]]
	do
		transfer_tape ${mslots[${y}]} ${transfer_slot}
		echo -e "load transferslot ${mslots[${y}]} --> ${transfer_slot} \t\e[1;32m[DONE]\e[0m"
		transfer_tape ${slot_in_lib[${y}]} ${mslots[${y}]}
		echo -e "unload in mailslot ${slot_in_lib[${y}]} --> ${mslots[${y}]} \t\e[1;32m[DONE]\e[0m"
		transfer_tape ${transfer_slot} ${slot_in_lib[${y}]}
		echo -e "load from transferslot ${transfer_slot} --> ${slot_in_lib[${y}]} \t\e[1;32m[DONE]\e[0m"
		((y++))
	done
}

transfer_tape() {
	if mtx -f /dev/changer${tape_library} transfer ${1} ${2} ;then :
	else
		echo -e "${1} --> ${2} transfer \e[6;31merror\e[0m"
		exit 1
	fi
}

label_cycle() {
	local y=0
	while [[ ${y} -lt ${1} ]]
	do
		labeling_in_pool ${drive} ${pool} ${slot_in_lib[${y}]} 
		#echo -e "\nСделать label для кассеты из слота ${slot_in_lib[${y}]} in ${pool}?"
	   	#read -p "Введите [y/n]: " yn
    	#case $yn in
    	#    [Yy]) labeling_in_pool ${drive} ${pool} ${slot_in_lib[${y}]};;
    	#    [Nn]) echo "⚠️  Позднее в bconsole обязательно выполните:"
		#		echo "label media storage=${storage} drive=${drive} pool=${pool} barcodes slot=${slot_in_lib[${y}]}";;
    	#    * ) echo 'Пожалуйста выберете "y" или "n".';;
    	#esac
		((y++))
	done
}

labeling_in_pool() {
	local stor_k6="
	Storage-oracle-LTO-2
	Storage-oracle-LTO-1
	Storage-oracle-LTO-3
	"
	local stor_cpp="
	Storage-oracle-LTO-4
	Storage-oracle-LTO-5
	Storage-oracle-LTO-6
	"
	case ${2} in
		pool-lto6-k6 ) stor_list=(${stor_k6[@]});;
		pool-lto6-k6-1 ) stor_list=(${stor_k6[@]});;
		pool-lto6-k6-2 ) stor_list=(${stor_k6[@]});;
		pool-lto6-cpp ) stor_list=(${stor_cpp[@]});;
		pool-lto6-cpp-1 ) stor_list=(${stor_cpp[@]});;
		pool-lto6-cpp-2 ) stor_list=(${stor_cpp[@]});;
	esac
	
	lbl=0
	for i in ${stor_list[@]}
	do
		((lbl++))
		echo -e "label media storage=${i} drive=${1} pool=${2} barcodes slot=${3} \nyes" > ${tmp_file}
		echo -e "\nPlease wait. Attempting labeling in ${i}\n"
		label_result=$(echo "@input ${tmp_file}"|${bconsole} |awk '/Error/||/3000 OK label/')
		echo "${label_result}"
		label_result=$(echo -e "${label_result}"|awk '{if (/Error/||/3000 OK label/) print $2;}')
		case ${label_result} in
			OK ) echo -e "Labeling status: \t\e[1;32m[DONE]\e[0m"
				break;;
			Error ) continue;;
			*) echo -e "\e[1;31m[ERROR]\e[0m\tUnusual error. Please label the cassette in slot ${4} manually.";;
		esac
		if [ ${lbl} -ge 3 ] ;then
			echo -e "Lebeling status slot ${3} in ${2}:\t\e[1;31m[ERROR]\e[0m"
			echo "⚠️  Позднее в bconsole обязательно выполните:"
			echo "label media storage=${i} drive=${1} pool=${2} barcodes slot=${3}"
			break
		fi
	done
}

main() {
	clear
	pool_menu
	changer_status=$(mtx -f /dev/changer${tape_library} status)
	mailslot_cassettes=($(echo "${changer_status}" |awk '{ sub(/:VolumeTag=/,"" ); if ($4 ~ "IMPORT/EXPORT:Full") print $5}'))
	mslots=($(echo "${changer_status}" |awk '{if ($4=="IMPORT/EXPORT:Full") print $3}'))
	check_cycle
	find_transfer_slot
	num_cassettes
	echo -e "\nUpdatyng slots in Library...\n"
	echo "update slots storage=${storage} drive=${drive}" |${bconsole} > /dev/null
	slot_in_lib=($(echo "SELECT media.slot
					FROM media
					JOIN pool ON pool.poolid = media.poolid
					WHERE pool.name='$pool'
					AND media.slot > 0
					AND media.volstatus = 'Full'
					ORDER BY media.lastwritten
					LIMIT 4;" |${psql}))
	exchange_tape ${num_c}
	label_cycle ${num_c}
}
#######################################################
main "$@"
