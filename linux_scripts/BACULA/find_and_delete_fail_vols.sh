#!/bin/bash

# Задача: находить и удалять "мусор" от незавершившихся заданий. 
#
# В процессе работы задания, по некоторым причинам задание может аварийно завершится (например - 
# не хватает места не диске), после авар.завершения, часть файла остаестя на диске, но практически, 
# (при следующем удачном задании) от этого файла нет пользы. 
#
# Выбрать из бд Bacula информацию (пара jobid-volumename) о всех fail-jobs для определенного задания.
# Передать информацию команде find, найти на диске файл оставшийся после незавершившегося job-а, добавить 
# информацию о нем в журнал, удалить файлы с диска и записи о них из базы.

job_name=${1}
istest=${2}

psql="psql -At -U postgres bareos"
SCRIPT_NAME="${0##*/}"
LOG="/var/log/bareos/${SCRIPT_NAME%.sh}.log"
[ -d ${LOG%/*} ] || mkdir ${LOG%/*}

if [ -f "${PIDFILE}" ]; then
    if ps $(cat ${PIDFILE}) >/dev/null; then
        echo "ERROR: ${SCRIPT_NAME} already running!" >&2; exit 1
    else
        rm -f ${PIDFILE}
    fi
fi

if [ -z "${1}" ]; then
    echo -e "Usage: ${0##*/} {JobName}\nUsage for test: ${0##*/} {JobName} {fake}" >&2; exit 1
fi

#exec >>${LOG} || { echo "No such file or directory ${LOG}" >&2; exit 1; }

echo -e "\nJobname: ${1}"
echo "==========================`date`=========================="

jobs_vols=$(echo "SELECT DISTINCT VolumeName FROM Job, JobMedia, Media
                  WHERE Job.JobId IN (SELECT JobId FROM Job
                                      WHERE Name='$job_name'
                                      AND JobStatus IN ('E','f','D','A')
                                      AND Type='B')
                  AND Job.JobId = JobMedia.JobId
                  AND JobMedia.MediaId = Media.MediaId;" |${psql})

for volume_name in ${jobs_vols}
do
    if [[ "${istest}" = "fake" ]]; then
        for file in $(find -L /mnt/md* -type f -name ${volume_name} -print)
        do
            du -sh ${file}
        done
    else
        find -L /mnt/md* /mnt/drive -type f -name ${volume_name} -print -delete
        echo "delete volume=${volume_name} yes" |bconsole |awk '/not found|This command will delete volume|and all Jobs/'
    fi
done
