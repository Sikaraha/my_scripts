#! /bin/bash

middle_ip="10.24.2.76"

sync_dir_list="
/mnt/backup_mirror_global
/mnt/drive
"

if [ -n "$(ip a|grep ${middle_ip})" ];then
	if [ "`hostname`" = "backup1-1.rian.off" ];then
		slave_srv="backup1-2.rian.off"
	else
		if [ "`hostname`" = "backup1-2.rian.off" ];then
			slave_srv="backup1-1.rian.off"
		else
			echo "ERROR: unknown server name! Exit."
			exit 1
		fi
	fi
else
	echo "Server role: SLAVE"
fi

for s_dir in ${sync_dir_list[@]}
do
	if [ -d ${s_dir} ];then
		rsync -aulv --delete-after ${s_dir} ${slave_srv}:${s_dir}
	else
		echo "ERROR ${s_dir} : source directory not found! Exit."
		exit 1
	fi
done
