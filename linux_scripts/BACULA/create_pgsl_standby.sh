#!/bin/bash

PGDATA=/data/pgsql/10
MASTER_PGDATA=/data/pgsql/10
bindir=/usr/pgsql-10/bin

if [ "$HOSTNAME" = "backup1-1.rian.off" ];
then
	MASTER="backup1-2.rian.off"
else
	MASTER="backup1-1.rian.off"
fi
 
/etc/init.d/postgresql-10 stop

rm -rf ${PGDATA}/* > /dev/null 2>&1
[ -d ${PGDATA} ] || mkdir ${PGDATA}

su postgres

/usr/bin/pg_basebackup -D ${PGDATA} -Xf -P -h ${MASTER} -U postgres 

rm -f rm -rf ${PGDATA}/recovery.done > /dev/null 2>&1

echo "standby_mode = 'on'" > ${PGDATA}/recovery.conf
echo "primary_conninfo = 'host=${MASTER} port=5432 user=postgres'" >> ${PGDATA}/recovery.conf
echo "trigger_file = 'failover'" >> ${PGDATA}/recovery.conf

sed -i 's/#hot_standby = on/hot_standby = on/g' ${PGDATA}/postgresql.conf



/etc/init.d/postgresql-10 start
