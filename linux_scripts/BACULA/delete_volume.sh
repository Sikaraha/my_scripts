#!/bin/bash

bconsole=/usr/sbin/bconsole

info(){
   if [ -z ${1} ]; then
        echo "Enter jobid to erase"
        read -p 'Jobid : ' jobid
   fi

   jobid=$(echo ${jobid} |tr -d ',')
   llist_jobid=$(echo "llist jobid=${jobid}" |${bconsole})
   jobname=$(echo "${llist_jobid}" |awk '/ name:/ {print $2}')
   pool=$(echo "${llist_jobid}" |awk '/poolname:/ {print $2}')
   server_at=$(echo "show storage=${jobname}" |${bconsole} |awk -F'"' '/Address/ {print $2}')

   echo ${server_at} ${jobid} ${jobname} ${pool}
}

check(){
    if [[ ${pool} == "pool-lto6-cpp" ||${pool} == "pool-lto6-cpp-1" ||${pool} == "pool-lto6-cpp-2" ||${pool} == "pool-lto6-k6" ||${pool} == "pool-lto6-k6-1" ||${pool} == "pool-lto6-k6-2" ]]; then
        echo TAPE BACKUP
        exit 0
    fi

    if [[ -z ${jobname} ]]; then
        echo "No such jobid!"
        find_lost_vol
    fi
}

find_lost_vol(){
    echo "Searching lost volume files: "
    find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name *${jobid}* -print -delete |grep 'md' |rev |cut -d'/' -f1 |rev >> /tmp/tmp.list
    ssh root@backup-storage1.rian.off "find -L /mnt/md* /mnt/drive  -maxdepth 2 -name *${jobid}* -print -delete |grep 'md' |rev |cut -d'/' -f1 |rev" >> /tmp/tmp.list
    cat /tmp/tmp.list

    if [[ ! -s /tmp/tmp.list ]]; then
       echo "No lost volumes!"
       exit 0
    fi

    cat /tmp/tmp.list |while read volume_name; do echo "Deleting records ${volume_name} in db... " |echo "delete volume=${volume_name} yes" |${bconsole} >> /dev/null; done
    > /tmp/tmp.list
    exit 0
}

delete_local(){
    echo "Deleting volume files: "
    find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name *${jobname}*${jobid}* -print -delete |grep 'md' |rev |cut -d'/' -f1 |rev >> /tmp/tmp.list
    cat /tmp/tmp.list
    cat /tmp/tmp.list |while read volume_name; do echo "Deleting records ${volume_name} in db... " |echo "delete volume=${volume_name} yes" |${bconsole} >> /dev/null; done
}

delete_ssh(){
    echo $ssh_com
    echo "Deleting volume files: "
    $ssh_com "find -L /mnt/md* /mnt/drive -maxdepth 2 -name *${jobname}*${jobid}* -print -delete |grep 'md' |rev |cut -d'/' -f1 |rev" >> /tmp/tmp.list
    cat /tmp/tmp.list
    cat /tmp/tmp.list |while read volume_name; do echo "Deleting records ${volume_name} in db... " |echo "delete volume=${volume_name} yes" |${bconsole} >> /dev/null; done
}

delete_job(){
    echo "Delete job ${jobid} from db..." 
    echo "delete jobid=${jobid}" |${bconsole} |grep ${jobid}
    > /tmp/tmp.list
}

main(){
    info ${1}
    check

    if [[ ${server_at} = "backup1.rian.off" ]]; then
        delete_local
    elif [[ ${server_at} == "backup-storage1.rian.off" ||${server_at} == "backup-storage1-1.rian.off" ||${server_at} == "backup-storage1-2.rian.off" ]]; then
        ssh_com="ssh root@${server_at}"
        delete_ssh
    else
        echo "Неизвестный storage!"
        exit 1
    fi
    delete_job
}

#######################
main ${1}

unset server_at jobid jobname pool volume_name
