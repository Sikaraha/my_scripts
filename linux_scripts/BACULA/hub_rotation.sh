#!/bin/bash

psql="/opt/postgresql/bin/psql -At -U bacula"
JOBNAME=${1} # %n
JOBLEVEL=${2} # %l
JOBID=${3} # %i
SAVE_FULL=${4} # Сколько хранит Full-ов

SCRIPT_NAME="${0##*/}"
LOG="/var/log/bacula/${SCRIPT_NAME%.sh}.log"

[ -d ${LOG%/*} ] || mkdir ${LOG%/*}

exec >>${LOG} || { echo "No such file or directory ${LOG}" >&2; exit 1; }

JOBDIR="$(awk -F= '/Archive Device/||/ArchiveDevice/ {print $2}' /etc/bacula/bacula-sd/[a-ZA-Z]${JOBNAME:1}_sd.conf || exit 1)"

echo -e "`date +%Y-%m-%d_%k:%M:%S` ${JOBID} Старт очистки ${JOBNAME}"
if [ "${SAVE_FULL}" = "" ]; then
    echo "Для старта очистки нехватает аргумента"
    kill -9 $$
fi

## Ищим актуальные Full
ACACTUAL_FULL_ID=$(echo "SELECT DISTINCT job.jobid
                        FROM job,jobmedia,media
                        WHERE job.name = '$JOBNAME'
                        AND job.jobstatus IN ('T','W')
                        AND job.level = 'F'
                        AND job.jobid = jobmedia.jobid
                        AND jobmedia.mediaid = media.mediaid
                        ORDER BY jobid DESC LIMIT $SAVE_FULL;" |${psql} |xargs |sed -r "s#\ #','#g")

ACTUAL_FULL_DATE=$(echo "SELECT starttime
                        FROM job
                        WHERE jobid IN ('$ACACTUAL_FULL_ID')
                        AND jobstatus IN ('T','W')
                        ORDER BY starttime ASC LIMIT 1;" |${psql})

## Если задание Full, ищем и удаляем все не актуальные job&volume
if [ "${JOBLEVEL}" = "Full" ]; then
    FVTD=$(echo "SELECT DISTINCT VolumeName
                FROM job,jobmedia,media
                WHERE job.name ='$JOBNAME'
                AND job.jobstatus IN ('T','W','f','A','e','E')
                AND job.type = 'B'
                AND job.priorjobid = 0
                AND job.jobid = jobmedia.jobid
                AND jobmedia.mediaid = media.mediaid
                AND job.starttime < '$ACTUAL_FULL_DATE'
                AND job.jobid NOT IN ('$ACACTUAL_FULL_ID');" |${psql})
    ### Удаляет volume-файлы
    for OLDVOL in ${FVTD}
    do
        echo "Volume to delete: ${JOBDIR}/${OLDVOL}"
        echo "delete volume=${OLDVOL} yes" |bconsole
        rm -rfv ${JOBDIR}/${OLDVOL}
    done
fi

## Ищем и удаляем все старые файлы (2678400 -1мес, 8035200 - 3мес, 18408222 - 7мес, 34186698 - 13мес)
echo -e "`date +%Y-%m-%d_%k:%M:%S` ${JOBID} Очищаем мусор в каталоге ${JOBDIR}"

LASTFULLENDTIME=$(echo "SELECT DISTINCT job.starttime
                        FROM job,jobmedia,media
                        WHERE job.name = '$JOBNAME'
                        AND job.jobstatus IN ('T','W')
                        AND job.level = 'F'
                        AND job.jobid = jobmedia.jobid
                        AND jobmedia.mediaid = media.mediaid
                        ORDER BY starttime DESC LIMIT 1;" |${psql})
LASTFULLENDTIMEFD=$(($(date -d "${LASTFULLENDTIME}" +%s) - 34186698))

if [ ${JOBLEVEL} = "Full" ]; then
    find ${JOBDIR} -type f ! -newermt "$(date -d @"${LASTFULLENDTIMEFD}" +%D\ %X)" -print
    #find ${JOBDIR} -type f ! -newermt "$(date -d @"${LASTFULLENDTIMEFD}" +%D\ %X)" -print -exec rm {} \;
fi
