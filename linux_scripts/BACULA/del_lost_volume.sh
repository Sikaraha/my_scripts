#!/bin/bash

LOG_FILE=/var/log/bareos/${0##*/}.log
bconsole="/sbin/bconsole -c /etc/bareos/bconsole.conf"
psql="psql -At -U postgres bareos"
exec > >(tee -a ${LOG_FILE}) || exit 1
small_vol_in_db=$(echo "SELECT DISTINCT volumename
                        FROM media
                        WHERE volbytes < 1024
                        AND volstatus IN ('Used','Full','Recycle','Purged','Archive','Error','Disabled');"|${psql})
i=1
for vol_name in ${small_vol_in_db}
do
    #find /mnt/md* \( -name ".check" -o -name ".wh.*" -o -path "/mnt/md16/backup1" -o  -path "/mnt/md0/trtrt" -o -path "/mnt/md0/from_doc1.rian.off_20181215" -o  -path "/mnt/md1/db/bacula" -o -path "/mnt/md1/working" -o -path "/mnt/md1/oracle" -o -path "/mnt/md1/clever-balancer1" -o -path "/mnt/md2/bacula_bootstrap" -o -path "/mnt/md7/rsync-gluster" -o -path "/mnt/md8/oracle/" -o -path "/mnt/md10/oracle" -o -path "/mnt/md13/oracle" -o -path "/mnt/md15/dermo" -o -path "/mnt/md16/backup1" -o -path "/mnt/md18/oracle" -o -path "/mnt/md17/bacula-db" -o -path "/mnt/md17/bacula-wal" -o -path "/mnt/md19/wal_logs" -o -path "/mnt/md7/old_rhg_filesets" \) -prune -o -type f -name ${vol_name} -print -exec rm -f {} \;
    find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${vol_name} -print -exec rm -f {} \;
    echo "${i} Deleting records ${vol_name} in db..."
    echo "delete volume=${vol_name} yes" |${bconsole} > /dev/null
    ((i++))
done
echo -e "\n============\nУдалено volume - ${i}"