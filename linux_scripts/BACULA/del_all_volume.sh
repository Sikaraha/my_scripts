#!/bin/bash
# Delete all volume from jobname

job_name=${1}

psql="psql -At -U postgres bareos"
SCRIPT_NAME="${0##*/}"
LOG="/var/log/bareos/${SCRIPT_NAME%.sh}.log"
[ -d "${LOG%/*}" ] || mkdir "${LOG%/*}"

if [ -f "${PIDFILE}" ]; then
    if ps $(cat "${PIDFILE}") >/dev/null; then
        echo "ERROR: ${SCRIPT_NAME} already running!" >&2; exit 1
    else
        rm -f "${PIDFILE}"
    fi
fi

if [ -z "${1}" ]; then
    echo -e "Usage: ${0##*/} {JobName}" >&2; exit 1
fi

#exec >>${LOG} || { echo "No such file or directory ${LOG}" >&2; exit 1; }

echo -e "\nJobname: ${job_name}"
echo "========================== $(date)=========================="

jobs_vols=$(echo "SELECT DISTINCT volumename FROM job, jobmedia, media
                WHERE job.jobid IN (SELECT jobid FROM job
                                    WHERE name = '$job_name')
                AND job.jobid = jobmedia.jobid
                AND jobmedia.mediaid = media.mediaid
                AND mediatype NOT IN ('LTO-4','LTO-5','LTO-6');" |${psql})

for volume_name in ${jobs_vols}
do
    find -L /mnt/md* /mnt/drive -type f -name "${volume_name}" -print #-delete
    echo "delete volume=${volume_name} yes" #|bconsole |awk '/not found|This command will delete volume|and all Jobs/'
done
