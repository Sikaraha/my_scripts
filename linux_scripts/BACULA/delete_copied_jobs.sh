#!/bin/bash

psql="psql -At -U postgres bareos"
path_to_drivedir="${1}"
SCRIPT_NAME="${0##*/}"
LOG="/var/log/bareos/${SCRIPT_NAME%.sh}.log"
[ -d ${LOG%/*} ] || mkdir ${LOG%/*}

if [ -f "${PIDFILE}" ]; then
    if ps $(cat ${PIDFILE}) >/dev/null; then
        echo "ERROR: ${SCRIPT_NAME} already running!" >&2; exit 1
    else
        rm -f ${PIDFILE}
    fi
fi

if [ -z "${1}" ]; then
    echo "Usage: ${0##*/} {/path/to/dir}" >&2; exit 1
elif [ -d ${1} ]; then :
else
    echo "No such directory ${1}" >&2; exit 1
fi

exec >>${LOG} || { echo "No such file or directory ${LOG}" >&2; exit 1; }

for file in $(ls ${path_to_drivedir} || { echo "Error listing dir!" >&2; exit 1; })
do
    if [ -f ${path_to_drivedir}/${file} ]; then
        volume_status=$(echo "SELECT volumename, poolid, mediatype FROM media
                                WHERE volstatus IN ('Used','Full','Append')
                                AND volumename = '$file';" |${psql})
        # Проверяем есть ли volume с таким именем в базе, если нет удаляем
        if [[ -z $(echo $volume_status) ]]; then
            echo "`date +%Y-%m-%d%k:%M:%S` The file ${file} is missing from the database"                                             #debug
            find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${file} -print
        else
            # Если в базе есть volume с таким именем проверяем jobid задания на нем
            echo "`date +%Y-%m-%d%k:%M:%S` The record about the volume ${file} is present in the database, we find out the jobid ..." #debug
            job_id=($(echo "SELECT DISTINCT job.jobid
                            FROM job
                            JOIN pool ON pool.poolid = job.poolid
                            JOIN jobmedia ON jobmedia.jobid = job.jobid
                            JOIN media ON media.mediaid = jobmedia.mediaid
                            WHERE media.volumename = '$file'
                            AND job.type = 'B'
                            AND job.jobstatus IN ('T','W','R');" |${psql}))
        
            echo "`date +%Y-%m-%d%k:%M:%S` JobId записанный на ${file}: ${job_id}"                                                    #debug
            [ -z $(echo ${job_id[1]}) ] || echo "SEVERAL JOBS WRITTEN IN ${file}! FILE MISSED!"; continue                             #debug
            [ -n $(echo ${job_id[0]}) ] || echo "There is no job on ${file}!"; continue                                               #debug

            # Проверяем есть ли копия этого задания на ленте
            check_copy=$(echo "SELECT jobid FROM job WHERE type IN ('B','C')
                                AND jobstatus IN ('T','W','R')
                                AND poolid IN ('1308','1312','1313','1369','1370','1371')
                                AND priorjobid = '$job_id';" |${psql})
        
            # Если копия есть удаляем
            if [[ -n ${check_copy} ]]; then
                echo "`date +%Y-%m-%d%k:%M:%S` A copy of the job was received by the job from JobIb ${check_copy}"                        #debug
                find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${file} -print
                echo "delete volume=${file} yes" #| bconsole |awk '/not found|This command will delete volume|and all Jobs/'
            fi
        fi
    else
        echo "${file} it's not a file!"
    fi
done
