#!/bin/bash

#################################################
#
#                 Описание:
#       Структура списка джобов на очистку
#  1. Название 
#  2. Тип бэкапов, которые проверяются (можно смешивать между собой) 
#  3. Временные рамки, в которых джобы не удаляются (можно указать, 
#   бэкап какого типа будет отправной точкой для очиски более старых джобов)
#   
#       Скрипт парсит список и проверяет наличие копий 
#   на лентах. После чего удаляет информацию из базы о
#   всех джобах, старше пареметра 3, а так же физически 
#   стирает их с СХД. В конец лога (/var/log/bareos/delete*)
#   выводится список джобов с количеством бэкапов, которые не удалялись.
#
#
#   Автор: 
#
#################################################


#####################################################################
# $1 - jobname
# $2 - в каких пулах проверять наличие копии: [FDI | All | None]
# $3 - количество дней, в течении которых храним файлы на диске
# $4 - turn on/off fake (debug) mode
#####################################################################

#"exiland-app1 None 93"         DIT-3815
#"doc1 None LastFull"           DIT-2468
#"lessons-rus4allru None LastFull"      DIT-778
#"mfd-db1 None LastFull"                DIT-1139
#"ria-phoneup None LastFull "           DIT-1501
#"service3-f-djin All LastFull "        DIT-1533
#"ria22-storage1 All LastFull"          DIT-1739
#"hosting-kg-wwwmedia All LastFull"     old_job
#"hosting-hk All 31"                    DIT-2798
#"hosting-hk-mongo None 31"             DIT-2798
#"mfd-stats1 F LastFull"                old job
#"portal F LastFull"                    old job
#"rw-rdf1 F LastFull"
#"rw-rdf2 F LastFull"
#"arctic-backend1 F LastFull"
#"cert All LastFull"                    #closed
#"gm-backup None LastFull"              #нет решения о резервном копировании
#"gm-backup-base None LastFull"         #нет решения о резервном копировании
#"gm-backup-cut All 60"                 #нет решения о резервном копировании
#"fabnews-coder1 None LastFull"
#"web-swf2jpg1 All LastFull"        DIT-3327
list_job=(
"ad1 FD LastDiff"
"ad6 none LastDiff"
"auth All LastFull"
"ax2009-aos1-c All LastFull"
"ax2009mia-aos1-app All LastFull"
"ax2009mia-sql None 14"
"ax2009-svc1-c All LastFull"
"ax2009-svc1-data All LastFull"
"ax2009-zgp None 365"
"axa14-app1 All LastFull"
"axa-bgl-rebuild All LastFull"
"axa-sql F LastFull"
"axa-sql-e F LastFull"
"back1-site F LastFull"
"back3-site F 14"
"back4-site F LastFull"
"back5-site F LastFull"
"back7-site F LastFull"
"back9-site F LastFull"
"bacula-db None LastFull"
"backup1 None 200"
"bes-sql1 None 14"
"bitrix3-rian-off F LastFull"
"bolid-app1-c-base F LastFull"
"common All LastFull"
"crm2011-app1-dv-c F LastFull"
"crm-dp-app1 F LastFull"
"crm-dp-db1 F LastFull"
"customer-ad1 None LastFull"
"devel-site F 14"
"dhcp1 None LastFull"
"djin-dbisrv1 F LastFull "
"doc2 None LastFull"
"docker-registry1 All LastFull"
"fabnews-coder2 None LastFull"
"fabnews-fms1 None LastFull"
"fabnews-fms2 None LastFull"
"fabnews-ivx1 None LastFull "
"fabnews-ivx2 None LastFull"
"fabnews-nfw1 None LastFull"
"fabnews-nfw2 None LastFull"
"fabnews-store1 None LastFull"
"fabnews-store2 None LastFull"
"fabnews-store2-d-out All LastFull"
"fontexpx-app1 None LastFull "
"git-dmp All LastFull"
"git-drpo All LastFull"
"git-libs-data F LastFull"
"gitlab None LastDiff"
"hosting-ee All 31"
"hosting-ee-mongo None 31"
"hosting-cn All 31"
"hosting-cn-mongo None 31"
"hosting-cn-media All 31"
"hosting-tp All 31"
"hosting-tp-mongo None 31"
"hosting-tp-media All 31"
"hosting-kg All 21"
"hosting-kg-mongo None 31"
"hosting-kg-media All 31"
"hosting-kz All 31"
"hosting-kz-mongo None 31"
"hosting-kz-media All 31"
"infochannel None LastFull"
"jira2018 None LastFull"
"kb-dmz-off F LastFull"
"kis-jabber All LastFull"
"lync-front1 F LastFull"
"m1 none 1"
"m12 F LastFull"
"m13 F LastFull"
"mail-klms1.rian.off None 365"
"mail-klms2.rian.off None 365"
"mfeed-rian-frontend1 None LastFull"
"mgr-1 F LastFull "
"mgr-2 F LastFull"
"mscl3-node3-c F LastFull"
"mscl3-node4-c F LastFull"
"ms-cluster1-crm F LastFull"
"ms-cluster1-fab F LastFull"
"ms-cluster1-lnc F LastFull"
"ms-cluster2-fs1-Departments All LastFull"
"ms-cluster2-fs2-distr F LastFull"
"ms-cluster-2-fs3-PublicFolders1 All LastFull"
"ms-cluster-2-fs4-PublicFolders2 All LastFull"
"ms-cluster2-fs5-pst All LastFull"
"ms-cluster2-fs5.msk.rian All LastFull"
"ms-cluster2-fs7-Departments All LastFull"
"ms-cluster2-fs8-l All LastFull"
"ms-cluster6-fs1-E F LastFull"
"ms-cluster6-fs2-djin_news none 570"
"ms-cluster6-fs2-djin_program none 570"
"ms-cluster6-fs3-G none LastDiff"
"ms-cluster6-fs3-G-DMC-Video none 200"
"mssql-cluster1 F 14"
"mssql-omni-uat-db1 All 1"
"mterm-web1 F LastFull"
"nas-swz1-PhotoFTP None LastFull"
"netss-1 F LastFull"
"netss-2 F LastFull"
"omni-uat-app1 All LastFull"
"phoneup1-c None LastFull"
"phoneup1-d F LastFull"
"pm F LastFull"
"pr13-app1-c F LastFull"
"pr13-app1-data All 30"
"pr13-app2 F LastFull"
"pr13-app3 F LastFull"
"pr1.rian.off F 120"
"pr2.rian.off F 120"
"pr3.rian.off F 120"
"ps1-msk-rian F LastFull"
"ps2 F LastFull"
"ps2-mssql None 15 "
"Radio-Production All LastFull"
"rdcb1 F LastFull"
"rdcb2 F LastFull"
"rhg-storage1 All LastFull"
"rhg-storage2 All LastFull"
"rhg-storage3 All LastFull"
"rhg-storage4 All LastFull"
"rhg-storage5 All LastFull"
"rhg-storage6 All LastFull"
"ria22-mongo-data0 F 14"
"ria22-storage1 All 30"
"rian21-rhs-media All LastFull"
"riasweeper None 180"
"rsauth All 180"
"rsnews-app1 All LastFull"
"rsnews-app2 All LastFull"
"rsnews-app3 All LastFull"
"rsnews-app4 All LastFull"
"s2d-fs1-djin All LastFull"
"ScanArchive_2017 All LastFull"
"sed-app1-c F LastFull"
"sed-app1-e F LastFull"
"sed-app2-c F LastFull"
"sed-db1 F LastFull "
"sed-uat-app1 None LastFull"
"sed-uat-app1-d None 30"
"sed-uat-app2 None LastFull"
"sed-uat-db None LastFull"
"sepm-app2 None 30"
"sep-new-web F LastFull"
"sep-new-web2 F LastFull"
"sep-sphinx1 F LastFull"
"sep-sphinx2 F LastFull"
"service13-CMOP-video-archive F LastFull "
"service3-d-ria All LastFull"
"service6-ARCHIVE None 10"
"service-cons F LastFull "
"service-cons-data None LastFull"
"service-dbo All LastFull"
"service-dbo2 F LastFull"
"service-dzido1s F LastFull"
"service-multitran F LastFull"
"service-wds1-F none LastFull"
"smrv-zabbix All 120"
"sp13-app1 F LastFull"
"sp13-app2 F LastFull"
"sp13-db1 F LastFull"
"sp13-wac1 F LastFull"
"sp13-wac2 F LastFull"
"sp13-web1 F LastFull"
"sp13-web2 F LastFull"
"src F LastFull"
"src-rhel None LastFull"
"stor All LastFull"
"storio-0 All LastFull"
"storwize-wd All LastFull"
"storwize1-CMOP All LastFull"
"storwize1-pubfolders All LastFull"
"storwize2-Archive None LastFull"
"storwize2-Aurora none 210"
"storwize2-authors All LastFull"
"storwize2-Correspondent None LastFull"
"storwize2-exchange None LastFull"
"storwize2-Online None LastFull"
"storwize2-Photo_Original_Arch All LastFull"
"storwize2-RIAN-Video None LastFull"
"storwize2-Scala All LastFull"
"storwize2-pubfolders All LastFull"
"svn None LastFull"
"tableau-service1 F LastFull"
"tickers F 217"
"vault None 62"
"vid1 None LastFull"
"visitorctrl-d F 14"
"web-backend-rian22-devel1 None LastFull"
"web-backend-rian22-devel2 None LastFull"
"web-devel2 None LastFull"
"web-devel3 None LastFull"
"web-devel5 None LastFull"
"web-inosmi2 None 31"
"web-multiscreen1-rian-off None LastFull"
"web-search1 F 14"
"web-search2 F 14"
"webfarmz-mongo-data0 F 14"
"wsus1-db F LastFull"
)

LOG_FILE=/var/log/bareos/${0##*/}.log

bconsole="/usr/sbin/bconsole"
psql="psql -At -U postgres bareos"

declare -a uncopied_jobs many_stored_copies

if [[ ${4} = fake ]];then 
  f_mode="fake" 
else 
  f_mode="normal" 
fi 

exec > >(tee -a ${LOG_FILE}) || exit 1

f_delete_old_files() {
    local jobname=${1}
    echo -e "\n=========================================================="
    echo "-> Job: ${1}"

    #Размер директории до очистки
    #before_size=$((`du -sm /mnt/backup_mirror_global/${jobname}/ |awk '{print $1}'`))  
    #echo "Before size: `du -sh /mnt/backup_mirror_global/${jobname}/ | awk '{print $1}'`"

    #Какие пулы необходимо проверить на наличие копии
    case ${2} in
        F | f) find_copy_on_pool="'${jobname}-quarter','${jobname}-Full'";;
        D | d) find_copy_on_pool="'${jobname}-monthly','${jobname}-Diff','${jobname}-Differential'";;
        I | i) find_copy_on_pool="'${jobname}-daily','${jobname}-Incr','${jobname}-Incremental'";;
        FD) find_copy_on_pool="'${jobname}-quarter','${jobname}-Full','${jobname}-monthly','${jobname}-Diff','${jobname}-Differential'";;
        DI) find_copy_on_pool="'${jobname}-monthly','${jobname}-Diff','${jobname}-Differential','${jobname}-daily','${jobname}-Incr','${jobname}-Incremental'";;
        FI) find_copy_on_pool="'${jobname}-quarter','${jobname}-Full','${jobname}-daily','${jobname}-Incr','${jobname}-Incremental'";;
        FDI) find_copy_on_pool="'backup-storage_${jobname}','${jobname}-quarter','${jobname}-Full','${jobname}','${jobname}-daily','${jobname}-Incr','${jobname}-Incremental','${jobname}-monthly','${jobname}-Diff','${jobname}-Differential'";;
        All | all | ALL) find_copy_on_pool="'backup-storage_${jobname}','${jobname}-quarter','${jobname}-Full','${jobname}','${jobname}-daily','${jobname}-Incr','${jobname}-Incremental','${jobname}-monthly','${jobname}-Diff','${jobname}-Differential'";;
        jobname) find_copy_on_pool="'backup-storage_${jobname}','${jobname}'";;
        pool=[A-Za-z0-9]*) find_copy_on_pool="'"${2/pool=/""}"'";;
        None | none | NONE) find_copy_on_pool="";;
        *) echo "Usage delete.sh <jobname> <FDI|All|None> <days(0-9)|LastFull> <fake>"
        return 1
        ;;
    esac
  
    #Не удалять, если в pool'е есть задания, без копии на ленте.
    if [ -n "${find_copy_on_pool}" ]; then
        iscopy=$(echo -e "SELECT DISTINCT job.jobid,job.starttime,media.volumename
                        FROM job
                        JOIN pool ON pool.poolid = job.poolid
                        JOIN jobmedia ON jobmedia.jobid = job.jobid
                        JOIN media ON media.mediaid = jobmedia.mediaid
                        WHERE pool.name IN ($find_copy_on_pool)
                        AND job.type='B'
                        AND job.jobstatus IN ('T','W')
                        AND Job.jobBytes > 0
                        AND job.jobid NOT IN (SELECT priorjobid FROM job WHERE type IN ('B','C') AND job.jobstatus IN ('T','W') AND priorjobid !=0)
                        ORDER BY job.starttime;" | ${psql})
        if [ -n "${iscopy}" ]; then
            echo "⚠️  Pool(s) contain uncopied job(s):"
            uncopied_jobs=("${uncopied_jobs[@]}" "${jobname}")
            echo "${iscopy}"
            echo "Exit."
            return 1
        fi
    fi

    #Дата очистки по умолчанию
    del_date="1991-12-25 19:38:00"
    #Определить период хранения бэкапов на диске
    #Передавать можно 2 значения: 
    # 1) к-во хранимых на диске последних удачных копий (LastCopies)
    # 2) все копии, младше последнего Full-а (LastFull)
    #Переменная $3 [ LastCopies(positive integer) | LastFull ]
    case $3 in
        [0-9]  ) del_date=$(date -d"$3 day ago" "+%Y-%m-%d %H:%M:%S");;
        [0-9][0-9]  ) del_date=$(date -d"$3 day ago" "+%Y-%m-%d %H:%M:%S");;
        [0-9][0-9][0-9] ) del_date=$(date -d"$3 day ago" "+%Y-%m-%d %H:%M:%S");;
        LastDiff) last_d_date=$(echo -e "SELECT max(starttime) AS lastdiff
                                        FROM job
                                        WHERE name='$jobname'
                                        AND level='D'
                                        AND jobstatus IN ('T','W')
                                        AND type='B';" |${psql}) #получим дату последнего Diff-а
                    if [ -z "${last_d_date}" ]; then
                        echo "No assignment!"
                        return 1
                    fi
                    del_date=$(echo -e "SELECT starttime
                                        FROM job
                                        WHERE name='$jobname'
                                        AND level IN ('D','I')
                                        AND jobstatus IN ('T','W')
                                        AND type='B'
                                        AND starttime < '$last_d_date'
                                        ORDER BY starttime DESC
                                        LIMIT 1;" |${psql})
                    if [ -z "${del_date}" ]; then
                        echo "No assignment!"
                        return 1
                    fi
                    echo "-> Last diff: ${del_date}";;
        LastFull) del_date=$(echo -e "SELECT max(starttime) AS maxfull
                                        FROM job
                                        WHERE name='$jobname'
                                        AND level='F'
                                        AND jobstatus IN ('T','W')
                                        AND type='B';" |${psql}) # 2) получим дату последнего Full-а
                if [ -z "${del_date}" ]; then
                    echo "No assignment!"
                    return 1
                fi
                count_jobs_after_full=$(echo -e "SELECT count(jobid) AS cjobid
                                                FROM job
                                                WHERE name='$jobname'
                                                AND starttime > '$del_date'
                                                AND jobstatus IN ('T','W')
                                                AND type='B';" |${psql}) # к-во удачных бэкапов, после последнего Full'а
                echo "-> Last full: ${del_date}, saved jobs: ${count_jobs_after_full}"
                if [ "$count_jobs_after_full" -lt 10 ]; then
                    echo "⚠️  The number of stored copies (${count_jobs_after_full}) less then 10"
                    return 1
                fi
                if [ "$count_jobs_after_full" -gt 13 ]; then
                    many_stored_copies=("${many_stored_copies[@]}" "${count_jobs_after_full} ${jobname}")
                fi;;
        *) echo "Usage delete.sh <jobname> <FDI|All|None> <days|LastFull>"
        return 1
        ;;
    esac

    if [ -z "${last_d_date}" ]; then
        jobs_to_del=$(echo "SELECT DISTINCT job.jobid,media.volumename
                            FROM job
                            JOIN jobmedia ON job.jobid=jobmedia.jobid
                            JOIN media ON jobmedia.mediaid=media.mediaid
                            WHERE job.name = '$jobname'
                            AND job.starttime < '$del_date'
                            AND job.type ='B'
                            AND job.priorjobid = 0
                            AND media.poolid NOT IN ('1308','1312','1313','1369','1370','1371')
                            ORDER BY jobid;" |${psql})
    else
        jobs_to_del=$(echo "SELECT DISTINCT job.jobid,media.volumename
                            FROM job
                            JOIN jobmedia ON job.jobid=jobmedia.jobid
                            JOIN media ON jobmedia.mediaid=media.mediaid
                            WHERE job.name = '$jobname'
                            AND job.starttime < '$del_date'
                            AND job.type ='B'
                            AND job.level IN ('D','I')
                            AND job.priorjobid = 0
                            AND media.poolid NOT IN ('1308','1312','1313','1369','1370','1371')
                            ORDER BY jobid;" |${psql})
    fi

    for i in ${jobs_to_del}
    do
        echo ${i}
        volumename=$(echo ${i} | awk -F"|" '{print $2}')
        jobid=$(echo ${i} | awk -F"|" '{print $1}')
        server_at=$(cat /etc/bareos/bareos-dir.d/job/${jobname}.conf | grep -A4 'Storage {'| awk '/Add/ {print $3}')
        #if [ -n $(echo -n "${volumename}"|grep L6) ]; then
        #    break
        #fi
        if [ "$f_mode" = fake ]; then
            echo "Delete volume file: "
            find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${volumename} -print
            echo "Deleting records in db.. "
        else
            echo "Delete volume file: "
            if [[ "${server_at}" = "backup1.rian.off" ]]; then
                find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${volumename} -print -delete
            else
                ssh root@${server_at} "find -L /mnt/md* /mnt/drive /mnt/backup_mirror_global -maxdepth 2 -name ${volumename} -print -delete" 
            fi
            
            echo "Deleting records ${volumename} in db... "
            echo "delete volume=${volumename} yes" |${bconsole} > /dev/null
        fi
    done

    #Выводим в лог вес каталога после очистки
    #after_size=$((`du -sm /mnt/backup_mirror_global/${jobname}/ |awk '{print $1}'`))
    #echo "After total size for ${jobname}: `du -sh /mnt/backup_mirror_global/${jobname}/|awk '{print $1}'`"
    #echo "Cleaned $((${before_size}-${after_size}))MB "
}

#####################################################################
echo -e "\n\n\n==========================`date`=========================="
echo "-> Mode: ${f_mode}"

if [ "${1}" != '' ]; then
    if [ "$f_mode" = fake ]; then
        f_delete_old_files "$@"
    else
        echo "For debug mode usage: ${0##*/} {jobname|copy pool|day before cleaning|fake}" 
    fi
else
    ind=0
    while [ ${ind} -lt ${#list_job[@]} ]
    do
        f_delete_old_files ${list_job[${ind}]}
        ((ind++))
    done
fi

echo -e "\n\n\n========================== Uncopied jobs =========================="
n_uj=0
while [ ${n_uj} -le ${#uncopied_jobs[@]} ]
do
    echo "${uncopied_jobs[${n_uj}]}"
    ((n_uj++))
done

echo -e "\n========================== Many saved copies =========================="
n_msc=0
while [ ${n_msc} -le ${#many_stored_copies[@]} ]
do
    echo "${many_stored_copies[${n_msc}]}"
    ((n_msc++))
done
