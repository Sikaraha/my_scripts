#!/bin/bash
action=$1
ip_addr=$2
force=$3


NORMAL='\033[0m'
GREEN='\033[0;92m'
export PATH=/bin:/sbin:/usr/bin:/usr/sbin

#validate ip
if  [[ -n ${ip_addr} ]]; then
    if [[ $ip_addr =~ ^195\.93\.246\.[0-9]+$ ]] || [[ $ip_addr =~ ^195\.93\.247\.[0-9]+$ ]] || [[ $ip_addr =~ ^194\.36\.17\.[0-9]+$ ]]; then
        true
    else
        echo -ne "${GREEN}ошибка, кривой ip адрес${NORMAL}\n"
        exit 1
    fi
fi

check_white_list() {
    if [[ $force == "force" ]]; then
        zebra_add_ip || exit 1
    else
        if cat /opt/scripts/ip-whitelist|grep -q ${ip_addr}; then
            echo -ne "${GREEN}${ip_addr} находится в белом списке, для принудительной блокировки добавьте force${NORMAL}\n"; exit 0
        else
            zebra_add_ip || exit 1
        fi
    fi 
}

zebra_add_ip() {
    if ! ip ro|grep -q ${ip_addr}; then
        echo -ne "conf t\nip route ${ip_addr}/32 Null0\nend\n"|vtysh >/dev/null || exit 1
        echo -ne "${GREEN}$ip_addr успешно заблокирован${NORMAL}\n" 
    else
        echo -ne "${GREEN}$ip_addr и так уже заблокирован${NORMAL}\n"
    fi
}

zebra_del_ip() {
    if ip ro|grep -q ${ip_addr}; then 
        echo -ne "conf t\nno ip route ${ip_addr}/32 Null0\nend\n"|vtysh 1>/dev/null || exit 1
        echo -ne "${GREEN}$ip_addr удален из блокировки${NORMAL}\n"
    else
        echo -ne "${GREEN}$ip_addr и так не заблокирован${NORMAL}\n"
    fi
}

zebra_list_ip() {
    if  [[ -n ${ip_addr} ]]; then
        echo -ne "${GREEN}${ip_addr} "
        if cat /opt/scripts/ip-whitelist|grep -q $ip_addr; then
            echo -ne "находится в белом списке "
        else
            echo -ne "не в белом списке, "
        fi

        if ip route|grep blackhole|grep -q "blackhole $ip_addr"; then 
            echo -ne "и заблокирован${NORMAL}\n"
        else
            echo -ne "и не в блокировке${NORMAL}\n"
        fi
    else
        echo -ne "${GREEN}Список ip добавленых в blackhole routing:${NORMAL}\n"
        (ip route|grep blackhole || echo "1 Нет_заблокированных_адресов") |awk '{print $2}'|sort
        echo ""
        diff <(ip route |grep blackhole |awk {'print $2'}|sort) <(vtysh -c "show ip bgp" |grep '*> '|awk '{print $2}'| cut -d  / -f 1|sort)
        if [[ $? -ne 0 ]] ;then
            echo -ne "\n${GREEN}Внимание, ошибка!. Не соответсвие локальной таблицы маршрутизации с bgp, обратитесь к администратору${NORMAL}\n\n"
        fi
    fi
}

case ${action} in
    add)
        clear
        check_white_list || exit 1
        ;;
    del)
        clear
        zebra_del_ip || exit 1
        ;;
    list)
        clear
        zebra_list_ip || exit 1
        ;;
       *)
        echo -en "syntax $0 <add|del> <ip> |force or $0 <list> <ip>\n"; exit 1
        ;;
esac
