#!/bin/bash
#echo ${1:?"Usage: `basename $0` volume_name dstdir(optional)"}

GPATH="/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/bareos/bin"
PIDFILE="/var/run/gluster-rsync-${1}.pid"
DATE=$(date +%Y%m%d%-H%M%S)
VOLUME=${1}
DSTDIR=${2}
SRCDIR="/mnt/rhs/rhg_${VOLUME}"
GLUSTERNODE=rhg-storage1.rian.off
STATEFILE="/var/run/gluster-rsync-${1}.state"
SESSION_NAME="rsync-backup"
FILELIST="/home/bareos/rsync-backup.${VOLUME}.${DATE}"
ERRDESC="Gluster rsync failed."
DESTFILE="/mnt/md7/rsync-gluster/rsync-gluster-${VOLUME}-${DATE}.filelist"
LOG_FILE="/mnt/md7/rsync-gluster/rsync-gluster-${VOLUME}-${DATE}.info.log"

checks_funk() {
    #Проверяем PID
    if [ -f "${PIDFILE}" ]; then
        if ps -p $(cat ${PIDFILE}) >/dev/null; then
            echo "Error: `basename $0` already running!"
            exit 1
        else
            rm -f ${PIDFILE}
            #отправляем письмо с ошибкой
            echo "$HOSTNAME: Error: the \$pidfile already exist!." |mail -s "$HOSTNAME: pidfile already exist" root-linux@rian.ru
        fi
    fi
    
    #Проверяем точку монтирования
    if ! mountpoint -q ${SRCDIR}; then
        echo "error, $SRCDIR - media from gluster storage not mounted, exiting"
        exit 1
    fi

    # Проверяем доступность узлов кластера
    for ((i=1; i < 7; i++))
    do
        ping rhg-storage${i}.rian.off -c1 -q -W1 &>/dev/null
        [ $? -gt 0 ] && { echo "Error: One or more nodes are unavailable!"; alert_func; exit 1; }
        echo "rhg-storage${i}.rian.off node available."
    done

    # не запускать при наличии STATEFILE - говорит о том, что glusterfind pre был запущен,
    # но не завершен, т.е. не выгружен список файлов; необходимо проверять наличие процессов glusterfind на нодах,
    # и прибивать их в случае необходимости
    if [ -f ${STATEFILE} ]; then
        ERR_MSG="Glusterfind стартовал но не был завершен. Убейте процессы glusterfind на всех узлах кластера, и удалите ${STATEFILE}, и запустите задание заново."
        alert_func
    fi

    #Создаем check фаил на томе gluster
    touch ${SRCDIR}/.check || exit 1
    #Ждем появления нового файла в списке изменений на серверах кластера.
    sleep 60
    
    if [[ ${DSTDIR} ]]; then
    	DESTPATH="${DSTDIR}/rhg_${VOLUME}"
        if ! mountpoint -q ${DESTPATH}; then
            echo "error, ${DESTPATH} - storwize not mount, exiting"
            exit 1
        fi
    else
    	DESTPATH="/mnt/storwize/rh-storage_backup/rhg_${VOLUME}"
        if ! mountpoint -q /mnt/storwize/rh-storage_backup; then
            echo "error, /mnt/storwize/rh-storage_backup - storwize not mount, exiting"
            exit 1
        fi
    fi
}

alert_func () {
    local SENDER="bacula@backup1.rian.off"
    local RECIEVERS='"root-linux@rian.ru"'
    local SUBJECT="${ERRDESC}"
    local BODY="By some reason current rhg_storage backup failed. Details follow:
VOL: '${VOLUME}'
JOBLEVEL: 'Incremental'
Execution time: '${DATE}'
Error message: '${ERR_MSG}'

COMMAND EXECUTED: 'sudo glusterfind pre ${SESSION_NAME} ${VOLUME} ${FILELIST} --no-encode${GFIND_OPTS}"
    printf "From: <%s>\nTo: <%s>\nSubject: %s\n\n%s" "${SENDER}" "${RECIEVERS}" "${SUBJECT}" "${BODY}" | /usr/sbin/sendmail.postfix -f ${SENDER} -t ${RECIEVERS}
    exit 1
}

rsh_glusterfind() {
    echo "$(date) glusterfind started "$1 >>${LOG_FILE}
    # если запустить glusterfind, и не дождавшись завершения прервать - файл status.pre не будет создан, но процесс glusterfind останется выполняться
    # на всех нодах, и при последующем запуске запустится ещё несколько экземпляров glusterfind
    touch ${STATEFILE}
    ssh bacula@${GLUSTERNODE} "export PATH=${GPATH}; sudo glusterfind pre ${SESSION_NAME} ${VOLUME} ${FILELIST} --no-encode${GFIND_OPTS}" >>${LOG_FILE} 2>&1
    if [[ $? -eq 0 ]]; then
        # файл выгружен, можно удалить STATEFILE, т.к. создан файл status.pre
        rm -f ${STATEFILE} 2>>${LOG_FILE}
        scp bacula@${GLUSTERNODE}:${FILELIST} ${DESTFILE}".tmp" >>${LOG_FILE} 2>&1
    else
        ERR_MSG="remote command execution failed (glusterfind), ${FILELIST} not backuped"
        alert_func
    fi

    if [[ "$(tail -n10 ${DESTFILE}".tmp" | wc -l)" -eq 0 ]]; then
        ERRDESC="Gluster rsync, filelist is empty"
		ERR_MSG="Gluster rsync, filelist is empty for $VOLUME, gluster post did not exectuted!"
		echo "Gluster rsync, filelist is empty for $VOLUME" >>${LOG_FILE}
		alert_func
    fi
    echo "$(date) glusterfind exited(sucess)" >>${LOG_FILE}
}

gluster_post() {
    ssh bacula@${GLUSTERNODE} "export PATH=${GPATH}; sudo glusterfind post ${SESSION_NAME} ${VOLUME}" >>${LOG_FILE} 2>&1 || exit 1
    local GFIND_STATE=$(ssh bacula@${GLUSTERNODE} "[ ! -f /var/lib/glusterd/glusterfind/${SESSION_NAME}/${VOLUME}/status.pre ] && cat /var/lib/glusterd/glusterfind/${SESSION_NAME}/${VOLUME}/status" 2>/dev/null) || GFIND_STATE="1"

    if [ "${GFIND_STATE}" = "1" ]; then
        ERRDESC="Gluster rsync post command failed"
        ERR_MSG="Post command failed to run after pre"
        alert_func
    fi
}

main() {
    local RLOG_FILE="/mnt/md7/rsync-gluster/rsync-gluster-${VOLUME}-${DATE}.sync.log"
    local GFIND_STATE=$(ssh bacula@${GLUSTERNODE} "[ -f /var/lib/glusterd/glusterfind/${SESSION_NAME}/${VOLUME}/status.pre ] && echo 1 || echo 0") 
    
    checks_funk
    
    echo $$ >"${PIDFILE}" || { echo "Error: couldn't create the \$pidfile!"; exit 1; }
    trap "rm ${PIDFILE}" 0 1 3 15
    
    # В конце работы команды Pre, создается фаил $SESSION_DIR/status.pre 
    # Псоле вызова Post, $SESSION_DIR/status.pre будет переименован в $SESSION_DIR/status
    # Иначе ошибка: Post command is not run after last pre, use --regenerate-outfile
    if [ "${GFIND_STATE}" = "1" ]; then
        GFIND_OPTS=" --regenerate-outfile"
    fi

    rsh_glusterfind

    >${DESTFILE}
    egrep -v '/$|^DELETE' ${DESTFILE}".tmp" |awk '$1 ~ /RENAME/ {print $3} $1 ~ /NEW/ || $1 ~ /MODIFY/ {print $2}' >>${DESTFILE}
    
    # Копирование файлов на том горячего резерва по листингу glusterfind
    [ -s ${DESTFILE} ] && rsync -a --files-from=${DESTFILE} --log-file=${RLOG_FILE} --exclude=*/captcha/images/* ${SRCDIR}/ ${DESTPATH}/ >>${LOG_FILE} 2>&1
    
    gluster_post
    
    #Архивация свежих и очистка устаревших логов glusterfind
    find /mnt/md7/rsync-gluster/ -maxdepth 1 -type f -not -name "*.xz" -mtime +2 -exec xz {} \;
    find /mnt/md7/rsync-gluster/ -maxdepth 1 -type f -name "*.xz" -mtime +7 -exec rm -f {} \;
}
