#!/bin/bash

SCRIPT_NAME="${0##*/}"
LOG="/var/log/${SCRIPT_NAME%.sh}.log"
PIDFILE=/var/run/${SCRIPT_NAME%.sh}.pid

exec >>${LOG} || { echo "No such file or directory ${LOG}" >&2; exit 1; }

if [ -f "${PIDFILE}" ]; then
    if ps $(cat ${PIDFILE}) >/dev/null; then
        echo "ERROR: ${SCRIPT_NAME} already running!"
        exit 1
    else
        rm -f ${PIDFILE}
    fi
fi

REPOLIST="
base
extras
updates
fasttrack
centosplus
epel
centos-gluster7
"

REPO_DIR="/data/repository/repo/"

for i in ${REPOLIST}
do
    echo " -=-=-=-=-=-=- ${i} reposync: -=-=-=-=-=-=-"
    /usr/bin/reposync --gpgcheck -l --repoid=${i} --download_path=${REPO_DIR}
    echo " -=-=-=-=-=-=- ${i} createrepo current: -=-=-=-=-=-=-"
    /usr/bin/createrepo ${REPO_DIR}${i}
    echo " -=-=-=-=-=-=- ${i} end -=-=-=-=-=-=-"
done

# Копирование новых пакетов tzdata в спец репозиторий и его обновление
echo -e "\n\n -=-=-=-=-=-=- copy tzdata packages -=-=-=-=-=-=-"

/usr/bin/cp -Rpvn /data/repository/updates.current/Packages/tzdata* /data/repository/mia-centos7.current/Packages/t/
/usr/bin/createrepo --update /data/repository/mia-centos7

exit 0

