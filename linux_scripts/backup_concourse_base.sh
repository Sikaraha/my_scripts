#!/bin/bash

SCRIPT_NAME="${0##*/}"
LOG_FILE="/var/log/${SCRIPT_NAME%.sh}.log"
PIDFILE=/var/run/${SCRIPT_NAME%.sh}.pid
exec >>${LOG_FILE} 2>&1 || { echo "No such file or directory ${LOG_FILE}" >&2; exit 1; }

echo -e "\n`date`"

if [ -f "${PIDFILE}" ]; then
    if ps $(cat ${PIDFILE}) >/dev/null; then
        echo "Ошибка: ${SCRIPT_NAME} уже запущен!"
        exit 1
    else
        rm -f ${PIDFILE}
    fi
fi

search_cont=$(docker ps -f status=running -f publish=5432 --format '{{.Names}}')

if [ $(echo "${search_cont}" |wc -l) -eq 1 ]; then
        cont_name=${search_cont}
else
        echo "Ошибка имени контейнера! Пожалуйста проверьте запущенные контейнеры!"
fi

exec_cont="docker exec -u postgres ${cont_name}"

${exec_cont} bash -c "pg_dump -U concourse_user concourse |gzip > /backup/postgres_backup_`date +%Y%m%d_%H_%M`.tar.gz" && \
if [ $(ls -1 /opt/concourse/backup/*.tar.gz |wc -l) -ge 168 ]; then
        echo "Резервное копирование выполнено!"
        ${exec_cont} bash -c "find /backup -maxdepth 1 -type f -name '*.tar.gz' -mtime +7 -print -exec rm -f {} \;"
else
        echo "В каталоге с бекапами архивы менее чем за 7 суток!"
fi

exit 0
