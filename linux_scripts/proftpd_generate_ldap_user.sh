#!/bin/bash
#
# Generating files 
#
# Get group context
# ldapsearch -x -H ldap://ad0.rian.off -b 'dc=msk,dc=rian' -D 'cn=SA_LDAP-Reader,ou=Test&ServiceUsers,dc=msk,dc=rian' -w 'SaL123456' "(&(objectClass=group)(cn=ARCTIC-FTP*))" cn 
#
# Name of project
project="pano"
# Group for find in LDAP
project_ldap_group="PANO-FTP"
# IP
ip="10.24.2.22"
# Config file folder
proftpd_config_dir="/etc/proftpd.d"
# File virtual config (permanent)
proftpd_config_virtualhost="$proftpd_config_dir/proftpd.conf.$project"
# File for all "Anonymous" context
proftpd_config_file_users="$proftpd_config_dir/proftpd.conf.$project.users"
proftpd_config_file_perms_group="$proftpd_config_dir/$project.perms.group"
# Path for "Anonymous" context  
path_of_root_ftp="/srv/ftp/$project"

#ldap configuration
ldap_server="ad0.rian.off"
ldap_bind_user="cn=SA_LDAP-Reader,ou=Test&ServiceUsers,dc=msk,dc=rian"
ldap_bind_password="SaL123456"
ldap_users_location="dc=msk,dc=rian"
ldap_groups_location="OU=$project_ldap_group,OU=FTP,OU=Test&ServiceUsers,DC=msk,DC=rian"


pidfile=/var/run/${0##*/}.pid
PATH=$PATH:/opt/scripts
RET=0

generate_proftpd_conf() {
    # Checking more then 1 MM group per user 
    check_dup=$(ldapsearch -x -H "ldap://$ldap_server" -b "$ldap_users_location" -D "$ldap_bind_user" -w "$ldap_bind_password" "(&(|$@)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))" sAMAccountName memberOf |ldap_translate |grep -e "^dn:" -e "${project_ldap_group}_" |awk '{if(/^dn:/){if(n>1)print a;n=0;a=$0;};if(/^memberOf:/){n++;a=a"\n"$0}}')
    [ "$check_dup" ] && echo "$check_dup" |mail -s "${project_ldap_group}: duplicate groups" root irina@rian.ru

    # Creating proftpd virtual config file
    cat >"${proftpd_config_virtualhost}.tmp" <<EOF
# VirtualHost ${project}

<VirtualHost $ip>
  ServerName                      "${project_ldap_group}"
  ServerIdent                     on "ProFTPD Server"
  Port                            21
#  MaxClients                      20
  MaxClientsPerHost               10
  AuthOrder                       mod_ldap.c
  LDAPServer                      ${ldap_server}
  LDAPBindDN                      ${ldap_bind_user} ${ldap_bind_password}
  LDAPUsers                       "${ldap_users_location}" "(&(sAMAccountName=%u)(|$@)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))"
  LDAPAttr                        uid sAMAccountName
  LDAPGenerateHomedir             on
  LDAPGenerateHomedirPrefix       ${path_of_root_ftp}
  LDAPForceGeneratedHomedir       on
  LDAPDefaultUID                  40
  LDAPDefaultGID                  49

  Include "${proftpd_config_file_users}"
</VirtualHost>
EOF

    commit "${proftpd_config_virtualhost}" || return 1
}

generate_file_perms_group() {
    # File permissions for group
    case "$1" in
        "READ")
            cat >${proftpd_config_file_perms_group}-$1.tmp <<EOF
    <Directory ${path_of_root_ftp}>
        <Limit READ>
            AllowAll
        </Limit>
    </Directory>
EOF
        ;;
        "WRITE")
            cat >${proftpd_config_file_perms_group}-$1.tmp <<EOF
    <Directory ${path_of_root_ftp}>
        <Limit READ WRITE>
            AllowAll
        </Limit>
    </Directory>
EOF
        ;;
    esac
    commit ${proftpd_config_file_perms_group}-$1 || return 1
}

generate_file_users() {
    # Context for members
    cat >>${proftpd_config_file_users}.tmp <<EOF
# ${dn_name}
<Anonymous ${path_of_root_ftp}>
    User                          ${user}
    AnonRequirePassword           on
$(for group in ${user_groups}; do echo "    Include ${proftpd_config_file_perms_group}-${group}"; done)
</Anonymous>

EOF
}

commit() {
    local -a retcom=0

    # If differrences with original
    if [ -f "$1" ]; then
        if diff "$1.tmp" "$1" >/dev/null; then
            rm "$1.tmp"
        else
            mv "$1" "$1.bkp" && mv "$1.tmp" "$1" && RET=255
        fi
    else
        mv "$1.tmp" "$1" && RET=255
    fi || retcom=1

    [ "${RET}" = 255 ] && mail_cont="${mail_cont}"$'\n'"$1"

    return ${retcom}
}

ldap_get_groups() {
    local -a group
    local -a ldap_ftp_groups

    # Getting all groups 
#    ldap_ftp_groups=$(ldapsearch -x -H ldap://ad0.rian.off -b 'dc=msk,dc=rian' -D 'cn=SA_LDAP-Reader,ou=Test&ServiceUsers,dc=msk,dc=rian' -w 'SaL123456' "(&(objectClass=group)(cn=$@))" cn |ldap_translate)
    ldap_ftp_groups=$(ldapsearch -x -H "ldap://$ldap_server" -b "$ldap_groups_location" -D "$ldap_bind_user" -w "$ldap_bind_password" "(&(objectClass=group)(cn=$@))" cn |ldap_translate)

    if [ $? -ne 0 ] || [ ! "${ldap_ftp_groups}" ]; then
        echo "Error ldap request groups $@!" >&2
        return 1
    fi

    # Creating files of permissions
    IFS=$'\n'
    for rq in ${ldap_ftp_groups}; do
        IFS=" "
        if echo "${rq}" |grep '^dn: ' >/dev/null; then
            # Getting members of groups
            users_req=${users_req}$(echo "${rq}" |awk '{sub(/^dn: /,""); print "(memberOf="$0")"}')
        fi
        if echo "${rq}" |grep '^cn: ' >/dev/null; then
            orig_groups=$(echo ${rq} |awk '{sub(/^[^ ]* /,"");print}')" "${orig_groups}
            # Name group without prefix
            group=$(echo "${rq}" |awk '{sub(/.*_/,"",$2); print $2}')
            groups="${group} ${groups}"
            generate_file_perms_group $group
        fi
    done
    generate_proftpd_conf "${users_req}"
}

ldap_get_users_of_groups() {
    local -a a
    local -a pref
    local -a users_pref

    # Getting every member of group
#    users_pref=$(ldapsearch -x -H ldap://ad0.rian.off -b 'dc=msk,dc=rian' -D 'cn=SA_LDAP-Reader,ou=Test&ServiceUsers,dc=msk,dc=rian' -w 'SaL123456' -S 'sAMAccountName' "(&(objectClass=user)(|$@)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))" sAMAccountName memberOf |ldap_translate |awk '{sub(/^$/,"-");print}')
    users_pref=$(ldapsearch -x -H "ldap://$ldap_server" -b "$ldap_users_location" -D "$ldap_bind_user" -w "$ldap_bind_password" -S 'sAMAccountName' "(&(objectClass=user)(|$@)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))" sAMAccountName memberOf |ldap_translate |awk '{sub(/^$/,"-");print}')

    if [ $? -ne 0 ] || [ ! "${users_pref}" ]; then
       echo "Error ldap request groups to contain users!" >&2
       return 1
    fi

    # Creating user with permission files
    IFS=$'\n'
    for pref in ${users_pref}; do
        IFS=" "
        if echo "${pref}" |grep '^dn: ' >/dev/null; then
            # DN record
            a=1
            user_groups=""
            group=""
            user=""
            dn_name=$(echo ${pref} |awk '{sub(/^[^ ]* /,""); print}')
        fi
        if echo "${pref}" |grep "^sAMAccountName: " >/dev/null; then
            # Username record
            user=$(echo ${pref} |awk '{sub(/^[^ ]* /,""); print}')
        fi
        if echo "${pref}" |grep '^memberOf: ' >/dev/null && [ "$a" = "1" ]; then
            # If groups contain user
            for ag in ${orig_groups}; do
                # Accepted groups only for common ftp
                if [ "${ag}" = "$(echo ${pref} |awk '{sub(/^[^ ]* CN=/,""); sub(/,(CN|OU|DC)=.*/,""); print}')" ]; then
                    user_groups=${user_groups}" "$(echo "${ag}" |awk '{sub(/.*_/,""); print $0}')
                    break
                fi
            done
        fi
        if [ "${pref}" = "-" ] && [ "$a" = "1" ]; then
            # Commit recording
            generate_file_users
            a=0
        fi
    done
    commit ${proftpd_config_file_users} || return 1
}


###########################################################################################################################
[ -f "$pidfile" ] && [ "$(cat $pidfile)" ] && ps h $(cat $pidfile) && { echo "Error: process already running"; exit 1; }
echo $$ >$pidfile || exit 1
trap "rm -f $pidfile" 0 1 2 3 15

which "ldap_translate" >/dev/null || { echo "No program: ldap_translate"; exit 1; }
[ -d "$proftpd_config_dir" ] || { echo "Not folder ${proftpd_config_dir}" >&2; exit 1; }
[ -f ${proftpd_config_file_users}.tmp ] && rm ${proftpd_config_file_users}.tmp

ldap_get_groups "${project_ldap_group}_*" || exit 1
ldap_get_users_of_groups "${users_req}" || exit 1

[ "${mail_cont}" ] && (echo "Below files have been changed."; echo "-----------------------------------------------------------"; echo "${mail_cont}") |mail -s "${project_ldap_group}: proftpd changes" root

exit $RET
               