#!/bin/bash
################################################################################
# Монтирование сетевого каталога (ntfs) и его проверка,
# снятие дампа и его сжатие если на целевом диске свободно больше 2Гб,
# очистка старых бекапов, копирование новой переписки с логированием
#
################################################################################
# Переменные

today="$(date +%Y%m%d)"

res="
root /
var /var
usr /usr
data /data
"
networkPath="//server.domain/Backups"
mountFolder="/mnt/backup"
baclup_path="/data3/Backups" #baclup_path=${mountFolder} при копировании на сетевой диск
srv_user=user
srv_pass=pass

################################################################################

function main {
	local log_p="/var/log/backup.log"
	local ret_per="22"
	exec &>${log_p}
 	echo "#####################################################################"
 	echo "BACKUP START!"
 	echo ${today}
# 	packagesInstall
#	mountBackupsDir ${srv_user} ${srv_pass}
	del_old
	echo ${res} | while read res_par; do
		damp_vol ${res_par}
	done
	copy_mail
 	echo "#####################################################################"
	echo "BACKUP DONE!"
}
################################################################################
# Установка пакетов необходимых для монтирования NTFS каталога

function packagesInstall {
	yum -y install cifs-utils
}
################################################################################
# Монтирование сетевого NTFS каталога если он еще не смотнтирован

function mountBackupsDir() {
	checkExistsFolder "${mountFolder}"
	if [[ $(mount -v | grep "${mountFolder}" > /dev/null; echo $?) = 1 ]]; then
		mount.cifs "${networkPath}" "${mountFolder}" -o user=${1},pass=${2}
	fi
}
################################################################################
# Снапшот раздела и его архивация

function damp_vol() {
	local vol_name="${1}"
	local vol_path="${2}"
	local FREE=$(df -k --output=avail "$baclup_path" | tail -n1)
	if [[ ${FREE} -gt 2097152 ]]; then
		dump -0Laf - ${vol_path} | gzip -9 > ${baclup_path}/${today}/${today}_${vol_name}.img.gz
	else
		echo "⚠️ In $baclup_path NO free disk space! ⚠️"
  	fi
}
################################################################################
# Удаление старых архивов

function del_old {
	echo "#####################################################################"
	echo "⚠️ DELETE OLD ARCHIVE! ⚠️"
	find ${backup_path} -mtime +${ret_per} -print -delete
	echo "#####################################################################"
}
################################################################################
# Инкриментное копирование новой переписки

function copy_mail {
	local src_dir="/data/mail/"
	local dst_dir="/data3/copy_mail/"
	echo "#####################################################################"
	echo "⚠️ copy mail ⚠️"
	cp -Rpvn ${src_dir} ${dst_dir} 1>/dev/null
}
################################################################################
# Проверка существования целевого каталоага и его создание

function checkExistsFolder {
	local path="${1}"
	local object="${2}"
	local fullPAth="${1}"/"${2}"
	if [[ ! -d ${fullPAth} ]]; then
		mkdir -p ${fullPAth}
	fi
}
################################################################################

main "$@" 
