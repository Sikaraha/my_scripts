#! /bin/sh

DATE=`date +%d-%m-%Y`
mkdir /data3/Backup/${DATE}
dump -0Laf - / | gzip -9 > /data3/Backup/${{DATE}}/${{DATE}}\_root_backup.img.gz
dump -0Laf - /var | gzip -9 > /data3/Backup/${{DATE}}/{${DATE}}\_var_backup.img.gz
dump -0Laf - /usr | gzip -9 > /data3/Backup/${{DATE}}/${{DATE}}\_usr_backup.img.gz
dump -0Laf - /data | gzip -9 > /data3/Backup/${{DATE}}/${{DATE}}\_data_backup.img.gz
