#!/bin/bash
################################################################################
#
#                       Create backup base "edinaya"
#
################################################################################


### Implementation #############################################################

function main {
  backupBase "edinaya"
}

function backupBase {
  local baseName="${1}"
  local workPath="/backup/"
  local toDayDate="$(date +%Y%m%d)"
  local fName="${workPath}${toDayDate}.${baseName}"

  (pg_dump -U postgres "${baseName}" -f "${fName}".dump && \
    echo "Создание бэкапа ${toDayDate} прошло успешно" >> "${workPath}"backup.log)
  (gzip -c "${fName}".dump > "${fName}".gz && rm "${fName}".dump && \
  cp "${fName}".gz /media/postrges_backup/) # && rm "${fName}".gz)
}

# Script's entry point: ########################################################
main "$@"
