#!/bin/bash

PATH=/opt/zabbix/bin:$PATH

# Проверяем время модификации
let time_mod=$(date +%s)-$(stat -c %Y /tmp/reposync.log)
# Проверяем наличие пакетов
n_pack=$(awk '/ Already downloaded/ {nl++}; END {print nl}' /tmp/reposync.log)

# Отправка значеня в zabbix
# 7200 секунд это 2 часа, 1000 ориентировочное количество пакетов в репе
if [[ (${time_mod} -le 7200) && (${n_pack} -gt 1000) ]]; then
	zabbix_sender -z observer1-0.rian.off -s src-rhel8.rian.off -k update_check -o 1 >/dev/null
    zabbix_sender -z observer1-0.rian.off -s src-rhel8.rian.off -k n_pack -o $n_pack >/dev/null
else
	zabbix_sender -z observer1-0.rian.off -s src-rhel8.rian.off -k update_check -o 0 >/dev/null
fi
