#!/bin/bash
################################################################################
#                                                                              #
#                          New IP Tables Rules v 0.9                           #
#                                                                              #
################################################################################

# Constant's ###################################################################
IPTABLES="/sbin/iptables"

EXTIF="eth0"                               # Внешний сетевой интерфейс
INTIF="br0"                                # Внутренний сетевой интерфейс
TUNIF="tun0"                               # Межсетевой тунель

WAN_IP="82.142.129.162"                    # Внешний IP офиса
GW_IP="192.168.0.1"                        # IP адрес шлюза
DC1_IP="192.168.0.2"                       # Основной домен контроллер IP
DC2_IP="192.168.0.3"                       # Резервный домен контроллер IP
TERMSRV_IP="192.168.0.4"                   # Терминальный сервер
MAIL_IP="192.168.0.15"                     # Почтовый сервер
LOCAL_SUBNET="192.168.0.0/24"              # Подсеть центрального офиса
NOVOSIB_SUBNET="10.2.4.0/24"               # Подсеть Новосибирск
SAMARA_SUBNET="192.168.1.0/24"             # Подсеть Самара
ROSTOV_SUBNET="192.168.2.0/24"             # Подсеть Ростов
VPN_SUBNET="10.10.0.0/24"                  # Подсеть VPN
INGUSHETIA_SUBNET="192.168.4.0/24"         # Подсеть Ингушетия
### Implementation #############################################################
function main {
  enableForwarding
  resetRules
  settingNAT
  settingFilter
  #settingMangle
}
#-------------------------------------------------------------------------------
function enableForwarding {
  # Ядро должно знать что оно может продвигать пакеты
  /bin/echo 1 > /proc/sys/net/ipv4/ip_forward
}
#-------------------------------------------------------------------------------
function resetRules {
  # Сбрасываем правила и удаляем цепочки
  "${IPTABLES}" -F
  "${IPTABLES}" -t nat -F
  "${IPTABLES}" -t mangle -F
  "${IPTABLES}" -X
  "${IPTABLES}" -t nat -X
  "${IPTABLES}" -t mangle -X
}
#-------------------------------------------------------------------------------
function settingNAT {
  # Настройка NAT
  # Проброс портов 25,80,110,111,143,144,443,465,585,993,995 на mail сервер.
  #"${IPTABLES}" -t nat -A PREROUTING -d "${WAN_IP}/32" -p tcp -m multiport --dports 25,80,110,111,143,144,443,465,585,993,995 -j DNAT --to-destination "${MAIL_IP}"
  "${IPTABLES}" -t nat -A PREROUTING -d "${WAN_IP}/32" -p tcp -m multiport --dports 465,993,995 -j DNAT --to-destination "${MAIL_IP}"

  # Проброс 22 портоа на 666 шлюза
  "${IPTABLES}" -t nat -A PREROUTING -d "${WAN_IP}/32" -p tcp -m multiport --dports 22 -j DNAT --to-destination "${GW_IP}":666

  # Проброс 2525 на 25 mail сервера
  #"${IPTABLES}" -t nat -A PREROUTING -d "${WAN_IP}/32" -p tcp -m tcp --dport 2525 -j DNAT --to-destination "${MAIL_IP}":25

  # Проброс 80,443 на 80 шлюза для Домен контроллеров
  "${IPTABLES}" -t nat -A PREROUTING -s "${DC1_IP}/32"     -p tcp -m multiport --dports 80,443 -j DNAT --to-destination "${GW_IP}":80
  "${IPTABLES}" -t nat -A PREROUTING -s "${DC2_IP}/32"     -p tcp -m multiport --dports 80,443 -j DNAT --to-destination "${GW_IP}":80
  "${IPTABLES}" -t nat -A PREROUTING -s "${TERMSRV_IP}/32" -p tcp -m multiport --dports 80,443 -j DNAT --to-destination "${GW_IP}":80
  # Правила для 1С, разрешаем терминальному серверу досуп на определенные адрес:порт
  "${IPTABLES}" -t nat -A PREROUTING -s "${TERMSRV_IP}/32" -d 74.113.233.187/32 -p tcp -m multiport --dports 80,443 -j DNAT --to-destination 74.113.233.187:80
  "${IPTABLES}" -t nat -A PREROUTING -s "${TERMSRV_IP}/32" -d 194.190.207.46/32 -p tcp -m multiport --dports 80,443 -j DNAT --to-destination 194.190.207.46:80
  # Проверка контрагентов ФНС npchk.nalog.ru
  "${IPTABLES}" -t nat -A PREROUTING -s "${TERMSRV_IP}/32" -d 81.177.5.187/32   -p tcp -m multiport --dports 80,443 -j DNAT --to-destination 81.177.5.187:80
  "${IPTABLES}" -t nat -A PREROUTING -s "${TERMSRV_IP}/32" -d 80.68.246.27/32   -j DNAT --to-destination 80.68.246.27

  "${IPTABLES}" -t nat -A POSTROUTING -s "${LOCAL_SUBNET}" -d "${MAIL_IP}/32"     -p tcp -m multiport --dports 22,25,80,110,111,143,144,443,465,585,993,995,5555 -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${LOCAL_SUBNET}" -d "${TERMSRV_IP}/32"  -p tcp -m tcp --dport 3389 -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${LOCAL_SUBNET}" -o eth1 -m mark ! --mark 0x1 -j SNAT --to-source 82.142.129.162
  "${IPTABLES}" -t nat -A POSTROUTING -s "${WAN_IP}/32"    -d "${NOVOSIB_SUBNET}" -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${WAN_IP}/32"    -d "${VPN_SUBNET}"     -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${WAN_IP}/32"    -d "${SAMARA_SUBNET}"  -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${WAN_IP}/32"    -d "${ROSTOV_SUBNET}"  -j SNAT --to-source "${GW_IP}"
  "${IPTABLES}" -t nat -A POSTROUTING -s "${VPN_SUBNET}"   -o eth1 -j SNAT --to-source 82.142.129.162
}
#-------------------------------------------------------------------------------
function settingMangle {
 #*mangle
 "${IPTABLES}" -A POSTROUTING -d "${NOVOSIB_SUBNET}" -j MARK --set-xmark 0x1/0xffffffff
 "${IPTABLES}" -A POSTROUTING -d "${VPN_SUBNET}"     -j MARK --set-xmark 0x1/0xffffffff
 "${IPTABLES}" -A POSTROUTING -d "${SAMARA_SUBNET}"  -j MARK --set-xmark 0x1/0xffffffff
 "${IPTABLES}" -A POSTROUTING -d "${ROSTOV_SUBNET}"  -j MARK --set-xmark 0x1/0xffffffff
}
#-------------------------------------------------------------------------------
function settingFilter {
  # Настройка FILTER
  inputTraff
  outputTraff
  forwardTraff
}
#-------------------------------------------------------------------------------
function inputTraff {
  #input
  #Задание правил для входящего траффика
  #"${IPTABLES}" -A INPUT -s 150.145.60.4/32 -j DROP
  # Открывает доступ с интерфейса eth0
  "${IPTABLES}" -A INPUT -i eth0 -j ACCEPT
  "${IPTABLES}" -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
  # Открывает доступ с интерфейса lo
  "${IPTABLES}" -A INPUT -i lo -j ACCEPT
  # Разрешён доступ протоколам
  "${IPTABLES}" -A INPUT -p icmp -j ACCEPT
  "${IPTABLES}" -A INPUT -p ah -j ACCEPT
  "${IPTABLES}" -A INPUT -p esp -j ACCEPT
  "${IPTABLES}" -A INPUT -p gre -j ACCEPT
  # Порты открытые на шлюзе
  #"${IPTABLES}" -A INPUT -p tcp -m multiport --dports 22,25,53,80,110,123,143,465,585,500,666,993,995,1723,4500,5555,10050,10051 -j ACCEPT
  "${IPTABLES}" -A INPUT -p tcp -m multiport --dports 465,993,995,1723,4500 -j ACCEPT
  #"${IPTABLES}" -A INPUT -p udp -m multiport --dports 53,500,4500,10050,10051,5555 -j ACCEPT
  "${IPTABLES}" -A INPUT -p udp -m multiport --dports 4500 -j ACCEPT

  #Дропаем все остальные пакеты
  "${IPTABLES}" -P INPUT DROP
}
#-------------------------------------------------------------------------------
function outputTraff {
  #output
  #Задание правил для исходящего траффика
  "${IPTABLES}" -A OUTPUT -s "${LOCAL_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -s "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -s "${SAMARA_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -s "${ROSTOV_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -s "${VPN_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -s "${INGUSHETIA_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -p icmp -j icmp-out
  "${IPTABLES}" -A OUTPUT -o "lo" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -o "${EXTIF}" -j ACCEPT
  "${IPTABLES}" -A OUTPUT -o "${TUNIF}" -j ACCEPT

  #Дропаем все остальные пакеты
  "${IPTABLES}" -P OUTPUT DROP
}
#-------------------------------------------------------------------------------
function forwardTraff {
  #forward
  #Задание правил для сквозного траффика, нужно когда шлюз распределяет трафик по внутренней сети

  #"${IPTABLES}" -A FORWARD -s 150.145.60.4/32 -j DROP
  # Доступ из удаленных подсетей к серверам
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -d "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -d "${VPN_SUBNET}"     -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -d "${SAMARA_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -d "${ROSTOV_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -d "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -d "${VPN_SUBNET}"     -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -d "${SAMARA_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -d "${ROSTOV_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d "${VPN_SUBNET}"     -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d "${SAMARA_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d "${ROSTOV_SUBNET}"  -j ACCEPT
  #
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -o eth1 -p tcp -m multiport --dports 53,80,123 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -o eth1 -p udp -m udp       --dport 53 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -o eth1 -p tcp -m multiport --dports 53,80,123 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -o eth1 -p udp -m udp       --dport 53 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -o eth1 -p tcp -m multiport --sports 53,3389 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -o eth1 -p tcp -m multiport --dports 53,80,123 -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -o eth1 -p udp -m udp       --dport 53 -j ACCEPT
  # Доступ к Терминальному серверу для удаленных представителей
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d 74.113.233.187/32   -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -d 194.190.207.46/32   -j ACCEPT



  /sbin/iptables -A FORWARD -s 192.168.0.4/32 -d 77.87.100.54/32 -j ACCEPT
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp -m tcp --dport 16409 -j DNAT --to-destination 192.168.0.4:3389

  /sbin/iptables -t nat -D PREROUTING -i eth1 -p tcp -m tcp --dport 16409 -j DNAT --to-destination 192.168.0.4:3389

  /sbin/iptables -t nat -A PREROUTING -s 192.168.0.4/32 -d 77.87.100.54/32 -p tcp -m multiport --dports 21,22 -j DNAT --to-destination 77.87.100.54:21
  /sbin/iptables -D FORWARD -s 192.168.0.4/32 -o eth1 -j DROP
  /sbin/iptables -A FORWARD -s 192.168.0.4/32 -o eth1 -j DROP



  # Запрет на выход в интернет для Контоллеров домена и Терминального сервера
  "${IPTABLES}" -A FORWARD -s "${DC1_IP}/32"     -o eth1                -j DROP
  "${IPTABLES}" -A FORWARD -s "${DC2_IP}/32"     -o eth1                -j DROP
  "${IPTABLES}" -A FORWARD -s "${TERMSRV_IP}/32" -o eth1                -j DROP
  # Допуск во внутреннюю сеть подсетей
  "${IPTABLES}" -A FORWARD -s "${LOCAL_SUBNET}"   -j ACCEPT
  "${IPTABLES}" -A FORWARD -d "${LOCAL_SUBNET}"   -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${SAMARA_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -d "${SAMARA_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${ROSTOV_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -d "${ROSTOV_SUBNET}"  -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A FORWARD -d "${NOVOSIB_SUBNET}" -j ACCEPT
  "${IPTABLES}" -A FORWARD -s "${VPN_SUBNET}"     -j ACCEPT
  "${IPTABLES}" -A FORWARD -d "${VPN_SUBNET}"     -j ACCEPT

  #Дропаем все остальные пакеты
  "${IPTABLES}" -P FORWARD DROP
}
# Script's entry point: ########################################################
main "$@"