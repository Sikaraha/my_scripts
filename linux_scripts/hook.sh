#!/bin/sh

case "$1" in
  "deploy_challenge")  printf "Please add to DNS:\n_acme-challenge.%s. %d in TXT \"%s\"\n" "${2}" "${TTL}" "${4}"
                       read -p "Configure, then press Enter..." x  ;;

  "clean_challenge" )  printf "Please delete from DNS:\n_acme-challenge.%s. %d in TXT \"%s\"\n" "${2}" "${TTL}" "${4}" ;;

  "deploy_cert"     )  ;; # optional: /path/to/deploy_cert.sh "$@" ;;
esac

