#!/bin/bash

PIDFILE=/var/run/gluster-sync.pid
RSYNC_PROG=/usr/bin/rsync
SRCDIR="/mnt/rhs/rhg_ria22_media /mnt/rhs/rhg_rian21_media /mnt/rhs/rhg_rsport /mnt/rhs/rhg_sputnik_media_vol1 /mnt/rhs/rhg_sputnik_media_vol2 /mnt/rhs/rhg_sputnik_media_vol3 /mnt/rhs/rhg_sputnik_media_vol4 /mnt/rhs/rhg_sputnik_media_vol5 /mnt/rhs/rhg_ria2018_media"
STORWIZE_MOUNT=" "
email="root-linux@rian.ru"

if [ -f "${PIDFILE}" ]; then
    if ps -p $(cat ${PIDFILE}) >/dev/null; then
        echo "Error: `basename $0` already running!"
        exit 1
    else
        rm -f ${PIDFILE}
        echo "$HOSTNAME: Error: the \$pidfile already exist!." | mail -s "$HOSTNAME: pidfile already exist" root-linux@rian.ru
    fi
fi

echo $$ >"$PIDFILE" || { echo "Error: couldn't create the \$pidfile!"; exit 1; }
trap "rm $PIDFILE" 0 1 3 15

check_mnt() {
    if ! mountpoint -q ${1}; then
        echo "error, $1 - not mounted, exiting"
        exit 1
    fi
}

find_dir() {
    find ${1}  -mindepth 1 -maxdepth 1 -type d ! -name deleted-files ! -name .trashcan -printf "%f\n"
}

for dir in ${SRCDIR}
do
    case ${dir} in
        *rhg_rsport)
              DESTDIR=${STORWIZE_MOUNT}/rhg_rsport/images
              SRCDIR="${dir}/images"
              check_mnt ${STORWIZE_MOUNT}
              check_mnt ${dir}
              ;;
        *sputnik_media_vol[0-9])
              DESTDIR=/mnt/storwize/nas-swz2/${dir##*/}
              SRCDIR=${dir}
              check_mnt ${SRCDIR}
              check_mnt ${DESTDIR}
              ;;      
        *rhg_ria2018_media)
              DESTDIR=/mnt/storwize/nas-swz1/${dir##*/}
              SRCDIR=${dir}
              check_mnt ${SRCDIR}
              check_mnt ${DESTDIR}
              ;;      
        *)
              DESTDIR=${STORWIZE_MOUNT}/${dir##*/}
              SRCDIR=${dir}
              check_mnt ${STORWIZE_MOUNT}
              check_mnt ${SRCDIR}
              ;;
    esac

    test -d ${DESTDIR}/deleted-files || mkdir ${DESTDIR}/deleted-files
    
    #first level dirs
    declare -a storwize_dirs=$(find_dir ${DESTDIR})
    declare -a gluster_dirs=$(find_dir ${SRCDIR})
    declare -a new_dirs
    declare -a deleted_dirs
    
    #deleted dirs
    if [[ -n ${storwize_dirs[@]} ]]; then
        for i in ${storwize_dirs[@]}
        do
            if [[ ! -d "${SRCDIR}/${i}"  && $i != ".snapshots" ]]; then
                mv ${DESTDIR}/${i} ${DESTDIR}/deleted-files/${i}.$(date +%Y%m%d%H%M%S) 
                deleted_dirs+=(${DESTDIR}/${i})
            fi
        done
    fi

    unset i
    #new dirs
    if [[ -n ${gluster_dirs[@]} ]]; then
            for i in ${gluster_dirs[@]}
            do
                if [[ ! -d "${DESTDIR}/${i}" ]]; then
                    new_dirs+=(${SRCDIR}/${i})
                fi
            done
    fi

    [[ -n ${new_dirs[@]} ]] && (for i in ${new_dirs[@]};do echo "NEW - $i";done) |mail -s "[GLUSTER] new dirs on volume ${dir#*_}" ${email}
    [[ -n ${deleted_dirs[@]} ]] && (for i in ${deleted_dirs[@]};do echo "DELETED - $i";done)|mail -s "[GLUSTER] deleted dirs on volume ${dir#*_}" ${email}
    
    unset new_dirs deleted_dirs i

    #second level dirs
    filelist=`diff <(find ${SRCDIR} -maxdepth 2 -mindepth 2 -type d ! -path "./deleted-files/*" |sort) <(find ${DESTDIR} -maxdepth 2 -mindepth 2 -type d ! -path "./deleted-files/*" |sort) |awk '/>/ {print $2}'`
    
    for i in ${filelist};
    do
        mdir=`echo ${i} |awk -F "/" '{print $2}'`
        if [[ -d ${DESTDIR}/deleted-files/${mdir} ]]; then
            mv -v ${i} ${DESTDIR}/deleted-files${i#.}.$(date +%Y%m%d%H%M%S)
        else
            mkdir ${DESTDIR}/deleted-files/${mdir}
            mv -v ${i} ${DESTDIR}/deleted-files${i#.}.$(date +%Y%m%d%H%M%S)
        fi
    done
    
    test -d deleted-files && find ${DESTDIR}/deleted-files/ -type f -ctime +35 -exec rm -f {} +
    
    #rm empty directory:
    test -d deleted-files && find ${DESTDIR}/deleted-files/ -mindepth 1 -type d -empty -delete
    
    #fix for rsport
    DESTDIR=${DESTDIR%%/images}
    RSYNC_LOG=/mnt/md7/gluster-rsync-${dir##*/}-$(date +%Y%m%d%H%M%S).full.log
    RSYNC_OPTS="-a --delete --suffix=.$(date +%Y%m%d%H%M%S) --backup --backup-dir=${DESTDIR}/deleted-files/ --relative --log-file=${RSYNC_LOG} --exclude=*/captcha/images/* --exclude=.snapshots"
    (find ${dir} -mindepth 2 -maxdepth 2 -print0 | xargs -0 -n1 -P3 -I% ${RSYNC_PROG} ${RSYNC_OPTS} "%" ${DESTDIR} && touch ${DESTDIR}/.last ) &
done
wait
