#!/bin/bash

#объявляем переменные
IPTABLES='/sbin/iptables' #собсно сами тэйблсы

EXTIF='eth0' #Внешний сетевой интерфейс

INTIF='br0' #Внутренний сетевой интерфейс

#TUNIF='tun0' #Межсетевой тунель

#-----------------------------------------------

/bin/echo 1 > /proc/sys/net/ipv4/ip_forward #Включаем форвардинг

#-----------------------------------------------
#Сбрасываем правила и удаляем цепочки
$IPTABLES -F
$IPTABLES -t nat -F
$IPTABLES -t mangle -F
$IPTABLES -X
$IPTABLES -t nat -X
$IPTABLES -t mangle -X
#-----------------------------------------------

#настройка nat

$IPTABLES -t nat -A POSTROUTING -o $EXTIF -j MASQUERADE #Даём выход в интернет
$IPTABLES -t nat -A POSTROUTING -o $INTIF -j MASQUERADE #to see tap networks

$IPTABLES -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 3389 -j DNAT --to-destination 192.168.11.253:3389 #rdp port forwardingu
$IPTABLES -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 13389 -j DNAT --to-destination 192.168.11.251:3389 #rdp port forwardingu
$IPTABLES -t nat -A PREROUTING -i eth0 -p tcp -m tcp --dport 33892 -j DNAT --to-destination 192.168.11.170:3389

#$IPTABLES -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3128 #redirect proxy

#настройка filtre

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 443 -j ACCEPT #SSL ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 443 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 443 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 443 -j ACCEPT


$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 110 -j ACCEPT #POP3 ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 110 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 110 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 110 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 25 -j ACCEPT #SMTP ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 25 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 25 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 25 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 21 -j ACCEPT #fTP ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 21 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 21 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 21 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 80 -j ACCEPT #fTP ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 80 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 80 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 80 -j ACCEPT



$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 3389 -j ACCEPT #RDP ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 3389 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 3389 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 3389 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 5190 -j ACCEPT #ICQ ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 5190 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 5190 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 5190 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 5690 -j ACCEPT #AUTOTRACKER ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 5690 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 5690 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 5690 -j ACCEPT

$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 3345 -j ACCEPT #AUTOTRACKER ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 3345 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 3345 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 3345 -j ACCEPT


$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --dport 1723 -j ACCEPT #AUTOTRACKER ACCESS
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --dport 1723 -j ACCEPT
$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp --sport 1723 -j ACCEPT
$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp --sport 1723 -j ACCEPT


#$IPTABLES -A FORWARD -o $EXTIF -i br0 -p tcp -j DROP
#$IPTABLES -A FORWARD -i $EXTIF -o br0 -p tcp -j DROP

$IPTABLES -A INPUT -p tcp -i $INTIF --dport 22 -j ACCEPT #Открываем 22 порт на внутреннем сетевом интерфейсе

$IPTABLES -A INPUT -p tcp -i $EXTIF --dport 22 -j ACCEPT #Открываем 22 порт external (not always)

$IPTABLES -A INPUT -p tcp --dport 80 -j ACCEPT #Открываем 1194 порт для использования openvpn

iptables -I INPUT -p tcp --dport 10000 -j ACCEPT
systemctl stop firewalld
systemctl mask firewalld
yum -y install iptables-services
systemctl enable iptables
systemctl start iptables
service iptables save


#$IPTABLES -A INPUT -p tcp -i $INTIF --dport 139 -j ACCEPT #Открываем 139 порт для использования samba

#$IPTABLES -A INPUT -p tcp -i $INTIF --dport 445 -j ACCEPT #Открываем 445 порт для использования samba

$IPTABLES -A INPUT -p tcp -i $INTIF --dport 3128 -j ACCEPT #squid

$IPTABLES -A INPUT -p tcp -i $EXTIF --dport 1723 -j ACCEPT #входящие подключения по pptp

$IPTABLES -A INPUT -p icmp -j ACCEPT #Разрешаем пинг

#$IPTABLES -A INPUT -i $EXTIF -m state --state NEW,INVALID -j DROP #Закрываем все входящие по внешнему сетевому интерфейсу

#$IPTABLES -A INPUT -i $INTIF -m state --state NEW,INVALID -j DROP #Закрываем все входящие по внутреннему сетевому интерфейс#у


