
#!/bin/bash
################################################################################
#
# Для правильной работы скрипта необходимо создать ЧИСТУЮ базу через менеджер
# PostgreSQL или с помощью консоли.
# restoreBackup  <имя базы, куда восстанавливаем> <имя дампа>
# В переменную workPath указываем путь до папки с бэкапом
#
################################################################################


### Implementation #############################################################

function main {
 #restoreBackup  <имя базы, куда восстанавливаем> <имя дампа>
  restoreBackup "test" "20150222.edinaya"
}

function restoreBackup {
  local workPath="/backup/"
  local workBase="${1}"
  local backupName="${2}"
  local fullPath="${workPath}${backupName}.gz"

  (gunzip "${fullPath}.gz" && /
    psql -U postgres -d "${workBase}" -f "${fullPath}" && /
    rm "${fullPath}")

}

# Script's entry point: ########################################################
main "$@"
