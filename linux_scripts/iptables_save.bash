#!/bin/bash
################################################################################
#                                                                              #
#                          IP Tables Save OR Restore                           #
#                                                                              #
################################################################################

# Constant's ###################################################################

WORK_DIR="/home/admin/iptables/"
FLAG="${1}"
IPTABLES_RESTORE="${2}"

#-------------------------------------------------------------------------------
function main {
  if [ "${FLAG}" == "save" ]; then
    Save
  elif [ "${FLAG}" == "restore" ]; then
    Restore "${IPTABLES_RESTORE}"
  else
    echo ""
    echo "iptables_save.bash [save]|[restore <file_name.backup>]"
    echo ""
  fi
}
#-------------------------------------------------------------------------------
function Save {
  local iptblSave="/sbin/iptables-save"

  if [ ! -d "${WORK_DIR}" ]; then
    mkdir -p "${WORK_DIR}"
  fi
  mdate="$(date +%Y%m%d\_%H:%M:%S)"
  sudo "${iptblSave}" >"${WORK_DIR}${mdate}_iptables.backup"
}
#-------------------------------------------------------------------------------
function Restore {
  local iptblRest="/sbin/iptables-restore"
  local fname="${1}"
  if [ ! -d "${WORK_DIR}${IPTABLES_RESTORE}" ]; then
    sudo "${iptblRest}" <"${WORK_DIR}${IPTABLES_RESTORE}"
  else
    echo "Укажите файл для восстановления"
  fi
}
#-------------------------------------------------------------------------------

# Script's entry point: ########################################################
main "$@"

