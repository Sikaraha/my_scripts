#!/bin/bash
################################################################################
# Снятие дампа и архивация db, архивация www
# Монтировнаие удаленного smb каталога и перемещение в него архивив
#
################################################################################
function main {
  echo "#######################################################################"
  date
  #packagesInstall
  mountBackupsDir "backup" "kLjgb867"
  backupWebFile 
  sqlDump
}
################################################################################
function packagesInstall {
  yum -y install cifs-utils
}
################################################################################
function mountBackupsDir() {
  local networkPath="//lv-r-srv-bak-1.lb.int/Backups"
  local mountFolder="/mnt/backup"
  local backupUser="${1}"
  local backupPass="${2}"
  checkExistsFolder "${mountFolder}"
  if [[ $(mount -v | grep "${mountFolder}" >/dev/null; echo $?) = 1 ]]; then
    mount.cifs "${networkPath}" "${mountFolder}" -o user=${backupUser},pass=${backupPass}
  fi
}
################################################################################
function backupWebFile {
  local inDirBackup="/root/backup"
  local outDirBackup="/mnt/backup"
  local wwwDir="/home/bitrix/www"
  local today="$(date +%Y%m%d)"
  local backupFullName="${inDirBackup}/${today}.okmr.gz"
  local ex1="${wwwDir}/bitrix/backup/*"
  local ex2="${wwwDir}/.git/*"
  local ex3="${wwwDir}/upload/*"
  local pcName=$(hostname -s)
  local FREE=$(df -k --output=avail "$inDirBackup" | tail -n1)
  checkExistsFolder "${inDirBackup}"
  if [[ ${FREE} -gt 2097152 ]]; then
    tar -zcf  "${backupFullName}"  "${wwwDir}" \
                            --exclude="${ex1}" \
                            --exclude="${ex2}" \
                            --exclude="${ex3}"
    else
      echo "⚠️ There is NO free disk space! Archiving files is not possible!⚠️"
  fi
  if [[ -f ${backupFullName} ]]; then
    if [[ -d "${outDirBackup}"/"${pcName}" ]]; then
      cp ${backupFullName} ${outDirBackup}/${pcName} && rm -f ${backupFullName}
    else
      echo "⚠️ WARNING! The destination directory for copying the file archive is not mounted!!! Big risk of filling up disk space!!!⚠️"
    fi
  else
    echo "⚠️ Archive is missing!⚠️"
  fi
}
################################################################################
function sqlDump {
  local dirBackup="/root/backup"
  local dbName="okmr"
  local dbUser="root"
  local dbPass=""
  local today="$(date +%Y%m%d)"
  local inDirBackup=${dirBackup}/${today}.${dbName}.sqldump.gz
  local outDirBackup="/mnt/backup"
  local pcName=$(hostname -s)
  local FREE=$(df -k --output=avail "$dirBackup" | tail -n1)
  if [[ ${FREE} -gt 2097152 ]]; then
    mysqldump -u ${dbUser} ${dbName} | gzip > ${inDirBackup}
    else
      echo "⚠️ There is NO free disk space! Archiving database dump is not possible!⚠️"
  fi
  if [[ -f ${inDirBackup} ]]; then
    if [[ -d "${outDirBackup}"/"${pcName}" ]]; then
      cp ${inDirBackup} ${outDirBackup}/${pcName} && rm -f ${inDirBackup}
    else
      echo "⚠️ WARNING! The target directory for copying the database dump is not mounted!!! Big risk of filling up disk space!!!⚠️"
    fi
  else
    echo "⚠️ Database dump is missing!⚠️"
  fi
}
################################################################################
function checkExistsFolder {
  local path="${1}"
  local object="${2}"
  local fullPAth="${1}"/"${2}"
  if [[ ! -d ${fullPAth} ]]; then
    mkdir -p ${fullPAth}
  fi
}
# Script's entry point:
################################################################################
main "$@" >> /root/backup/backup.log
