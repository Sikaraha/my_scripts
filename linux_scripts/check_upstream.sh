#!/bin/bash
DATE=`date +%Y%m%d`
LOGIN="zabbix"
PASS="3UqEyC7Us75hi6Wlq6Jc"

ELK_QUERY=$(cat <<EOF
{
  "version": true,
  "size": 1,
  "sort": [
    {
      "@timestamp": {
        "order": "desc"
      }
    }
  ],
  "query": {
    "bool": {
      "filter": [
        {
          "match_phrase": {
            "ha_http_host_header.keyword": "${1}"
          }
        },
        {
          "range": {
            "@timestamp": {
              "gte": "now-3m",
              "lt": "now"
            }
          }
        }
      ],
      "should": [
        {
          "term": {
            "ha_client_ip": "66.110.32.128/30"
          }
        },
        {
          "term": {
            "ha_client_ip": "83.234.15.112/30"
          }
        },
        {
          "term": {
            "ha_client_ip": "87.245.197.192/30"
          }
        },
        {
          "term": {
            "ha_client_ip": "185.94.108.0/24"
          }
        }
      ],
      "minimum_should_match" : 1
    }
  },
  "_source": [
    "ha_http_host_header",
    "ha_client_ip",
    "@timestamp"
  ]
}
EOF
)

curl -s -k -u ${LOGIN}:${PASS} "https://elk1.rian.off:9200/syslog-ng-haproxy-${DATE}-*/_search" -H "Content-Type: application/json" -d "${ELK_QUERY}" \
	|jq .hits.total.value
	#|jq .hits.hits[]._source.ha_client_ip

