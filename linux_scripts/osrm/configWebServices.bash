#!/bin/bash
################################################################################
# Запускам скрипт OSRM как демон
# Добавлено в rc.local
# Версия для Centos
################################################################################
function main {
  configWebServices
  ln -s ${webPath}/okmr.conf  ${webPath}/sites-enabled/okmr.conf
}
################################################################################
function configWebServices {
  local webPath="/etc/httpd/"
  /bin/cat << EOFconfigWebServices > ${webPath}/sites-available/okmr.conf
upstream osrm {
    server 0.0.0.0:5000;
}

server {
    listen 80;
    server_name your_server_ip;

    location /example_path {
        proxy_pass http://osrm/;
        proxy_set_header Host $http_host;
    }
}
EOFconfigWebServices
}


yum install -y yum-utils centos-release-scl
yum-config-manager --enable rhel-server-rhscl-7-rpms
yum install -y devtoolset-6
scl enable devtoolset-6 bash
yum install -y git cmake zlib-devel

# Script's entry point:
main "$@"
################################################################################
