#!/bin/bash
################################################################################
# Запускам скрипт OSRM как демон
# Версия для Centos
################################################################################

# Имя сервиса
DAEMONNAME="osrm"
# Путь до *.service файла. По умолчанию сюда /etc/systemd/system/
DAEMONPATH="/etc/systemd/system/"
# Путь до скрипта, который запускаем как сервис
SCRIPTPATH="/usr/bin/${DAEMONNAME}.bash"

################################################################################
function main {
  createServiceFile ${DAEMONNAME} ${DAEMONPATH} ${SCRIPTPATH}
  createRunServiceFile ${DAEMONNAME} ${SCRIPTPATH}
  restartDaemonService
  startNewService ${DAEMONNAME}
  checkStatus ${DAEMONNAME}
  autoStartService ${DAEMONNAME}
}
################################################################################
function createServiceFile {
  local daemonName=${1}
  local daemonPath=${2}
  local scriptPath=${3}
  /bin/cat <<EOFcreateServiceFile > ${daemonPath}/${daemonName}.service
[Unit] 
Description=${daemonName}

[Service] 
Type=simple 
ExecStart=${scriptPath}

[Install] 
WantedBy=multi-user.target
EOFcreateServiceFile
}
################################################################################
function createRunServiceFile {
  local daemonName=${1}
  local scriptPath=${2}
  /bin/cat <<'EOFcreateRunServiceFile' > ${scriptPath}
#!/bin/bash
################################################################################
# Запускам скрипт OSRM как демон
# Добавлено в rc.local
################################################################################
function main {
local osrmRouted=/home/user/osrm-backend/build/osrm-routed
local osrmMaps=/home/user/osrm-backend/maps/central-fed-district-latest.osrm
${osrmRouted} --algorithm=MLD ${osrmMaps}
}
# Script's entry point:
################################################################################
main "$@"
EOFcreateRunServiceFile
chmod +x ${scriptPath}
}
################################################################################
function restartDaemonService {
  sudo systemctl daemon-reload
}
################################################################################
function startNewService {
  local daemonName=${1}
  sudo systemctl start ${daemonName}
}
################################################################################
function checkStatus {
  local daemonName="${1}"
  local logPath="/var/log/"${daemonName}".log"
  sudo systemctl status ${daemonName} >> ${logPath}
}
################################################################################
function autoStartService {
  local daemonName="${1}"
  sudo systemctl enable ${daemonName}
}
# Script's entry point:
################################################################################
main "$@"