#!/bin/bash
################################################################################
# Запускам скрипт OSRM как демон
# Добавлено в rc.local
# Версия для Ubuntu
################################################################################

function main {

local osrmRouted=/home/osm/Project-OSRM/build/osrm-routed
local osrmMaps=/home/osm/Project-OSRM/central-fed-district-latest.osrm

start-stop-daemon -Sbvx ${osrmRouted} ${osrmMaps} &
}


# Script's entry point:
################################################################################
main "$@"





