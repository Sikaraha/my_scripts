#!/bin/bash
################################################################################
# Запускам скрипт OSRM как демон
# Добавлено в rc.local
# Версия для Centos
################################################################################

function main {
  local osrmPath="/root/Project-OSRM"
  local osrmRouted=${osrmPath}/build/osrm-routed
  local osrmMaps=${osrmPath}/central-fed-district-latest.osrm
  ${osrmRouted} ${osrmMaps} &
}

# Script's entry point:
################################################################################
main "$@"