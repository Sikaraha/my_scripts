#!/bin/bash

#объявляем переменные
IPTABLES='/sbin/iptables' #собсно сами тэйблсы

EXTIF='eth1' #Внешний сетевой интерфейс

INTIF='eth0' #Внутренний сетевой интерфейс

#TUNIF='tun0' #Межсетевой тунель

#-----------------------------------------------

/bin/echo 1 > /proc/sys/net/ipv4/ip_forward #Включаем форвардинг

#-----------------------------------------------
#Сбрасываем правила и удаляем цепочки
$IPTABLES -F
$IPTABLES -t nat -F
$IPTABLES -t mangle -F
$IPTABLES -X
$IPTABLES -t nat -X
$IPTABLES -t mangle -X
#-----------------------------------------------