#!/bin/bash

export LOGFILE=/data/msrb/logs/backup-archive/`basename $0`_$(date +%Y%m%d%H%M%S).log
export PIDFILE=/var/run/`basename $0`.pid

exec >>$LOGFILE 2>&1

if [ -f "$PIDFILE" ]; then
  if ps -p $(cat $PIDFILE) >/dev/null; then
    echo "Error: `basename $0` already running!"
    exit 1
  else
    rm -f $PIDFILE
    # send mail for debugging
    echo "$HOSTNAME: Error: the \$pidfile already exist!." 
  fi
fi

echo $$ >"$PIDFILE" || { echo "Error: couldn't create the \$pidfile!"; exit 1; }
trap "rm $PIDFILE" 0 1 3 15

echo -e "\n===========\nTime backup started = $(date +%T)"
before="$(date +%s)"

check_mnt() {
    if ! mountpoint -q $1; then echo "error, $1 - not mounted, exiting";exit 1;fi
}

check_mnt "/data/glusterfs"
check_mnt "/data/archive-backup"

echo "Отметка времени для мониторинга"
echo "$(date +%s)" > /data/glusterfs/MSRB/archive/.last

echo "find and remove deleted files older than 7 days:"
find /data/archive-backup/deleted/ -type f -mtime +7 -print -delete

echo "find and remove empty folders in deleted files:"
find /data/archive-backup/deleted/ -type d -empty -print -delete

#for i in $(find /data/glusterfs/MSRB/archive/ -mindepth 1 -maxdepth 1 -type d -mtime -3); do
rsync -au --delete -b --backup-dir=/data/archive-backup/deleted  --suffix=.$(date +%Y%m%d%H%M%S) --log-file=${LOGFILE} /data/glusterfs/MSRB/archive/ /data/archive-backup/archive/

echo "Compress logs"
find /data/msrb/logs/backup-archive -regex ".*\.log$" -mtime +1 -exec xz {} \;

echo "delete old logs"
find /data/msrb/logs/backup-archive -regex ".*\.log.xz$" -mtime +30 -delete

echo Time backup finished = $(date +%T)
after="$(date +%s)"
elapsed="$(expr $after - $before)"
hours=$(($elapsed / 3600))
elapsed=$(($elapsed - $hours * 3600))
minutes=$(($elapsed / 60))
seconds=$(($elapsed - $minutes * 60))
echo Time taken: "$hours hours $minutes minutes $seconds seconds"
