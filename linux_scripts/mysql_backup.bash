#!/bin/bash
################################################################################
# Снятие дампа, архивация и передача в монторованный каталог
# Архивация www и откправка на монтированный каталог
# Global constant's ############################################################

################################################################################
function main {
  packagesInstall
  mountBackupsDir "backup" "kLjgb867"
  #sqlDump
  #backupWebFile
}
################################################################################
function sqlDump {
  local dirBackup="/root/backup"
  local dbName="crm_okmr"
  local dbUser="root"
  local dbPass=""
  local today="$(date +%Y%m%d)"
  local inDirBackup=${dirBackup}/${today}.${dbName}.sqldump.gz
  local outDirBackup="/mnt/backup"
  local pcName=$(hostname -s)
  mysqldump -u ${dbUser} ${dbName} | gzip > ${inDirBackup}
  checkExistsFolder "${outDirBackup}" "${pcName}"
  if [[ -f ${inDirBackup} ]]; then
    cp ${inDirBackup} ${outDirBackup}/${pcName} && rm -f ${inDirBackup}
  fi
}
################################################################################
function mountBackupsDir() {
  local networkPath="//lv-r-srv-bak-1.lb.int/Backups"
  local mountFolder="/mnt/backup"
  local backupUser="${1}"
  local backupPass="${2}"
  checkExistsFolder "${mountFolder}"
  if [[ $(mount -v | grep "${mountFolder}" >/dev/null; echo $?) = 1 ]]; then
    mount.cifs "${networkPath}" "${mountFolder}" -o user=${backupUser},pass=${backupPass}
  fi
}
################################################################################
function backupWebFile {
  local inDirBackup="/root/backup"
  local outDirBackup="/mnt/backup"
  local wwwDir="/home/bitrix/www"
  local today="$(date +%Y%m%d)"
  local backupFullName="${inDirBackup}/${today}.okmr.gz"
  local ex1="${wwwDir}/bitrix/backup/*"
  local ex2="${wwwDir}/.git/*"
  local ex3="${wwwDir}/upload/*"
  local pcName=$(hostname -s)
  checkExistsFolder "${inDirBackup}"
  tar -zcvf  "${backupFullName}"  "${wwwDir}" \
                              --exclude="${ex1}" \
                              --exclude="${ex2}" \
                              --exclude="${ex3}"
  checkExistsFolder "${outDirBackup}" "${pcName}"
  if [[ -f ${backupFullName} ]]; then
    cp ${backupFullName} ${outDirBackup}/${pcName} && rm -f ${backupFullName}
  fi
}
################################################################################
function checkExistsFolder {
  local path="${1}"
  local object="${2}"
  local fullPAth="${1}"/"${2}"
  if [[ ! -d ${fullPAth} ]]; then
    mkdir -p ${fullPAth}
  fi
}
################################################################################
function packagesInstall {
  #yum -y install samba-client samba-common
  yum -y install cifs-utils
}

# Script's entry point:
################################################################################
main "$@"