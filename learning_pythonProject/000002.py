# -*- coding: utf-8 -*-
import re, json
from subprocess import Popen, PIPE


class LogParser(object):
    def init(self, options):
        pattern_pgsql_general = options["regex_pgsql_general"]
        pattern_pgsql_other = options["regex_pgsql_other"]
        self.regex_pgsql_general = re.compile(pattern_pgsql_general)
        self.regex_pgsql_other = re.compile(pattern_pgsql_other)
        self.counter = 0
        return True

    def deinit(self):
        return True

    def parse(self, log_message):
        match = self.regex_pgsql_general.match(log_message['MESSAGE'])
        if match:
            match = match.groupdict()
            if match.get('message_type') == 'statement' and match.get('command_type') in ['SELECT', 'UPDATE', 'DELETE', 'INSERT']:
                message_body = match.get('message_body').replace('#011', ' ').replace('#012', ' ')
                command = ["/usr/bin/normalize7", message_body]
                # normalize_qurey, errs = Popen(command, universal_newlines=True, stdout=PIPE).communicate()
                # query_text = re.findall('^.*\;', normalize_qurey)
                # query_parameter = re.findall('\{.*\}$', normalize_qurey)
                query_text = message_body
                query_parameter = 'none'
                try:
                    # match['query_text'] = query_text[0]
                    # match['query_parameter'] = query_parameter[0]
                    match['query_text'] = query_text
                    match['query_parameter'] = query_parameter
                    match['message_body'] = ''
                except IndexError:
                    match['query_text'] = '{ERR}'
                    match['query_parameter'] = '{ERR}'
            for key, value in match.items():
                if value is not None:
                    if "pgsql_timestamp" in key:
                        value = str(value.replace(" ", "T", 1)) + "+0300"
                    log_message[key] = str(value)
            self.counter += 1
            # return True
            return print(log_message)
        else:
            match = self.regex_pgsql_other.match(log_message['MESSAGE'])
            if match:
                for key, value in match.groupdict().items():
                    if value is not None:
                        log_message[key] = str(value)
                self.counter += 1
                return True
        return False


options = {
    "regex_pgsql_general": "\\[\\d+\\]\\s(?P<pgsql_timestamp>\\d+-\\d+-\\d+\\s\\d+:\\d+:\\d+.\\d+)\\sMSK\\s\\[\\d+\\]:\\s\\[\\d+\\]\\suser=(?P<pgsql_user>[^,]+),db=(?P<pgsql_db>\\S+)\\s\\[(?P<transaction_backend_id>\\d+)/(?P<transaction_localxid>\\d+)\\]\\s\\[(?P<command_type>[A-z\\s]*)\\]\\s?(?:\\[[a-z0-9\\.]+\\])?\\s(?:LOG:)?\\s*(?:duration:)?\\s?(?P<duration>[0-9.]+)?\\s?(?:ms)?\\s*(?P<message_type>\\w+):?\\s?(?:\\S+:)?\\s*(?P<command_comment>\\/\\*[\\sa-z\\-]*(?P<haproxy_request_id>[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+)?.?\\*\\/)?\\s*(?P<message_body>.+)$",
    "regex_pgsql_other": "(?P<other_message>.*)"
}

# options = {
#     "regex_pgsql_general": "\\[\\d+\\]\\s(?P<pgsql_timestamp>\\d+-\\d+-\\d+\\s\\d+:\\d+:\\d+.\\d+)\\sMSK\\s\\[\\d+\\]:\\s\\[\\d+\\]\\suser=(?P<pgsql_user>[^,]+),db=(?P<pgsql_db>\\S+)\\s\\[(?P<transaction_backend_id>\\d+)/(?P<transaction_localxid>\\d+)\\]\\s\\[(?P<command_type>[A-z\\s]*)\\]\\s?(?:\\[[a-z0-9\\.]+\\])?\\s(?:LOG:)?\\s*(?:duration:)?\\s?(?P<duration>[0-9.]+)?\\s?(?:ms)?\\s*(?P<message_type>\\w+):?\\s?(?:\\S+:)?\\s*(?P<command_comment>\\/\\*[\\sa-z\\-]*(?P<haproxy_request_id>[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+:[0-9A-Z_]+)?.?\\*\\/)?\\s*(?P<message_body>.+)$",
#     "regex_pgsql_other": "(?P<other_message>.*)"
# }

log = LogParser()
log.init(options)

arg = {
    'MESSAGE': '[2381] 2021-04-28 10:09:46.580 MSK [55466]: [2373] user=ria22,db=ria22 [1044/0] [SELECT] [60890a2b.d8aa] LOG:  duration: 1.266 ms  statement: INSERT INTO "cms_user_logger" ("cms_user_logger_id", "cms_user_logger_obj_id", "cms_user_logger_obj_type", "cms_user_logger_action", "cms_user_logger_old_values", "cms_user_logger_new_values", "cms_user_logger_user_sid", "cms_user_logger_user_name") VALUES ($1, $2, $3, $4, $5, $6, $7, $8);{1730172006,\'1730161384\',\'Article\',\'update\',\'{"modifiedDate":"2021-04-27T16:18:27+03:00","ArticleBlock":{"oldValues":[{"body":"Впервые о грозовой астме как о регулярном явлении заговорили после трех событий 1980-х годов: в <a href="http:\/\/ria.ru\/location_Birmingham\/" target="_blank" data-auto="true">Бирмингеме<\/a> в <a href="http:\/\/ria.ru\/location_United_Kingdom\/" target="_blank" data-auto="true">Великобритании<\/a> в 1983 году, и в Мельбурне в 1987 и 1989 годах. Затем последовали вспышки в <a href="http:\/\/ria.ru\/location_London_2\/" target="_blank" data-auto="true">Лондоне<\/a> в 1994 году, <a href="http:\/\/ria.ru\/location_Madrid\/" target="_blank" data-auto="true">Мадриде<\/a> в 2001-м, <a href="http:\/\/ria.ru\/location_Napoli\/" target="_blank" data-auto="true">Неаполе<\/a> в 2004-м, <a href="http:\/\/ria.ru\/location_Iran\/" target="_blank" data-auto="true">Иране<\/a> в 2013-м, <a href="http:\/\/ria.ru\/location_China\/" target="_blank" data-auto="true">Китае<\/a> в 2018 году — всего 26 событий, из которых десять случились в Австралии. Причем в районе Мельбурна эпизоды грозовой астмы происходят примерно раз в пять лет.","modifiedDate":"2021-04-27T15:43:18+03:00"},{"body":"Виной тому — поля райграса, пыльца которого считается сильным аллергеном. Во время цветения этого растения аллергики стараются не выходить из дома. Но в том то и заключалась загадка, что во время вспышек грозовой астмы, симптомы удушья отмечали и здоровые в этом отношении люди. Результаты трехлетнего обсервационного <a href="https:\/\/apallergy.org\/DOIx.php?id=10.5415\/apallergy.2020.10.e30" target="_blank" rel="nofollow noopener">исследования<\/a> показали, что из примерно десяти тысяч пострадавших в Мельбурне в ноябре 2016 года у 56 процентов никогда раньше не диагностировали астму, а 13 процентов не имели даже ринита в анамнезе.","modifiedDate":"2021-04-27T15:43:18+03:00"},{"body":"Довольно быстро ученые установили, что аллергическую реакцию вызывает не сама пыльца плевела, а заключенные внутри пыльцевых зерен крахмальные гранулы. В каждой частице пыльцы содержится до 700 таких гранул. Пока зерна пыльцы целые, они задерживаются волосками в носу, а высвободившиеся гранулы микронного размера легко проходят сквозь этот фильтр, попадая напрямую в бронхи и легкие и вызывая приступ астмы. Оставалось понять механизм высвобождения гранул.","modifiedDate":"2021-04-27T15:43:18+03:00"},{"body":"После вспышки 2016 года в Мельбурне департамент здравоохранения штата <a href="http:\/\/ria.ru\/location_StateofVictoria\/" target="_blank" data-auto="true">Виктория<\/a> создал систему мониторинга. К работе активно подключились ученые, которые регулярно отбирали пробы воздуха на наличие в них зерен пыльцы и их фрагментов. Пробы, взятые в период сезона пыльцы, показали, что после дождя содержание в атмосфере фрагментов разрушенных зерен — субпыльцевых частиц SPP (sub-pollen particles) — увеличивается в 50 раз по сравнению с другими днями того же сезона. ","modifiedDate":"2021-04-27T15:43:18+03:00"},{"body":"Отсюда родилась первоначальная гипотеза: во время грозы сильный ветер поднимает в воздух зерна пыльцы, которые, напитываясь от дождя влагой, лопаются в результате осмотического шока и распадаются на SPP, разносимые ветром. Исследования динамики конвективных течений в атмосфере в зоне движения грозового фронта <a href="https:\/\/journals.ametsoc.org\/view\/journals\/apme\/56\/5\/jamc-d-17-0027.1.xml" target="_blank" rel="nofollow noopener">подтвердили<\/a> возможность такого сценария. Однако оставались вопросы. Грозы в период цветения трав — не такая уж редкость, и все они сопровождаются сильными ветрами и повышением влажности, но далеко не каждая приводит к вспышке заболевания. ","modifiedDate":"2021-04-27T15:43:18+03:00"},{"body":"Появилось <a href="https:\/\/onlinelibrary.wiley.com\/doi\/full\/10.1111\/resp.13410" target="_blank" rel="nofollow noopener">предположение<\/a>, что для возникновения катастрофических последствий нужен "идеальный шторм" — одновременное совпадение многих факторов, таких как специфические погодные условия, когда длительный период жары и засухи, приводящий к рассеиванию и растрескиванию зерен пыльцы, сменяется сильным ветром и дождем; увеличение концентрации углекислого газа в атмосфере и температуры воздуха, усиливающее аллергическую реакцию; нахождение людей на воздухе в самое неблагоприятное время и так далее. Но окончательный ответ удалось найти, только когда ученые выполнили компьютерное моделирование, учитывающее все эти факторы.","modifiedDate":"2021-04-27T15:43:18+03:00"}]}}\',\'{"modifiedDate":"2021-04-27T16:27:54+03:00","ArticleBlock":{"newValues":[{"body":"Грозовую астму фиксировали в <a href="http:\/\/ria.ru\/location_Birmingham\/" target="_blank" data-auto="true">Бирмингеме<\/a> в <a href="http:\/\/ria.ru\/location_United_Kingdom\/" target="_blank" data-auto="true">Великобритании<\/a> в 1983-м, Мельбурне — в 1987-м и 1989-м, <a href="http:\/\/ria.ru\/location_London_2\/" target="_blank" data-auto="true">Лондоне<\/a> — в 1994-м, <a href="http:\/\/ria.ru\/location_Madrid\/" target="_blank" data-auto="true">Мадриде<\/a> — в 2001-м, <a href="http:\/\/ria.ru\/location_Napoli\/" target="_blank" data-auto="true">Неаполе<\/a> — в 2004-м, <a href="http:\/\/ria.ru\/location_Iran\/" target="_blank" data-auto="true">Иране<\/a> — в 2013-м, <a href="http:\/\/ria.ru\/location_China\/" target="_blank" data-auto="true">Китае<\/a> — в 2018=м. Всего 26 случаев, из которых десять — в Австралии. В Мельбурне — примерно раз в пять лет.","modifiedDate":"2021-04-27T16:27:54+03:00"},{"body":"Виной тому — поля райграса, пыльца которого считается сильным аллергеном. При цветении этого растения аллергики стараются не выходить из дома. Но в том-то и заключалась загадка, что заболевали не только они. Трехлетнеее обсервационное <a href="https:\/\/apallergy.org\/DOIx.php?id=10.5415\/apallergy.2020.10.e30" target="_blank" rel="nofollow noopener">исследование<\/a> показало, что из примерно десяти тысяч пострадавших в Мельбурне в ноябре 2016-го у 56 процентов никогда раньше не диагностировали астму, а у 13-ти в анамнезе не было даже ринита.","modifiedDate":"2021-04-27T16:27:54+03:00"},{"body":"Ученые установили, что аллергическую реакцию вызывает не сама пыльца плевела, а заключенные в ней крахмальные гранулы. В каждой частице содержится до 700 таких гранул. Пока зерна пыльцы целые, они задерживаются волосками в носу, а высвободившиеся гранулы микронного размера легко проходят сквозь этот фильтр, попадая в бронхи и легкие. Осталось понять механизм высвобождения гранул.","modifiedDate":"2021-04-27T16:27:54+03:00"},{"body":"После вспышки 2016 года в Мельбурне департамент здравоохранения штата <a href="http:\/\/ria.ru\/location_StateofVictoria\/" target="_blank" data-auto="true">Виктория<\/a> создал систему мониторинга. Специалисты регулярно брали пробы воздуха. Выяснилось, что после дождя содержание в атмосфере фрагментов разрушенных зерен — субпыльцевых частиц SPP (sub-pollen particles) — увеличивается в 50 раз. ","modifiedDate":"2021-04-27T16:27:54+03:00"},{"body":"Выдвинули гипотезу: во время грозы сильный ветер поднимает в воздух зерна пыльцы, которые, напитываясь влагой, лопаются в результате осмотического шока и распадаются на SPP. Исследования динамики конвективных течений в атмосфере в зоне движения грозового фронта <a href="https:\/\/journals.ametsoc.org\/view\/journals\/apme\/56\/5\/jamc-d-17-0027.1.xml" target="_blank" rel="nofollow noopener">подтвердили<\/a> возможность такого сценария. Однако грозы в период цветения — не такая уж редкость, все они сопровождаются сильными ветрами и повышением влажности, но далеко не каждая приводит к вспышке заболевания. ","modifiedDate":"2021-04-27T16:27:54+03:00"},{"body":"<a href="https:\/\/onlinelibrary.wiley.com\/doi\/full\/10.1111\/resp.13410" target="_blank" rel="nofollow noopener">Предположили<\/a> "идеальный шторм" — одновременное совпадение многих факторов, таких как специфические погодные условия, когда длительный период жары и засухи, приводящий к рассеиванию и растрескиванию зерен пыльцы, сменяется сильным ветром и дождем; увеличение концентрации углекислого газа в атмосфере и температуры воздуха, усиливающее аллергическую реакцию; нахождение людей на воздухе в самое неблагоприятное время и так далее. Но окончательный ответ удалось найти, только когда ученые выполнили компьютерное моделирование, учитывающее все эти факторы.","modifiedDate":"2021-04-27T16:27:54+03:00"}]}}\',\'kulakov\',\'Кулаков Владислав Геннадьевич\'}'
}

log.parse(arg)
