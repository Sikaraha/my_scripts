class JobList():

    def job_sched_dict(job_dict):
        """Выгрузить из bareos director словарь имен Job и имен их Shaduler
        исключая задания без Shaduler или с Enabled = no"""

        for job_conf in str(job_dict).split('Job {\n'):
            if "Schedule" in job_conf and "Enabled = no" not in job_conf:
                for l_conf in job_conf.split('\n'):
                    if "Name" in l_conf:
                        job_name = l_conf.replace('  Name = ', '').replace('"', '')
                    elif "Schedule" in l_conf:
                        shad_name = l_conf.replace('  Schedule = ', '').replace('"', '')
                yield job_name, shad_name

    def job_discr_dict(job_dict):
        """Выгрузить из bareos director словарь имен Job и имен их Description
        исключая задания без Shaduler или с Enabled = no"""

        for job_conf in str(job_dict).split('Job {\n'):
            if "Schedule" in job_conf and "Enabled = no" not in job_conf:
                for l_conf in job_conf.split('\n'):
                    if "Name" in l_conf:
                        job_name = l_conf.replace('  Name = ', '').replace('"', '')
                    elif "Description" in l_conf:
                        job_discr = l_conf.replace('  Description = ', '').replace('"', '')
                yield job_name, job_discr
