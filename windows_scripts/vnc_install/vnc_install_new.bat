@echo off
rem =========================================================================================
echo This is a script for remote installation and configuration of the VNC server 
echo Allow access to the distribution directory!
echo The script must be run with Windows admin domain permissions.
SET /P PC="Input remote PC name: "
SET LINK_FOLDER="C:\Users\%USERNAME%\scripts\windows_scripts\vnc_install\link"
SET DISTRIB_FOLDER="\\%COMPUTERNAME%\Users\%USERNAME%\scripts\windows_scripts\vnc_install"
echo %LINK_FOLDER% - link file directory
echo %DISTRIB_FOLDER% - origin distribution directory
rem =========================================================================================
echo copy distrib TightVNC and DFMirage
xcopy /y %DISTRIB_FOLDER%\tightvnc32.msi \\%PC%\ADMIN$\Temp
xcopy /y %DISTRIB_FOLDER%\dfmirage.exe \\%PC%\ADMIN$\Temp
rem copy http://https://www.tightvnc.com/download/2.8.11/tightvnc-2.8.11-gpl-setup-64bit.msi \\%PC%\ADMIN$\Temp
rem copy https://www.tightvnc.com/download/dfmirage/dfmirage-setup-2.0.301.exe \\%PC%\ADMIN$\Temp
rem =========================================================================================
echo Install TightVNC
psexec \\%PC% msiexec /i "C:\Windows\Temp\tightvnc32.msi" /quiet /norestart ADDLOCAL="Server,Viewer"
VIEWER_ASSOCIATE_VNC_EXTENSION=1 SERVER_REGISTER_AS_SERVICE=1 SERVER_ADD_FIREWALL_EXCEPTION=1 VIEWER_ADD_FIREWALL_EXCEPTION=1 SERVER_ALLOW_SAS=1 SET_USEVNCAUTHENTICATION=1 VALUE_OF_USEVNCAUTHENTICATION=1 SET_PASSWORD=1 VALUE_OF_PASSWORD=PASSWORD SET_USECONTROLAUTHENTICATION=1 VALUE_OF_USECONTROLAUTHENTICATION=1 SET_CONTROLPASSWORD=1 VALUE_OF_CONTROLPASSWORD=PASSWORD SET_VIEWONLYPASSWORD=1 VALUE_OF_VIEWONLYPASSWORD=1
rem =========================================================================================
echo Install DFMirage Driver
psexec \\%PC% -d c:\Windows\Temp\dfmirage.exe /verysilent /norestart
rem =========================================================================================
echo Ņonfiguring VNC
echo [connection] > %LINK_FOLDER%\%PC%.vnc
echo host=%PC% >> %LINK_FOLDER%\%PC%.vnc
echo port=5900 >> %LINK_FOLDER%\%PC%.vnc
echo Password - PASSWORD
echo password=5595320fe13858ab >> %LINK_FOLDER%\%PC%.vnc
echo [options] >> %LINK_FOLDER%\%PC%.vnc
echo use_encoding_1=1 >> %LINK_FOLDER%\%PC%.vnc
echo copyrect=1 >> %LINK_FOLDER%\%PC%.vnc
echo viewonly=0 >> %LINK_FOLDER%\%PC%.vnc
echo fullscreen=0 >> %LINK_FOLDER%\%PC%.vnc
echo 8bit=0 >> %LINK_FOLDER%\%PC%.vnc
echo shared=1 >> %LINK_FOLDER%\%PC%.vnc
echo belldeiconify=0 >> %LINK_FOLDER%\%PC%.vnc
echo disableclipboard=0 >> %LINK_FOLDER%\%PC%.vnc
echo swapmouse=0 >> %LINK_FOLDER%\%PC%.vnc
echo fitwindow=0 >> %LINK_FOLDER%\%PC%.vnc
echo cursorshape=1 >> %LINK_FOLDER%\%PC%.vnc
echo noremotecursor=0 >> %LINK_FOLDER%\%PC%.vnc
echo preferred_encoding=7 >> %LINK_FOLDER%\%PC%.vnc
echo compresslevel=-1 >> %LINK_FOLDER%\%PC%.vnc
echo quality=6 >> %LINK_FOLDER%\%PC%.vnc
echo localcursor=1 >> %LINK_FOLDER%\%PC%.vnc
echo scale_den=1 >> %LINK_FOLDER%\%PC%.vnc
echo scale_num=1 >> %LINK_FOLDER%\%PC%.vnc
echo local_cursor_shape=1 >> %LINK_FOLDER%\%PC%.vnc
rem =========================================================================================
pause
exit