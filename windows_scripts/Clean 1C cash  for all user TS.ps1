﻿#Отбираем сеансы с запущеной 1С
$1cv8 = Get-WMIObject win32_process | where {$_.ProcessName -eq "1cv8.exe"}
foreach ($proc in $1cv8)
{
#Оповещаем пользователей об отключении
msg $proc.SessionID /server:localhost "Внимание через 3 минуты ваш сейнс будет завершен! Пожалуйста закройте все приложения!"
#Таймаут
Strat-Sleep 180
#Завершаем сеансы с запущенной 1С
logoff $proc.SessionID
}
Strat-Sleep 60
#Чистим кэш у всех пользователей
Get-ChildItem "D:\Users\*\AppData\Local\1C\1Cv8\*","D:\Users\*\AppData\Roaming\1C\1Cv8\*" | Where {$_.Name -as [guid]} |Remove-Item -Force -Recurse