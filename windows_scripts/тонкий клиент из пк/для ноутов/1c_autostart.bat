@echo off
set executable=mstsc.exe
set process=mstsc.exe

:begin
tasklist |>nul findstr /b /l /i /c:%process% || start "" "%executable%" 1c_work.rdp
timeout /t 1 /nobreak >nul
goto :begin