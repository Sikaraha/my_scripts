My_memory
================================================
Сохраненные ключи Windows

		rundll32 keymgr.dll,KRShowKeyMgr

Выводим 10 самых обьемных файлов

		find /mnt/md0 -mount -type f -ls 2>/dev/null | sort -rnk7 | head -10 | awk '{printf "%10d MB\t%s\n",($7/1024)/1024,$NF}'

Найти файлы в которых отсутствует слово "year"

		find . -name "*.conf" -exec grep --files-without-match year {} \;

Найти все сломанные симлинки в каталоге

		find /dir -xtype l

Удаляем файлы старше 7 дней

		find /somewhere -type f -mtime +7 -print0 | xargs -0 rm -f
		find /somewhere -type f -btime +7 -delete
		find /data/mail/sushivesla.su/servicedesk@sushivesla.su/ -type f -btime +7 -delete

Копируем через SSH

		scp -P 7722 evdokimov@mail.sushivesla.su:/home/evdokimov/list_mail.txt ~
		scp file.txt user@remote.host:/some/remote/directory

Изменить язык по умолчанию в GDM
		
		/etc/X11/xorg.conf.d/00-keyboard.conf

Отключение тача:

	sudo /usr/bin/synclient TouchpadOff=1

Подключение ntfs в freebsd

		cd /usr/ports/sysutils/fusefs-ntfs
		make install clean
		kldload fuse
		ntfs-3g /dev/da0 /mnt

Монтирование ntfs linux

		mount -t ntfs-3g /dev/sdb1 /mnt/ntfs/

Проверка работоспособности spamassasin

		spamassassin --lint

Пример обучения по папке со спамом

		sa-learn --dbpath /путь/к/базе_bayes --spam /home/user/.Junk

Работаем с архивами

		tar -cvf <имя<путь - пакуем папку
		-c, --create  -создает новый архив
		-t, --?  -посмотреть содержимое
		-v, --verbose подробнрый вывод
		-f, --file указать обьект
		-z, --unzip распаковать tar.gz
		-J, --? распаковать tar.xz
		tar cvf home.tar /folder
		tar cZvf archive.tar.gz /folder
		gzip -9 archive.tar
		gzip -d archive.tar.gz
		tar xvf archive.tar
		tar tvf archive.tar
		tar xvzf archive.tar.gz
		tar xJf archive.tar.xz
		gzip -d <фаил- распаковываем (уничтожает архив при распаковке)

Поиск и удаление файла по дате и времени

		find /data2/mail/sushivesla.su/*/cur/ -mindepth 1 -newermt '2017-11-30 18:55' ! -newermt '2017-11-30 18:56' -ls -delete

Затираем память/меряем скорость записи,чтения

		dd if=/dev/zero of=/dev/[раздел] bs=1024k
		dd if=/dev/urandom of=/dev/[раздел] bs=1024k

		sync; dd if=/dev/zero of=/mnt/rhgs/tmp.file bs=1M count=1024; sync
		dd if=/mnt/rhgs/tmp.file of=/dev/null bs=1M count=1024

Меряем скорость в терминале

		wget http://speedtest.tele2.net/10GB.zip

Отключение второго дисплея

		xrandr --output TV-1 --off

Групповое переименование папок

		for i in `find /mnt/ntfs/finaudit.expert/ -name "*@finaudit.expert"`; do mv "$i" "${i%\@finaudit.expert}"; done

Групповое переименование файлов

		rename -v 's/@finaudit.expert//gi' /mnt/ntfs/finaudit.expert/*

Просмотреть состояние Firevall Linux

		iptables -L -v -n

Генерируем фаил произвольного размера

		dd if=/dev/zero of=bigfile bs=100M count=5
		dd if=/dev/random of=bigfile bs=5G
		dd of=bigfile bs=1 count=0 seek=5G

Генерируем кучу мелких файлов

		n=1 ;while [ ${n} -le 100 ] ;do cp /etc/fstab /home/source_file/many/new_${n}.file ;((n++)) ;done

Тести рование конфига bacula

		/opt/bacula/sbin/bacula-dir -tc /etc/bacula/bacula-dir.conf

Поиск информации об устройстве

		for i in $(find /sys/ |grep port_name); do cat $i | grep -i 500308C0A1991001; done

Записать файлы в tar на ленту

		tar -cvf /dev/st0 /root/test-folder

Посмотреть содержимое tar архива на ленте

		tar -tvf /dev/st0

Писать содержимое файла на ленту

		dd if=/root/test-folder/random.file of=/dev/st0 bs=1k

Записать содержимое леныт в фаил

		dd if=/dev/st0 of=/home/restore/restore.file bs=1k

CentOS плагин yum удаляющий зависимости yum-plugin-remove-with-leaves

		yum --remove-leaves remove PACKAGENAME

Сравнить 2 файла и вывести одинаковые строки в 3ий

		comm -1 -2 one.file two.file thfee.file 

Поиск слова по конфигам

		grep -inr 'djin-fs1.msk.rian' . |egrep -v '#|.svn|.old|switch'

скрипт рескан в дистр RH scsi (череват таймаутом!)

		/usr/bin/rescan-scsi-bus.sh

Поиск и удаление битых заданий

		for i in $(ls); do /etc/bacula/scripts/find_and_delete_fail_vols.sh ${i} fake; done

Посмотреть информацию о FC

		cat /sys/class/fc_host/host0/

ВКЛ/ВЫКЛ автозагрузку сервиса в UPSTART

		chkconfig service on/off

Проверить активные файлы на массивах

		lsof /mnt/md* | awk '/^bacula-sd/ && !/\/mnt\/md1\/working/ {print $9}'

Собираем bareos

		cd /usr/src/build
		cmake3 -DCMAKE_INSTALL_PREFIX:PATH=/opt/bareos-Release-18.2.6/ -Dpostgresql=yes -Dsysconfdir="/opt/bareos-Release-18.2.6/scripts/system/" -Dsbindir="/opt/bareos-Release-18.2.6/bin/" -Dconfdir="/opt/bareos-Release-18.2.6/etc/" -Dlogdir="/opt/bareos-Release-18.2.6/log/" -Dlibdir="/opt/bareos-Release-18.2.6/lib64/bareos/" -Dbackenddir="/opt/bareos-Release-18.2.6/backends/" -Dplugindir"/opt/bareos-Release-18.2.6/plugins/" -Dscriptdir="/opt/bareos-Release-18.2.6/scripts/" -Dworkingdir="/opt/bareos-Release-18.2.6/working/" -Dpiddir="/opt/bareos-Release-18.2.6/pid/" -Darchivedir="/opt/bareos-Release-18.2.6/drive/" /usr/src/bareos-Release-18.2.6/
		cmake3 --build . --target install --config Release



Получить тикет от kerberos

		kinit <username>

Подключить dedup3

	kinit sa_mssql_backup
	credentials=/root/.swize_mssql_cred

Сменить свой пароль на всех хостах через pssh
	
	usermod -p \$6\$JmDnMskX\$U3CZvoz3bm3P.0dylhUX7bLPfLPrV0q5JX4jJfLjZyp3.SOvraZ9aMNEE4ZHDKHZzTG1NAIrbWzeNYQu.bC/a/ ovechkin


Понизить установленый пакет до указанного
	
	rpm -Uvh ftp://src.rian.off/RPM/rhel7/bash-4.2.46-19.hs.el7.x86_64.rpm --oldpackage

Используем pssh для как инструмент управления группой хостов

	pssh -h /etc/servers/adminkey/group/Service--Balancer '/usr/bin/svn up --non-interactive /www/conf/cert'

	pssh -h /etc/servers/adminkey/group/System--Linux-RHEL7 'sed -i "s/vm.swapness=0/vm.swappiness=1/" /etc/tuned/ria-profile/tuned.conf; sysctl vm.swappiness=1'

	pssh -h /tmp/tmp.list 'if [ -f /etc/tuned/ria-profile/tuned.conf ]; then i="ria-profile"; else i="ria-db-profile"; fi; sed -i "s/^vm.swapness.*/vm.swappiness = 1/g" /etc/tuned/${i}/tuned.conf; sysctl vm.swappiness=1'
